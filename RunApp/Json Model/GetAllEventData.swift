//
//  GetPostResponse.swift
//  Majjelo
//
//  Created by Chatur Macbook on 07/12/17.
//  Copyright © 2017 Chatur Macbook. All rights reserved.
///Volumes/Untitled/Run APP git/RunApp.xcodeproj

import Foundation
import ObjectMapper


class GetAllEventData : Mappable {
    
    var event_id: Int?
    var event_name: String?
    var event_from_date: String?
    
    required init?(map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        
        event_id <- map["event_id"]
        event_name <- map["event_name"]
        event_from_date <- map["event_from_date"]
        
    }
}

