//
//  FeedbackViewController.swift
//  RunApp
//
//  Created by My Mac on 30/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
///Users/mymac/Downloads/RUNAPPSOURCENEW/RunApp.xcodeproj





import UIKit
import NightNight
import SVProgressHUD
import Alamofire



class FeedbackViewController: UIViewController {

    @IBOutlet var txtfeedback: UITextView!
    
    
    @IBOutlet var vwPopupTop: UIView!
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        txtfeedback.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtfeedback.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
        txtfeedback.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtfeedback.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
    
        
        txtfeedback.layer.cornerRadius = 5
        txtfeedback.layer.borderColor = UIColor.gray.cgColor
        txtfeedback.layer.borderWidth = 2
       
    }
    
      @objc func textFieldDidChange(textField: UITextField) {
            
      
        
        
        }
    
    
    func Feedback()
      {
          SVProgressHUD.show(withStatus: nil)
          let parameters = ["message" : txtfeedback.text!]
        
          let headers = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                        "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                        ] as [String : String]
          
        self.callApi(ServiceList.SERVICE_URL+ServiceList.Feedback,
                       method: .post,
                       param: parameters ,
                       extraHeader: headers ) { (result) in
                          
                          print(result)
                          
                          if result.getBool(key: "status")
                          {
                              showToast(uiview: self, msg: result.getString(key: "message"))
                          }
                          
                         showToast(uiview: self, msg: result.getString(key: "message"))
          }
        
      }
      

    @IBAction func btn_submit(_ sender: Any) {
        
        if txtfeedback.text != ""
        {
          Feedback()
        }
        else{
              showToast(uiview: self, msg:"Please Enter Feedback")
        }
        
    }
    
    
    @IBAction func btn_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
