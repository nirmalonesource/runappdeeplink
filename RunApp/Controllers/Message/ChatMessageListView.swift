//
//  ChatMessageListView.swift
//  RunApp
//
//  Created by My Mac on 06/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight

class ChatMessageSenderListCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        
        lblMessage.leftInset = 10
        lblMessage.topInset = 10
        lblMessage.rightInset = 10
        lblMessage.bottomInset = 10
        lblMessage.layer.cornerRadius = 5
        lblMessage.clipsToBounds = true
        lblMessage.sizeToFit()
        lblMessage.layoutIfNeeded()
        
    }
    
}

class ChatMessageReceiverListCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: CustomLabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        lblMessage.leftInset = 10
        lblMessage.topInset = 10
        lblMessage.rightInset = 10
        lblMessage.bottomInset = 10
        lblMessage.layer.cornerRadius = 5
        lblMessage.clipsToBounds = true
        lblMessage.sizeToFit()
        lblMessage.layoutIfNeeded()
        
    }
}

class ChatMessageListView: UIViewController , UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate
{

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var vwBottomMessage: UIView!
    @IBOutlet weak var imgUserMessage: UIImageView!
    @IBOutlet weak var txtWriteMessage: UITextField!
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
        txtWriteMessage.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        txtWriteMessage.setLeftPaddingPoints(30, strImage: "")
        txtWriteMessage.layer.masksToBounds = true
        txtWriteMessage.layer.cornerRadius = 20.0
        
        
        txtWriteMessage.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        
//        imgBG.setGredient()
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        
        // tblView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TextField Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendPressed(_ sender: UIButton) {
        print("Send Sms")
    }
    
    //MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 19
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMessageSenderListCell", for: indexPath) as! ChatMessageSenderListCell
            
            cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
            
            
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMessageReceiverListCell", for: indexPath) as! ChatMessageReceiverListCell
            
            
             cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
            cell.selectionStyle = .none
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    

}
