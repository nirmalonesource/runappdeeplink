//
//  ChatListView.swift
//  RunApp
//
//  Created by My Mac on 10/12/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight

class ChatListCell: UITableViewCell {
    
  
    
  
    @IBOutlet var btnUSerProfile: UIButton!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet var lblmsg: UILabel!
    
    override func awakeFromNib() {
        lblPlayerName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblmsg.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}

class ChatGroupListCell: UITableViewCell {
    
    @IBOutlet var btn_leavegroup: UIButton!
    @IBOutlet weak var btnAddmemebers: UIButton!
    @IBOutlet weak var imgUserG: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet var lblmsg: UILabel!
    
    override func awakeFromNib() {
        lblGroupName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblmsg.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
    
}



class ChatGroupPlayerListCell: UITableViewCell {
    
    
    
    
    
    
    @IBOutlet weak var imgUserG: UIImageView!
    @IBOutlet weak var lblPlayersName: UILabel!
    @IBOutlet var lblmsg: UILabel!
    
    
    override func awakeFromNib() {
      
        lblPlayersName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblmsg.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
    
}

class ChatListView: UIViewController,UITableViewDataSource ,UITableViewDelegate,UISearchBarDelegate ,UIGestureRecognizerDelegate{
   
    
    
    @IBOutlet var tblchatlist: UITableView!
    
    @IBOutlet weak var txtGroupName: UITextField!
    @IBOutlet weak var tblgroupchatlist: UITableView!
    
    @IBOutlet weak var tblPlayerForGroupList: UITableView!
    
    @IBOutlet var runlogo: UIImageView!
    
    @IBOutlet weak var ViewGroupChat: UIView!
    
    @IBOutlet weak var AddGrouNameView: UIView!
    
    
    @IBOutlet weak var blurimgGrpNAme: UIImageView!
    
    
    @IBOutlet weak var btn_addGroup: UIButton!
    
    
    
    
    @IBAction func back_click(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    
    var ArrMemberAdd :NSMutableArray  = []
    
     var Groupid : String? = ""
    var memberid : String? = ""
    
    var isselected : String? = ""
    
    
    var SavedChatListArr = [[String:Any]]()
    var ChatListArr = [[String:Any]]()
    var FilterChatListArr = [[String:Any]]()
    
    var SavedGroupChatListArr = [[String:Any]]()
    var ChatGroupListArr = [[String:Any]]()
    var FilterGroupChatListArr = [[String:Any]]()
    
    var SavedGroupPlayerChatListArr = [[String:Any]]()
    var ChatGroupPlayerListArr = [[String:Any]]()
    var FilterGroupPlayerChatListArr = [[String:Any]]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
           tapGestureRecognizer.delegate = self
           runlogo.isUserInteractionEnabled = true
           runlogo.addGestureRecognizer(tapGestureRecognizer)
        
        
        
        tblgroupchatlist.isHidden = true
        // Do any additional setup after loading the view.
        tblchatlist.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        tblgroupchatlist.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        tblPlayerForGroupList.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        ViewGroupChat.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
        
        self.hideKeyboardWhenTappedAround()

    }
    override func viewWillAppear(_ animated: Bool) {
        
        getchatlist()
        getGroupchatlist()
        
        
    }
        
    
    @IBAction func btn_addGroup(_ sender: Any) {
       
        blurimgGrpNAme.isHidden = false
        AddGrouNameView.isHidden = false
        
    }
    
    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
            
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
              nextViewController.type = "Adv"
             self.navigationController?.pushViewController(nextViewController, animated: true)
           
             
         }
    
    
    
   
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    //   let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        
         if isselected == "2"
        {
            let  array = SavedGroupPlayerChatListArr.filter({
                    // this is where you determine whether to include the specific element, $0
            ($0["FirstName"] as AnyObject).localizedCaseInsensitiveContains(searchText) //|| ($0["username"] as AnyObject).localizedCaseInsensitiveContains(searchText)
                    // or whatever search method you're using instead
                })

                FilterGroupPlayerChatListArr = array
                if FilterGroupPlayerChatListArr.count > 0 {
                    ChatGroupPlayerListArr = FilterGroupPlayerChatListArr
                }
                else
                {
                    ChatGroupPlayerListArr = SavedChatListArr
                }
                FilterGroupPlayerChatListArr = [[:]]
                tblPlayerForGroupList.reloadData()
                }
        
        
        else if isselected == "1"
        {
            let  array = SavedGroupChatListArr.filter({
                    // this is where you determine whether to include the specific element, $0
            ($0["GroupTitle"]! as AnyObject).localizedCaseInsensitiveContains(searchText)
                    // or whatever search method you're using instead
                })

            FilterGroupChatListArr = array
                if FilterGroupChatListArr.count > 0 {
                    ChatGroupListArr = FilterGroupChatListArr
                }
                else
                {
                    ChatGroupListArr = SavedGroupChatListArr
                }
                FilterGroupChatListArr = [[:]]
                tblgroupchatlist.reloadData()
                }
            
      
        else
        {
       // let array = (SavedChatListArr as NSArray).filtered(using: searchPredicate)
   let  array = SavedChatListArr.filter({
            // this is where you determine whether to include the specific element, $0
    ($0["username"]! as AnyObject).localizedCaseInsensitiveContains(searchText) || ($0["FirstName"]! as AnyObject).localizedCaseInsensitiveContains(searchText)
            // or whatever search method you're using instead
        })

        FilterChatListArr = array
        if FilterChatListArr.count > 0 {
            ChatListArr = FilterChatListArr
        }
        else
        {
            ChatListArr = SavedChatListArr
        }
        FilterChatListArr = [[:]]
        tblchatlist.reloadData()
        }
            
    }
        //MARK:- Table methods
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
            if tableView == tblchatlist {
                return ChatListArr.count
            }
            else if tableView == tblgroupchatlist
            {
               return ChatGroupListArr.count
            }
            else
            {
                return ChatGroupPlayerListArr.count
            }
            
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            
            if tableView == tblchatlist {
                
                
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell", for: indexPath) as! ChatListCell
                let playerData = ChatListArr[indexPath.row]
                cell.lblPlayerName.text = playerData["FirstName"] as? String
                cell.lblmsg.text = playerData["LastMessage"] as? String
            let imgurl = playerData["ImageURL"] as? String ?? ""
                cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
                cell.imgUser.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
              
               
          cell.btnUSerProfile.tag = indexPath.row
         cell.btnUSerProfile.addTarget(self, action: #selector(userprofile), for: .touchUpInside)
                
                
                
                
                
                // let imgURL = ServiceList.IMAGE_URL + imgurl
                
               // cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)
                
                
                
                return cell
            }
            else if tableView == tblgroupchatlist
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatGroupListCell", for: indexPath) as! ChatGroupListCell
                   
                    let playerData = ChatGroupListArr[indexPath.row]
                    cell.lblGroupName.text = playerData["GroupTitle"] as? String
                 
                  let isAdmin = playerData["isAdmin"] as? Int
                 
        cell.lblmsg.text =  "Unread Msg "  + "\(playerData["UnreadCount"] as? Int ?? 0)"
                
                if isAdmin == 0
                {
                    cell.btnAddmemebers.isHidden = true
                }
                else
                {
                      cell.btnAddmemebers.isHidden = false
                }
                
                 //   cell.lblmsg.text = playerData["LastMessage"] as? String
                    let imgurl = playerData["ImageURL"] as? String ?? ""
                    cell.imgUserG.setRadius(radius: cell.imgUserG.frame.height/2)
                    cell.imgUserG.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                
                cell.btnAddmemebers.tag = indexPath.row
                cell.btnAddmemebers.addTarget(self, action: #selector(btnRsvpAction), for: .touchUpInside)
                
                cell.btn_leavegroup.tag = indexPath.row
                cell.btn_leavegroup.addTarget(self, action: #selector(btnLeavegroup), for: .touchUpInside)
                
                   // let imgURL = ServiceList.IMAGE_URL + imgurl
                    
                   // cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)
                    
                    return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatGroupPlayerListCell", for: indexPath) as! ChatGroupPlayerListCell
                   
                    let playerData = ChatGroupPlayerListArr[indexPath.row]
                    cell.lblPlayersName.text = playerData["FirstName"] as? String
                 //   cell.lblmsg.text = playerData["LastMessage"] as? String
                    let imgurl = playerData["profilePic"] as? String ?? ""
                    cell.imgUserG.setRadius(radius: cell.imgUserG.frame.height/2)
                    cell.imgUserG.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options:.progressiveLoad, completed: nil)
                
                let id = ChatGroupPlayerListArr[indexPath.row]["id"] as! String
                          
                          if ArrMemberAdd.contains(id) {
                              
                              
                             // cell!.accessoryType = UITableViewCell.AccessoryType.none
                             
                              //let indexOfA = ArrMemberAdd.index(of: id)
                              
                              //ArrMemberAdd.removeObject(at: indexOfA)
                              
                            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
                              //
                             print("yes")
                         
                         }
                          else{
                             // cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                             
                            cell.accessoryType = UITableViewCell.AccessoryType.none
                               print("no")
                              
                          }
                        
                          
                
                
                   // let imgURL = ServiceList.IMAGE_URL + imgurl
                    
                   // cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)
                    
                    return cell
            }
            
            
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
          if tableView == tblchatlist
            
          {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailView") as! ChatDetailView
            nextViewController.dic_DataDetail = ChatListArr[indexPath.row]
            print(ChatListArr)
//                      nextViewController.currentUserName = lblUserName.text ?? ""
//                      nextViewController.currentUserimg = playerDetailResult[0]["UserProfile"] as? String ?? ""
            self.navigationController?.pushViewController(nextViewController, animated: true)
                    //  print("Comment Clicked: \(buttonRow)")
            }
            
            else if tableView == tblgroupchatlist
          {
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailView") as! ChatDetailView
                        nextViewController.dic_DataDetail = ChatGroupListArr[indexPath.row]
           
                print(ChatGroupListArr)
            
            nextViewController.isGroupChat = "true"
            //                      nextViewController.currentUserName = lblUserName.text ?? ""
            //                      nextViewController.currentUserimg = playerDetailResult[0]["UserProfile"] as? String ?? ""
            self.navigationController?.pushViewController(nextViewController, animated: true)
           
          }
            else if tableView == tblPlayerForGroupList
          {
            let cell = tableView.cellForRow(at: indexPath)
            let id = ChatGroupPlayerListArr[indexPath.row]["id"] as! String
            
            if ArrMemberAdd.contains(id) {
                
             
                
               // cell!.accessoryType = UITableViewCell.AccessoryType.none
               
                let indexOfA = ArrMemberAdd.index(of: id)
                
                ArrMemberAdd.removeObject(at: indexOfA)
                
                 cell!.accessoryType = UITableViewCell.AccessoryType.none
                //
               print("yes")
           
           }
            else{
               // cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                ArrMemberAdd.add(id)
              cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
                 print("no")
                
            }
           tblgroupchatlist.reloadData()
            
            }
            
           
    }
    
    func getchatlist()
    {
        let parameters = [ : ] as [String : Any]
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_CHATLIST,
                method: .get,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.ChatListArr = result["data"] as? [[String:Any]] ?? []
                        self.SavedChatListArr = result["data"] as? [[String:Any]] ?? []
                        
                        self.tblchatlist.reloadData()
                        
                        
                    }
        })
        
    }
    func getGroupchatlist()
       {
           let parameters = [ : ] as [String : Any]
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]
           
           callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_GROUPCHATLIST,
                   method: .get,
                   param: parameters ,
                   extraHeader: header ,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                          
                        self.ChatGroupListArr = result["data"] as? [[String:Any]] ?? []
                        self.SavedGroupChatListArr = result["data"] as? [[String:Any]] ?? []
                           
                        self.tblgroupchatlist.reloadData()
                           
                       }
           })
           
       }
   
    
    func leavegroupchat()
    {
        let parameters = ["MemberID":UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "GroupID" :Groupid!] as [String : Any]
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.API_GET_LEAVEGROUPCHAT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                       
                        self.getGroupchatlist()
                        
                     self.tblgroupchatlist.reloadData()
                        
                    }
        })
        
    }
    
    func getGroupPlayerchatlist()
          {
            
            let parameters = [ : ] as [String : Any]
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                            ] as [String : Any]
              
              callApi(ServiceList.SERVICE_URL+ServiceList.APP_get_ADDPLAYERList,
                      method: .get,
                      param: parameters ,
                      extraHeader: header ,
                      completionHandler: { (result) in
                          print(result)
                          if result.getBool(key: "status")
                          {
                             
                            self.ChatGroupPlayerListArr = result["data"] as? [[String:Any]] ?? []
                            self.SavedGroupPlayerChatListArr = result["data"] as? [[String:Any]] ?? []
                            self.tblPlayerForGroupList.reloadData()
                              
                            
                          }
              })
              
          }
    
      func AddMemebers()
            {

//                let jsonData: Data? = try? JSONSerialization.data(withJSONObject: ArrMemberAdd)
//                let jsonString = String(data: jsonData!, encoding: .utf8)
//
//                let cleanJsonString = jsonString!.replacingOccurrences(of: "\\", with: "")

             let string = ArrMemberAdd.componentsJoined(by: ",") as? String


            let parameters = [
                    "MemberID" :string! ,
                    "GroupID" : Groupid!
                ]

                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                              ]

                callApi(ServiceList.SERVICE_URL+ServiceList.APP_ADD_Players,
                        method: .post,
                        param: parameters as [String : Any],
                        extraHeader: header ,
                        completionHandler: { (result) in
                            print(result)

                            if result.getBool(key: "status")
                            {

                           showToast(uiview: self, msg: result.getString(key: "message"))

                                self.ViewGroupChat.isHidden = true
                                self.self.blurimgGrpNAme.isHidden = true
  
                            }
                            showToast(uiview: self, msg: result.getString(key: "message"))

                })
            }

 @objc func btnRsvpAction(button: UIButton) {
    
    let buttonRow = button.tag
     print("\(buttonRow): Btn Cell is Pressed")

    
    Groupid = ChatGroupListArr[buttonRow]["GroupID"] as? String ?? ""
   
    getGroupPlayerchatlist()
    ViewGroupChat.isHidden = false
    isselected = "2"
    
    }
    @objc func btnLeavegroup(button: UIButton) {
     
     let buttonRow = button.tag
      print("\(buttonRow): Btn Cell is Pressed")

     
     Groupid = ChatGroupListArr[buttonRow]["GroupID"] as? String ?? ""
     memberid = ChatGroupListArr[buttonRow]["GroupID"] as? String ?? ""
          
     leavegroupchat()
   
     
     }
    
    @objc func userprofile(button: UIButton) {
        
        let buttonRow = button.tag
        
        print("\(buttonRow): Btn Cell is Pressed")
        
        
        

        let usertype = ChatListArr[buttonRow]["usertype"] as? String

                                            if usertype == "Organization"
                                            {
                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                                nextViewController.playerOrglResult = ChatListArr[buttonRow]
                                                   nextViewController.isfromsearch = true;
                                               self.navigationController?.pushViewController(nextViewController, animated: true)


                                            }
                                            else
                                            {

                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                                   nextViewController.playerListDetail = ChatListArr[buttonRow]
                                                nextViewController.isfromsearch = true;
                                              self.navigationController?.pushViewController(nextViewController, animated: true)
                                            }

      
        
        }
       
    
    
    func CreateGroup()
        {
            let parameters = ["GroupTitle" : txtGroupName.text! ,
                              "DeviceID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                            ]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ]
 
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_CREATE_CROUPCHAT,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)

                        if result.getBool(key: "status")
                        {
                            
                            self.blurimgGrpNAme.isHidden = true
                            self.AddGrouNameView.isHidden = true
                            self.getGroupchatlist()
                            
                             
//                            self.txtOldPasssword.text = ""
//                            self.txtNewPassword.text = ""
//                            self.txtConfirmPassword.text = ""
//                            self.navigationController?.popViewController(animated: true)
                        }
                        showToast(uiview: self, msg: result.getString(key: "message"))
   
            })
        }
        
    
    
    
    
    @IBAction func btn_done(_ sender: Any) {
        
        if txtGroupName.text == ""
        {
            showToast(uiview: self, msg: "Please Enter Group Name")
        }
        else
        {
            CreateGroup()
        }
    }
   
    @IBAction func btn_close(_ sender: Any) {
        
        blurimgGrpNAme.isHidden = true
        AddGrouNameView.isHidden = true
        
    }
    
    
    
    @IBAction func btn_groupchat(_ sender: Any) {
        
        isselected = "1"
        tblchatlist.isHidden = true
        tblgroupchatlist.isHidden = false
        getGroupchatlist()
        
    }
    
    
    @IBAction func btn_singlechat(_ sender: Any) {
        
        isselected = "3"
        tblchatlist.isHidden = false
        tblgroupchatlist.isHidden = true
        
    }
    
    
    @IBAction func btn_addMember(_ sender: Any) {
      
       
    }
    
    
    @IBAction func btn_addingPlayersDone(_ sender: Any) {
            self.AddMemebers()
        
    }
    
}
