//
//  CustomPageControl.swift
//  RunApp
//
//  Created by My Mac on 30/05/19.
//  Copyright © 2019 My Mac. All rights reserved.
// 

import UIKit

class CustomPageControl: UIPageControl {
    var borderColor: UIColor = .red
    
    override var currentPage: Int {
        didSet {
            updateBorderColor()
        }
    }
    
    func updateBorderColor() {
        subviews.enumerated().forEach { index, subview in
            if index != currentPage {
               subview.layer.borderColor = borderColor.cgColor
                subview.layer.borderWidth = 1
            } else {
                subview.layer.borderWidth = 0
            }
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
