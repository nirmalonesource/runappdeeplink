//
//  OrganizationProfile.swift
//  RunApp
//
//  Created by My Mac on 08/01/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SVProgressHUD
import Foundation
import MobileCoreServices
import Photos
import AVKit
import AVFoundation
import NightNight

class OrganizationProfile: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, UITableViewDataSource ,  UITableViewDelegate,UIGestureRecognizerDelegate {
    
    var type = ""
    var isfromnotification = false
   var BoolFriendIs = false
var imgURL = String()
       
    
    @IBOutlet var runlogo: UIImageView!
    @IBOutlet weak var btnAddFriendOutlet: UIButton!
    
    
    @IBOutlet weak var lblorganizationanme: UILabel!
    
    @IBOutlet weak var lblorganizationemail: UILabel!
    
    
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var tblViewPlayers: UITableView!
    @IBOutlet weak var collectionGallery: UICollectionView!
    @IBOutlet weak var BtnCharBadges: UIButton!
       @IBOutlet weak var BtnSkillBadge: UIButton!
       @IBOutlet weak var BtnTeamBadges: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
      @IBOutlet weak var lblUserName: UILabel!
      @IBOutlet weak var lblHomeTown: UILabel!
      @IBOutlet weak var lblSchool: UILabel!
      @IBOutlet weak var lblPost: UILabel!
      @IBOutlet weak var lblLOC: UILabel!
      @IBOutlet weak var lblBadge: UILabel!
    @IBOutlet weak var btnSettingOutlet: UIButton!
    @IBOutlet weak var lblHeight: UILabel!

       @IBOutlet weak var heightPostLbl: NSLayoutConstraint!
         
         @IBOutlet weak var btn2Width: NSLayoutConstraint!
         @IBOutlet weak var btn1Width: NSLayoutConstraint!
         
         @IBOutlet weak var charWidth: NSLayoutConstraint!
         
         @IBOutlet weak var btnLeadingOutlet: NSLayoutConstraint!
         
         @IBOutlet weak var btnCenterLeading: NSLayoutConstraint!
         @IBOutlet weak var btn2Trailing: NSLayoutConstraint!
         
         @IBOutlet weak var tblIndexingTop: NSLayoutConstraint!
         
         @IBOutlet weak var btn2Leading: NSLayoutConstraint!
         
         @IBOutlet weak var tableIndexingView: UITableView!
    
    var names: [Name] = []
    var sortedFirstLetters: [String] = []
    var sections: [[Name]] = [[]]
       
    
    
    var isfromsearch = Bool()
    var friendGetList = [[String : Any]]()
    var friendRequestList = [[String : Any]]()
    var eventList = [[String : Any]]()
    
    var playerListArray = [Player]()
    var dataPlayer:[Player] = []
    var playerList = [[String : Any]]()
    var arr_images = [UIImage]()
     var imagArray = [UIImage]()
     var getPhotoDict = [[String : Any]]()
     var playerDetailResult = [[String:Any]]()
    var playerOrglResult = [String:Any]()
     var levelOfCompositionList = [[String : Any]]()
     var BoolIs = false
    let GetId = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
    var id = ""
    var selectedIndex = Int()
    var PhotoID = ""
    fileprivate var currentVC: UIViewController?
    let picker = UIImagePickerController()
     var GetLOCId : String?
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if playerOrglResult.count > 0 {
     
            GetLOCId = playerOrglResult["CLVLTitle"] as? String ?? ""
                    
                    id = playerOrglResult["id"] as? String ?? ""
                    print("GetLOCId: \(GetLOCId ?? "")")
                    lblLOC.text = "LOC: \(GetLOCId ?? "")"

                    btnAddFriendOutlet.isHidden = false
                    btnSettingOutlet.isHidden   = false
                  
            
                    lblUserName.text = playerOrglResult["username"] as? String
                    lblHomeTown.text = "Home Town : \(playerOrglResult["Hometown"] as? String ?? "")"
                    lblBadge.text = playerOrglResult["Points"] as? String ?? ""
                    lblorganizationanme.text = playerOrglResult["FirstName"] as? String ?? ""
                    lblorganizationemail.text = playerOrglResult["website"] as? String ?? ""
                  //  lblScore.text = playerDetailResult[0]["Total_Points"] as? String ?? ""
                   // lblScore.text = playerDetailResult[0]["Score"] as? String ?? ""
                 //   lblPoint.text = playerDetailResult[0]["Points"] as? String ?? ""
                    let school = "\(playerOrglResult["RoleName"] as? String ?? "")"

                    let heightFt = playerOrglResult["HeightFT"] as? String ?? ""
                    let HeightInch = playerOrglResult["HeightInch"] as? String ?? ""

                    lblHeight.text = "Height: \(heightFt)′ \(HeightInch)″"

                    //6′ 0″

                    let status = playerOrglResult["status"] as? Int


                    if school != "" {
                        lblSchool.text = "\(playerOrglResult["RoleName"] as? String ?? "")"
                    }

                    else {
                        lblSchool.isHidden = true
                       // lblLOC.isHidden = true
                    }

                    let imgURL = ServiceList.IMAGE_URL + "\(playerOrglResult["UserProfile"] as? String ?? "")"

                     imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                     imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                     imgUser.setRadius(radius: imgUser.frame.height/2)
                     let imageView = RoundedImageView()
                     imageView.image = imgUser.image

    }
                
        
            else
        {
              id = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
              btnAddFriendOutlet.isHidden = true
              //btnSettingOutlet.isHidden   = true
                              
        }
        
               
        
        
        // Do any additional setup after loading the view.
      //    id = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
        lblUserName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        btnSettingOutlet.setMixedImage(MixedImage(normal:"setting", night:"setting-W"), forState: .normal)
        lblHomeTown.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblSchool.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblPost.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLOC.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblHeight.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblorganizationemail.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblorganizationanme.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        tblViewPlayers.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
        
        
               // btnCartOutlet.setMixedImage(MixedImage(normal:"shopping-cart", night:"shopping-cart-W"), forState: .normal)
                
              //  imgBadge.mixedImage = MixedImage(normal: UIImage(named: "setting") ?? UIImage(), night: UIImage(named: "setting-W") ?? UIImage())
               // lblBadgeTitle.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
              //  lblSponserName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
              //  lblBadgeDescription.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
              //  btn1.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
              //   btn2.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
              //   btn3.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
               //  btn4.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
               //  btn5.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
               //  btn6.setMixedTitleColor( MixedColor(normal: 0xffffff, night:0x000000 ), forState: .normal)
              //  lblTotalPoint.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
              //  lblTotalNominate.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        //         vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
        //         vwPopupDetailView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
                vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
              //  vwPopupDetailView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
                
              //  self.view.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.nightfont)
                
               // vwPopup2ndView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        //         vwPopup2ndView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
        //        tblHistory.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x252525)
             //   vwPopup2ndView.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
             //   tblHistory.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
                
                
//                if NightNight.theme == .night {
//                    btnCartOutlet.imageView?.changeImageViewImageColor(color: UIColor.white)
//
//                } else {
//                     btnCartOutlet.imageView?.changeImageViewImageColor(color: UIColor.black)
//
//                }
                
                picker.delegate = self
//                makeViewCircular(view: btnFirstSubOutlet)
//                makeViewCircular(view: btnSecondSubOutlet)
//                makeViewCircular(view: btnThirdSubOutlet)
//                makeViewCircular(view: btnFourthSubOutlet)
//                makeViewCircular(view: btnFifthSubOutlet)
//                makeViewCircular(view: btnSixSubOutlet)
         APP_USER_PROFILE_DETAIL()
       // playerListApi()
        Player_Wise_Event()
        appGetPhoto()
        arr_images = [(UIImage.init(named: "PersonWithBoll") ?? UIImage())  ,#imageLiteral(resourceName: "img_gallery"),#imageLiteral(resourceName: "img_user") ]
        self.view.layoutIfNeeded()
        
        
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
                                    tapGestureRecognizer2.delegate = self
                                    runlogo.isUserInteractionEnabled = true
                                    runlogo.addGestureRecognizer(tapGestureRecognizer2)
                     
                     
                     
                 }
                 
                 @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
                    
                     let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
                      nextViewController.type = "Adv"
                     self.navigationController?.pushViewController(nextViewController, animated: true)
                   
                     
                 }
    
    
    override func viewWillAppear(_ animated: Bool) {
           myCollectionView.reloadData()
       }

  //MARK:- Table methods
        
        func numberOfSections(in tableView: UITableView) -> Int {
            
           if tableView == tableIndexingView {
                           return sections.count
                       }
                           
                       else {
                           return 1
                       }
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView == tblViewPlayers {
                return eventList.count
            }
            else if tableView == tableIndexingView
            {
                   return sections[section].count
            }
            else
            {
                return 0
            }
                
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
            if tableView == tableIndexingView
          {
                         
                          let cell = tableView.dequeueReusableCell(withIdentifier: "indexingTableCell1", for: indexPath) as! indexingTableCell1
                                       
                                       let name = sections[indexPath.section][indexPath.row]
                                       
                                       cell.lblName.text = name.nameTitle
                                     //  cell.lblLast.text = name.homeTown
                                        cell.lblLast.text = name.Score
                                       
                                       imgURL = name.image
                                       
                                       imgURL = ServiceList.IMAGE_URL + imgURL
                                       cell.imagUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString())!), placeholderImage : nil , options: .progressiveLoad,completed: nil)
                                       
                                       cell.imagUser.setRadius(radius: cell.imagUser.frame.height/2)
                                       //            cell.BtnConfirmFriend.setRadius(radius: cell.BtnConfirmFriend.frame.height/2)
                                       //            cell.btnDecline.setRadius(radius: cell.btnDecline.frame.height/2)
                                       
                                       cell.BtnConfirmFriend.tag = (indexPath.section*100)+indexPath.row
                                       cell.btnDecline.tag = (indexPath.section*100)+indexPath.row
                                       
                                       cell.BtnConfirmFriend.addTarget(self, action: #selector(self.btnCellConfirmFriendAction(_:)), for: .touchUpInside)
                                       
                                       cell.btnDecline.addTarget(self, action: #selector(self.btnCellDeclineAction(_:)), for: .touchUpInside)
                                       
                                       if GetId != id {
                                           cell.BtnConfirmFriend.isHidden = true
                                           cell.btnDecline.isHidden = true
                                       }
                                       
                                      
                                       
                                       if BoolFriendIs {
                                           cell.BtnConfirmFriend.isHidden = false
                                           cell.btnTopConfirm.constant = 20
                                           cell.BtnConfirmFriend.backgroundColor = UIColor.red
                                           cell.BtnConfirmFriend.setTitle("Unfriend", for: .normal)
                                           
                                           cell.btnDeclineBotom.constant = 0
                                           cell.btnDecline.isHidden = true
                                       }
                                           
                                       else {
                                           cell.BtnConfirmFriend.setTitle("Confirm", for: .normal)
                                           cell.BtnConfirmFriend.backgroundColor = UIColor.green
                                           cell.btnDeclineBotom.constant = 10
                                           cell.BtnConfirmFriend.isHidden = false
                                           cell.btnDecline.isHidden = false
                                       }
                                       
                                       if isfromsearch
                                       {
                                           cell.BtnConfirmFriend.isHidden = true
                                       }
                                       
                                       
                                       cell.BtnConfirmFriend.setRadius(radius: cell.BtnConfirmFriend.frame.height/2)
                                       cell.btnDecline.setRadius(radius: cell.btnDecline.frame.height/2)
                                       
                                       return cell
                      }
            
            else {
                  let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlayerListCell", for: indexPath) as! SearchPlayerListCell
//                let playerData = dataPlayer[indexPath.row]
//                cell.lblPlayerName.text = playerData.PlayerName
//                cell.lblSchool.text = playerData.School
//                cell.lblLocation.text = playerData.Location
//                cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
//                cell.imgUser.sd_setImage(with: URL(string : (playerData.imgUser.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
//                let imgURL = ServiceList.IMAGE_URL + playerData.imgUser
//
//
//                cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)
//
//                if playerData.CheckedIn == "1" {
//                    cell.imglocation.image = UIImage (named: "LocationGreen")
//                }
//                else
//                {
//                     cell.imglocation.image = UIImage (named: "redLocation")
//                }
             
                

                 cell.lblPlayerName.text = eventList[indexPath.row]["EvenetName"] as? String ?? ""
                 cell.lblSchool.text = eventList[indexPath.row]["EventDate"] as? String ?? ""
                cell.lblLocation.text = eventList[indexPath.row]["CLVLTitle"] as? String ?? ""
                              
                 cell.lblPlayerName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                 cell.lblSchool.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                 cell.lblLocation.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                 cell.lblLoc.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                
                // cell.lblPublish.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                 
                 
                   let cretedOn = eventList[indexPath.row]["EventDate"] as? String ?? ""
                 

                 if cretedOn != "" {
                     cell.lblSchool.text = cretedOn.toDate5()
                 }
                 
//                 cell.btn_Cancel.tag = indexPath.row
//                 cell.btn_Cancel.addTarget(self, action: #selector(self.btnCancelEvent), for: .touchUpInside)
                 
                 
//                 let EventStatus = eventList[indexPath.row]["EventStatus"] as? String ?? ""
//
//                 if EventStatus == "1"
//                 {
//                     cell.lblPublish.text = "Pending"
//                     cell.btn_Cancel.isHidden = false
//
//                 }
//                 else if EventStatus == "2"
//                 {
//                     cell.lblPublish.text = "Approved"
//                     cell.btn_Cancel.isHidden = false
//
//                 }
//                 else if EventStatus == "3"
//                 {
//                     cell.lblPublish.text = "Rejected"
//                 }
//                 else if EventStatus == "4"
//                 {
//                     cell.lblPublish.text = "Completed"
//                 }
//                 else if EventStatus == "5"
//                 {
//                     cell.lblPublish.text = "Deactivated"
//                 }
//                 else if EventStatus == "6"
//                        {
//                            cell.btn_Cancel.isHidden = true
//                            cell.lblPublish.text = "Cancelled"
//                        }
//                 else
//                 {
//                 //   cell.lblPublish.text = "Pending"
//                 }
//
                 
                 
                   cell.lblLoc.text = eventList[indexPath.row]["LocationName"] as? String ?? ""
                    
                
                
                 cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
                 
                 return cell
            }
            
            
            
           
              
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if tableView == tblViewPlayers {
               
               //  let eventData = eventList[indexPath.row]
                 let LocationID = self.eventList[indexPath.row]["LocationID"] as? String ?? ""
                 let status = self.eventList[indexPath.row]["EventStatus"] as? String ?? ""
                  let EventId = self.eventList[indexPath.row]["EventID"] as? String ?? ""
                
                
                  if status == "0" {
                    
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
            nextViewController.LocationID = LocationID
            nextViewController.EventStatus = status
           self.navigationController?.pushViewController(nextViewController, animated: true)
                                        
                  }
                            
                                        else if status == "1" {
                                            
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                        nextViewController.Eventstatus = status
                        nextViewController.LocationID = LocationID
                        nextViewController.EventID = EventId
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                        }
                            
                            else if status == "2" {
                                            
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                                      nextViewController.Eventstatus = status
                                      nextViewController.LocationID = LocationID
                                      nextViewController.EventID = EventId
                                      self.navigationController?.pushViewController(nextViewController, animated: true)
                                            
                            }
                
//                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailView") as! EventDetailView
//                nextViewController.Event_ID = eventList[indexPath.row]["EventID"] as? String ?? ""
//                    nextViewController.Event_Name = eventList[indexPath.row]["EvenetName"] as? String ?? ""
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
            else if tableView == tableIndexingView
                {
                    if BoolFriendIs {
                                 // cell.BtnConfirmFriend.isHidden = true
                                 // cell.btnDecline.isHidden = true
                                   let playerData = friendGetList[indexPath.section]
                                     print(playerData)
                                     
                                     let name = sections[indexPath.section][indexPath.row]
                                     let didselectname = name.nameTitle

                                     let friendReqlist : NSArray = friendGetList as! NSArray
                                       
                                     print(friendGetList)
                                         var index = 0
                                         for i in 0 ..< friendReqlist.count
                                         {

                                         let dict:NSDictionary = friendReqlist.object(at:i) as! NSDictionary
                                         print(dict.value(forKey: "FirstName") as! String)
                                             
                                             if ( name.nameTitle == dict.value(forKey: "FirstName") as! String)
                                                                       {
                                                                     
                                                         index = i
                                                         let selectedarr:NSDictionary = self.friendGetList[index] as! NSDictionary
                                                             print(selectedarr)
                                                                  
                                             }
                              
                                                                     
                                             
                                                                 
                                                               }

                                     let usertype = friendGetList[index]["usertype"] as? String
                                     
                                     if usertype == "Organization"
                                     {
                                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                         nextViewController.playerOrglResult = friendGetList[index]
                                            nextViewController.isfromsearch = true;
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                         
                                         
                                     }
                                     else
                                     {
                                         
                                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                            nextViewController.playerListDetail = friendGetList[index]
                                         nextViewController.isfromsearch = true;
                                         self.navigationController?.pushViewController(nextViewController, animated: true)
                                     }
                                     
                                             
                                                        
                                     
                                     
                                  // nextViewController.playerListDetail = friendGetList[indexPath.section]
                                 }
                                 else
                                 {
                                     
                                     
                                   let playerData = friendRequestList[indexPath.section]
                                 let name = sections[indexPath.section][indexPath.row]
                                let didselectname = name.nameTitle

                                     let friendReqlist : NSArray = friendRequestList as! NSArray

                                     print(friendRequestList)
                                     var index = 0
                                                for i in 0 ..< friendReqlist.count {

                                 let dict:NSDictionary = friendReqlist.object(at:i) as! NSDictionary
                                      print(dict.value(forKey: "FirstName") as! String)
                                     if ( name.nameTitle == dict.value(forKey: "FirstName") as! String)
                                                    {
                                                        
                                        index = i
                                        let selectedarr:NSDictionary = self.friendRequestList[index] as! NSDictionary
                                          print(selectedarr)
                                                                                                     
                                                    }
                                            }
                            
                                     let usertype = friendRequestList[index]["usertype"] as? String
                                                       
                                                       if usertype == "Organization"
                                                       {
                                                           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                                           nextViewController.playerOrglResult = friendRequestList[index]
                                                              nextViewController.isfromsearch = true;
                                                          self.navigationController?.pushViewController(nextViewController, animated: true)
                                                           
                                                           
                                                       }
                                                       else
                                                       {
                                                           
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                nextViewController.playerListDetail = friendRequestList[index]
                nextViewController.isfromsearch = true;
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
                                    }
                                                       
                                                               
                                                                          
                    
                    }
            }
        }
    
    
     //MARK:- COLLECTION METHODS
            
            func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }
            
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                if collectionView == myCollectionView {
                    return arr_images.count
                }
                    
              
                    
                else //if collectionView == collectionGallery
                {
                    if GetId == id {
                        return getPhotoDict.count
                    }
                        
                    else {
                        return getPhotoDict.count
                    }
                }
                
            }
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
                if collectionView == myCollectionView
                {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionCell", for: indexPath) as! MainCollectionCell
                    
                    cell.imgOutlet.image = arr_images[indexPath.row]
                    if indexPath.row == selectedIndex
                    {
                        cell.imgOutlet.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    }
                    else
                    {
                      //  cell.imgOutlet.changeImageViewImageColor(color: UIColor.black)
                       //  cell.imgOutlet.mixedTintColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                        if NightNight.theme == .night {
                            cell.imgOutlet.changeImageViewImageColor(color: UIColor.white)
                        } else {
                           cell.imgOutlet.changeImageViewImageColor(color: UIColor.black)
                        }
                    }
                    return cell
                }
                    
                else //if collectionView == collectionGallery
                {
                    
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileGalleryCollectionCell", for: indexPath) as!
                    ProfileGalleryCollectionCell
                                
                    
                                if BoolIs {
                                     if GetId == id
                                     {
                                        cell.btnMessage.tag = indexPath.row
                                        cell.btnMessage.addTarget(self, action: #selector(btnCommentAction), for: .touchUpInside)
                                        
                                        cell.btn_comment.tag = indexPath.row
                                        cell.btn_comment.addTarget(self, action: #selector(btnAddCommentAction), for: .touchUpInside)
                                        cell.btn_comment.isUserInteractionEnabled = true
                                    }
                                    else
                                     {
                                        
                                    }
                                    cell.imgGallery.isHidden = false
                                    cell.btnLike.isHidden = false
                                    cell.btnMessage.isHidden = false
                                    cell.lblCount.isHidden = false
                                    
                                    cell.lblmesaagecount.layer.cornerRadius = 7.5
                                     cell.lblmesaagecount.layer.borderColor = UIColor.green.cgColor
                                    cell.lblmesaagecount.layer.borderWidth = 1
                                    
                                    
                                    
                                    let imgURL = ServiceList.IMAGE_URL + "\(getPhotoDict[indexPath.row]["Photo"] as? String ?? "")"
                                 
                                    let type = getPhotoDict[indexPath.row]["Type"] as? String ?? ""
                                   
                                    if type == "image" {
                                        cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                                                    completed: nil)
                                        cell.btnVideo.isHidden = true
                                        
                                    }
                                    
                                    else {
                                        let url = URL(string: imgURL)
                                        cell.btnVideo.isHidden = false
                                        if let videoThumbnail = getThumbnailFrom(path: url!) {
                                            cell.imgGallery.image = videoThumbnail
                                        }
                                    }
                                
                                   
                                    let total = getPhotoDict[indexPath.row]["total"] as? String ?? ""
                                    cell.lblCount.text = total
                                    
                                    
                                    let status = getPhotoDict[indexPath.row]["status"] as? String ?? ""
                                    
                                    if status == "0" {
                                    
                                        let image = UIImage(named: "UnfilledHurt1")
                    
                                        cell.btnLike.setImage(image, for: .normal)
                                        
                                        PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                                        cell.btnLike.tag = indexPath.row
                                        cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                                        cell.btnLike.isEnabled = true
                                        cell.btn_comment.tag = indexPath.row
                                        cell.btn_comment.addTarget(self, action: #selector(btnAddCommentAction), for: .touchUpInside)
                                        cell.btn_comment.isUserInteractionEnabled = true
                                        
                                    }
                                        
                                    else {
                                        
                                         let image = UIImage(named: "filledHurt1")
                                         cell.btnLike.setImage(image, for: .normal)
                                         cell.lblCount.text = total
                                         PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                                        cell.btnLike.isEnabled = false

                                    }
                                   
                                    cell.btnVideo.tag = indexPath.row
                                    cell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                                   
                                }
                                    
                                else {
                                    
                                    if GetId == id {
                                        
                                        let imgURL = ServiceList.IMAGE_URL + "\(getPhotoDict[indexPath.row]["Photo"] as? String ?? "")"
                                        
                                        let type = getPhotoDict[indexPath.row]["Type"] as? String ?? ""
                                        
                                        if type == "image" {
                                            cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                                                        completed: nil)
                                            cell.btnVideo.isHidden = true
                                        }
                                            
                                        else {
                                            let url = URL(string: imgURL)
                                            cell.btnVideo.isHidden = false
                                            if let videoThumbnail = getThumbnailFrom(path: url!) {
                                                cell.imgGallery.image = videoThumbnail
                                            }
                                        }
                                        
                                        let status = getPhotoDict[indexPath.row]["status"] as? String ?? ""
                                        let total = getPhotoDict[indexPath.row]["total"] as? String ?? ""
                                        cell.lblCount.text = total
                                        
                                        if status == "0" {
                                            
                                            let image = UIImage(named: "UnfilledHurt1")
                                            
                                            cell.btnLike.setImage(image, for: .normal)
                                            
                                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                                            
                                            cell.btnLike.tag = indexPath.row
                                            cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                                            cell.btnLike.isEnabled = true
                                        }
                                            
                                        else {
                                            
                                            let image = UIImage(named: "filledHurt1")
                                            cell.btnLike.setImage(image, for: .normal)
                                            cell.lblCount.text = total
                                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                                            cell.btnLike.isEnabled = false

                                        }
                                        
                                        PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                                        cell.lblCount.text = "(\(getPhotoDict[indexPath.row]["total"] as? String ?? ""))"
                                        cell.btnVideo.tag = indexPath.row
                                        
                                        cell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                                        cell.btnMessage.tag = indexPath.row
                                        cell.btnMessage.addTarget(self, action: #selector(btnCommentAction), for: .touchUpInside)
                                        cell.btn_comment.tag = indexPath.row
                                        cell.btn_comment.addTarget(self, action: #selector(btnAddCommentAction), for: .touchUpInside)
                                        cell.btn_comment.isUserInteractionEnabled = true
                                        
                                    }
                                        
                                    else {
                                        
                                        
                                        let imgURL = ServiceList.IMAGE_URL + "\(getPhotoDict[indexPath.row]["Photo"] as? String ?? "")"
                //                        cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                //                                                    completed: nil)
                                        
                                        
                                        let type = getPhotoDict[indexPath.row]["Type"] as? String ?? ""
                                        
                                        if type == "image" {
                                            cell.imgGallery.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                                                        completed: nil)
                                            cell.btnVideo.isHidden = true
                                            
                                        }
                                            
                                        else {
                                            let url = URL(string: imgURL)
                                            cell.btnVideo.isHidden = false
                                            if let videoThumbnail = getThumbnailFrom(path: url!) {
                                                cell.imgGallery.image = videoThumbnail
                                            }
                                        }
                                        
                                        let total = getPhotoDict[indexPath.row]["total"] as? String ?? ""
                                        cell.lblCount.text = total
                                        
                                        
                                        let status = getPhotoDict[indexPath.row]["status"] as? String ?? ""
                                        
                                        if status == "0" {
                                            
                                            let image = UIImage(named: "UnfilledHurt1")
                                            
                                            cell.btnLike.setImage(image, for: .normal)
                                            
                                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                                            
                                            cell.btnLike.tag = indexPath.row
                                            cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                                            cell.btnLike.isEnabled = true
                                            cell.btn_comment.tag = indexPath.row
                                            cell.btn_comment.addTarget(self, action: #selector(btnAddCommentAction), for: .touchUpInside)
                                            cell.btn_comment.isUserInteractionEnabled = true
                                        }
                                            
                                        else {
                                            
                                            let image = UIImage(named: "filledHurt1")
                                            cell.btnLike.setImage(image, for: .normal)
                                            cell.lblCount.text = total
                                            PhotoID = getPhotoDict[indexPath.row]["PhotoID"] as? String ?? ""
                                            
                                            cell.btnLike.isEnabled = false
                                        }
                                        cell.btnVideo.tag = indexPath.row
                                        cell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                                    }
                                    
                                }
                                
                                return cell
                            }
                    
                 
            }
    @objc func btnAddCommentAction(button: UIButton) {
        let buttonRow = button.tag
        PhotoID = getPhotoDict[buttonRow]["PhotoID"] as? String ?? ""
        
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CommentListViewController") as! CommentListViewController
            nextViewController.PhotoID = PhotoID
           
    self.navigationController?.pushViewController(nextViewController, animated: true)
       
    }
    override func viewDidAppear(_ animated: Bool) {
        if isfromnotification {
            if type == "friend_request" {
                let indexPath = IndexPath(item: 2, section: 0);
                myCollectionView.delegate?.collectionView!(myCollectionView, didSelectItemAt: indexPath)
            }
            else
            {
                let indexPath = IndexPath(item: 1, section: 0);
                myCollectionView.delegate?.collectionView!(myCollectionView, didSelectItemAt: indexPath)
            }
        }
        

    }
            
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
                if collectionView == myCollectionView {
                    selectedIndex = indexPath.row
                    collectionView.reloadData()
                    
                    if indexPath.row == 0 {
                        collectionGallery.isHidden = true
                        tblViewPlayers.isHidden = false
                        BtnSkillBadge.isHidden = true
                        BtnTeamBadges.isHidden = true
                        BtnCharBadges.isHidden = true
                        tableIndexingView.isHidden = true
                    }
                        
                    else if indexPath.row == 1 {
                        
                        if GetId == id {
                            BtnCharBadges.isHidden = false
                        }
                            
                        else {
                            BtnCharBadges.isHidden = true
                        }
                        
                        BtnTeamBadges.backgroundColor = UIColor.clear
                        BtnSkillBadge.backgroundColor = UIColor.clear
                        BtnCharBadges.backgroundColor = UIColor.clear
                        charWidth.constant = 30
                        //btnLeadingOutlet.constant = 25
                        BtnSkillBadge.setTitle("", for: .normal)
                        BtnCharBadges.setTitle("", for: .normal)
                        BtnTeamBadges.setTitle("", for: .normal)
                        
                        
                        
    //                    BtnSkillBadge.setImage(UIImage(named: "img_menu_grid"), for: .normal)
    //                    BtnCharBadges.setImage(UIImage(named: "plus1"), for: .normal)
    //                    BtnTeamBadges.setImage(UIImage(named: "menu"), for: .normal)
                        
                        BtnSkillBadge.setMixedImage(MixedImage(normal:"img_menu_grid", night:"img_menu_grid-W"), forState: .normal)
                        BtnCharBadges.setMixedImage(MixedImage(normal:"plus1", night:"plus-button"), forState: .normal)
                        BtnTeamBadges.setMixedImage(MixedImage(normal:"menu", night:"menu-W"), forState: .normal)
                        
                        
                        //BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                        
                        tableIndexingView.isHidden = true
                        collectionGallery.isHidden = false
                        tblViewPlayers.isHidden = true
                        BtnSkillBadge.isHidden = false
                        BtnTeamBadges.isHidden = false
                        BtnCharBadges.isHidden = false
                        collectionGallery.reloadData()
                    }
                    else if indexPath.row == 2  {
                       
                        
                        BoolFriendIs = true
                        APP_MY_FRIEND()
                                          
                            if GetId == id {
                                              tblIndexingTop.constant = 65
                                }
                                              
                        else {
                                             // tblIndexingTop.constant = 0
                                    tblIndexingTop.constant = 65
                                }
                                          
                                          BtnSkillBadge.isHidden = false
                                          BtnTeamBadges.isHidden = false
                                          BtnCharBadges.isHidden = true
                                          btn1Width.constant = 90
                                          
                                          btnLeadingOutlet.constant = 0
                                          btn2Width.constant = 120
                                          btn2Trailing.constant = 0
                                          BtnSkillBadge.setImage(UIImage(), for: .normal)
                                          BtnTeamBadges.setImage(UIImage(), for: .normal)
                                          BtnSkillBadge.setTitle("Friends", for: .normal)
                                          
                                          BtnSkillBadge.backgroundColor = UIColor.red
                                          BtnSkillBadge.setTitleColor(UIColor.white, for: .normal)
                                          BtnTeamBadges.backgroundColor = UIColor.clear
                                          BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                                          BtnTeamBadges.setTitle("Friend Requests", for: .normal)
                                          BtnTeamBadges.setRadius(radius: 5)
                                          BtnSkillBadge.setRadius(radius: 5)
                                          
                                          //vwPopup2ndView.isHidden = true
                                         // CollectionViewFootBoll.isHidden = true
                                          //CollectionCoinsView.isHidden = true
                                          collectionGallery.isHidden = true
                                  
//                                          tblHistory.isHidden = true
                                         tblViewPlayers.isHidden = true
                                         tableIndexingView.isHidden = false
                                          tableIndexingView.reloadData()
                    }
                    else {
                        collectionGallery.isHidden = true
                    }
                }
                    
                else if collectionView == collectionGallery {
                
                       let cell = collectionGallery.cellForItem(at: indexPath) as! ProfileGalleryCollectionCell
                    
                     self.imageTapped(image: cell.imgGallery.image!)
                }
                
            }
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
            {
                if collectionView == myCollectionView {
                   // return  CGSize(width: collectionView.frame.width/5 - 10, height: 50)
                     return  CGSize(width: collectionView.frame.width/3 - 10, height: 40)
                }
                    
               
                    
                else if collectionView == collectionGallery {
                    if BoolIs {
                        
                        return  CGSize(width: collectionView.frame.width/3 - 10, height: collectionView.frame.width/3 + 30 )
                        
                    }
                        
                    else {
                        
                        return  CGSize(width: collectionView.frame.width/1 - 10, height: collectionView.frame.width/1 - 10)
                   
                    }
                    
                }
                    
                else {
                    
                   // return  CGSize(width: collectionView.frame.width/3 - 10, height: 104)
                    return  CGSize(width: collectionView.frame.width/3 - 10, height: 150)
                }
            }
    
    @IBAction func btnAddFriendAction(_ sender: UIButton) {
             if sender.currentTitle == "Cancel request" {
                 addCancelFriendApi()
             }
                 
             else {
                 addFriendApi()
             }
         }
    func addFriendApi()
        {
            let parameters = ["To_ID" : id ,
                              "Form_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.ADD_FRIENDS,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            let arrDict = result["data"] as? [String:Any] ?? [:]
                            print("Data: \(arrDict)")
                            self.btnAddFriendOutlet.setTitle("Cancel request", for: .normal)
                        }
                        showToast(uiview: self, msg: result.getString(key: "message"))
            })
        }
    
    func addCancelFriendApi()
      {
          let parameters = ["To_ID" : id ,
                            "Form_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
              ] as [String : Any]
          
          let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                        "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                        ] as [String : Any]
          
          callApi(ServiceList.SERVICE_URL+ServiceList.APP_CANCLE_FRIEND,
                  method: .post,
                  param: parameters ,
                  extraHeader: header ,
                  completionHandler: { (result) in
                      print(result)
                      if result.getBool(key: "status")
                      {
                          let arrDict = result["data"] as? [String:Any] ?? [:]
                          print("Data: \(arrDict)")
                          self.btnAddFriendOutlet.setTitle("Add friend", for: .normal)
                      }
                      showToast(uiview: self, msg: result.getString(key: "message"))
          })
      }
    
    
     //MARK:- Web service
    
    func APP_FRIENDS_REQUEST_GET()
    {
        let header = ["USER-ID": id /* UserDefaults.standard.getUserDict()["id"] as? String ?? "" */,
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_FRIENDS_REQUEST_GET,
                method: .post,
                param: ["User_ID" : id ] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                  //  self.friendGetList.removeAll()
                    self.names.removeAll()
                    self.sections.removeAll()
                    self.sortedFirstLetters.removeAll()
                    
                    if result.getBool(key: "status")
                    {
                        self.friendRequestList = result["data"] as? [[String:Any]] ?? []
                        
                        self.names.removeAll()
                        for i in 0..<self.friendRequestList.count {
                            
                            let name = self.friendRequestList[i]["FirstName"] as? String ?? ""
                            let homeTown  = self.friendRequestList[i]["Hometown"] as? String ?? ""
                            let icon = self.friendRequestList[i]["UserProfile"] as? String ?? ""
                            let friendsId = self.friendRequestList[i]["Friends_ID"] as? String ?? ""
                            let Score = self.friendRequestList[i]["Score"] as? String ?? ""
                            
                            self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown,Score: Score))
                           // self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown))
                        }
                        
                        let firstLetters = self.names.map { $0.titleFirstLetter }
                        let uniqueFirstLetters = Array(Set(firstLetters))
                        
                        self.sortedFirstLetters = uniqueFirstLetters.sorted()
                        self.sections = self.sortedFirstLetters.map { firstLetter in
                            return self.names
                                .filter { $0.titleFirstLetter == firstLetter }
                                .sorted { $0.nameTitle < $1.nameTitle }
                        }
                        
                    }
                    
                    DispatchQueue.main.async {
                        self.tableIndexingView.reloadData()
                    }
        })
    }
    

    
    
    func APP_MY_FRIEND()
        {
            
            let parameters = ["User_ID" : id
                /* UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any  */]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_MY_FRIEND,
                    method: .post,
                    param: parameters,
                    extraHeader: header,
                    completionHandler: { (result) in
                        print(result)
                        
                        self.names.removeAll()
                        self.sections.removeAll()
                        self.sortedFirstLetters.removeAll()
                        
                        if result.getBool(key: "status")
                        {
                            self.friendGetList = result["data"] as? [[String:Any]] ?? []
                            
                            for i in 0..<self.friendGetList.count {
                                
                                let name = self.friendGetList[i]["FirstName"] as? String ?? ""
                                let homeTown  = self.friendGetList[i]["Hometown"] as? String ?? ""
                                let icon = self.friendGetList[i]["UserProfile"] as? String ?? ""
                                let friendsId = self.friendGetList[i]["Friends_ID"] as? String ?? ""
                                let Score = self.friendGetList[i]["Score"] as? String ?? ""
                                
                                self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown,Score: Score))
                              //  self.names.append(Name(nameTitle: name, image: icon, friendId: friendsId, homeTown: homeTown))
                            }
                            
                            let firstLetters = self.names.map { $0.titleFirstLetter }
                            let uniqueFirstLetters = Array(Set(firstLetters))
                            
                            self.sortedFirstLetters = uniqueFirstLetters.sorted()
                            self.sections = self.sortedFirstLetters.map { firstLetter in
                                return self.names
                                    .filter { $0.titleFirstLetter == firstLetter }
                                    .sorted { $0.nameTitle < $1.nameTitle }
                            }
                            
                        }
                        
                        DispatchQueue.main.async {
                            self.tableIndexingView.reloadData()
                        }
            })
        }
    func APP_USER_PROFILE_DETAIL()
          {
              var getId = ""
              
              let userId = UserDefaults.standard.getUserDict()["id"] as? String ?? ""
              
              if userId == id {
                  getId = "0"
              }
                  
              else {
                  getId = id
              }
              
              let parameters = ["FromID" : getId ,
                                "UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                  ] as [String : Any]
              
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                    "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
              
              callApi(ServiceList.SERVICE_URL+ServiceList.APP_USER_PROFILE ,
                      method: .post ,
                      param: parameters ,
                      extraHeader: header ,
                      completionHandler: { (result) in
                          print(result)
                          if result.getBool(key: "status")
                          {
                              self.playerDetailResult = result["data"] as? [[String:Any]] ?? []
                              print("playerDetailResult: \(self.playerDetailResult)")
                              self.setDataPlayerDetail()
                          }
              })
          }
          
          func setDataPlayerDetail()
          {
              GetLOCId = playerDetailResult[0]["CLVLTitle"] as? String ?? ""
              
              print("GetLOCId: \(GetLOCId ?? "")")
              lblLOC.text = "LOC: \(GetLOCId ?? "")"
              
              lblUserName.text = playerDetailResult[0]["username"] as? String
              lblHomeTown.text = "Home Town : \(playerDetailResult[0]["Hometown"] as? String ?? "")"
              lblBadge.text = playerDetailResult[0]["Points"] as? String ?? ""
              lblorganizationanme.text = playerDetailResult[0]["FirstName"] as? String ?? ""
              lblorganizationemail.text = playerDetailResult[0]["website"] as? String ?? ""
            //  lblScore.text = playerDetailResult[0]["Total_Points"] as? String ?? ""
             // lblScore.text = playerDetailResult[0]["Score"] as? String ?? ""
           //   lblPoint.text = playerDetailResult[0]["Points"] as? String ?? ""
              let school = "\(playerDetailResult[0]["RoleName"] as? String ?? "")"
              
              let heightFt = playerDetailResult[0]["HeightFT"] as? String ?? ""
              let HeightInch = playerDetailResult[0]["HeightInch"] as? String ?? ""
              
              lblHeight.text = "Height: \(heightFt)′ \(HeightInch)″"
          
              //6′ 0″
          
             let status = playerDetailResult[0]["status"] as? Int
                         
                         if status == 0 {
                             btnAddFriendOutlet.setTitle("Add Friend", for: .normal)
                         }
                             
                         else if status == 1 {
                             btnAddFriendOutlet.setTitle("Cancel request", for: .normal)
                         }
                             
                         else {
                             btnAddFriendOutlet.setTitle("Friend", for: .normal)
                         }
              
              
              if school != "" {
                  lblSchool.text = "\(playerDetailResult[0]["RoleName"] as? String ?? "")"
              }
                  
              else {
                  lblSchool.isHidden = true
                 // lblLOC.isHidden = true
              }
              
              let imgURL = ServiceList.IMAGE_URL + "\(playerDetailResult[0]["UserProfile"] as? String ?? "")"
          
               imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
               imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
               imgUser.setRadius(radius: imgUser.frame.height/2)
               let imageView = RoundedImageView()
               imageView.image = imgUser.image
          }
          
          func getLOCList()
          {
              callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOC_LIST,
                      method: .get,
                      completionHandler: { (result) in
                          print(result)
                          if result.getBool(key: "status")
                          {
                              self.levelOfCompositionList = result["data"] as? [[String:Any]] ?? []
                              
                              for i in 0..<self.levelOfCompositionList.count {
                                  if self.GetLOCId == self.levelOfCompositionList[i]["CLVLID"] as? String ?? ""
                                  {
                                      self.lblLOC.text = "Loc: \(self.levelOfCompositionList[i]["CLVLDesc"] as? String ?? "")"
                                      print("CLVLID: \(self.levelOfCompositionList[i]["CLVLDesc"] as? String ?? "")")
                                  }
                              }
                          }
              })
          }
    
       func addPhotoApi(image: UIImage = UIImage(), video: NSURL = NSURL())
            {
                
                var imgdata = Data()
            
    //            let parameters = ["PlayerID" : id ,
    //                              "Type" : "image"
    //                ] as [String : Any]
                
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                              ] as [String : Any]
            
            
                if video.absoluteString == "" {
                    let parameters = ["PlayerID" : id ,
                                      "Type" : "image"
                        ] as [String : Any]
                    
                    imgdata = image.jpegData(compressionQuality: 0.5)!
                    
                    self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PHOTO_ADD,
                                 method: .post,
                                 param: parameters ,
                                 extraHeader: header ,
                                 data: [imgdata] ,
                                 dataKey: ["Gallery_Image"]) { (result) in
                                    
                                    print(result)
                                    
                                    if result.getBool(key: "status")
                                    {
                                        self.appGetPhoto()
                                    }
                                    
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                    }
                }
                    
                else {
                    
                    let parameters = ["PlayerID" : id ,
                                      "Type" : "Video"
                        ] as [String : Any]
                    
                    self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PHOTO_ADD,
                                 method: .post,
                                 param: parameters ,
                                 extraHeader: header ,
                                 data1: [video as URL] ,
                                 dataKey: ["Gallery_Image"]) { (result) in
                                    
                                    print(result)
                                    
                                    if result.getBool(key: "status")
                                    {
                                        self.appGetPhoto()
                                    }
                                    
                                  showToast(uiview: self, msg: result.getString(key: "message"))
                    }
                }
                
            }
            
    @objc func btnCellConfirmFriendAction(_ sender : UIButton)
           {
               let section = sender.tag / 100
               let row = sender.tag % 100
               let indexPath = NSIndexPath(row: row, section: section)
               
               let name = sections[indexPath.section][indexPath.row]
               
               if sender.currentTitle == "Confirm" {
                   self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "APPROVE")
               }
                   
               else {
                   self.APP_UN_FRIENDS(Friends_ID: name.friendId)
               }
               
            
            
               //        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Do you want to buy this product?")
               //
               //        let action1 = EMAlertAction(title: "Yes", style: .cancel)
               //        {
               //            self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "APPROVE")
               //        }
               //
               //        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
               //
               //        }
               //
               //        alert.addAction(action: action1)
               //        alert.addAction(action: action2)
               //
               //        self.present(alert, animated: true, completion: nil)
           }
           
           @objc func btnCellDeclineAction(_ sender : UIButton)
           {
               let section = sender.tag / 100
               let row = sender.tag % 100
               let indexPath = NSIndexPath(row: row, section: section)
               
               let name = sections[indexPath.section][indexPath.row]
               self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "REJECT")
               
               //        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Do you want to buy this product?")
               //
               //        let action1 = EMAlertAction(title: "Yes", style: .cancel)
               //        {
               //            self.APP_FRIENDS_ACTION(Friends_ID: name.friendId, Action: "REJECT")
               //        }
               //
               //        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
               //        }
               //
               //        alert.addAction(action: action1)
               //        alert.addAction(action: action2)
               //
               //        self.present(alert, animated: true, completion: nil)
           }
    func APP_FRIENDS_ACTION(Friends_ID: String, Action: String)
         {
             let parameters = ["Friends_ID" : Friends_ID , "Action" : Action] as [String : Any]
             
             let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                           ] as [String : Any]
             
             callApi(ServiceList.SERVICE_URL+ServiceList.APP_FRIENDS_ACTION,
                     method: .post,
                     param: parameters,
                     extraHeader: header,
                     completionHandler: { (result) in
                         print(result)
                         if result.getBool(key: "status")
                         {
                             self.APP_FRIENDS_REQUEST_GET()
                             showToast(uiview: self, msg: result.getString(key: "message"))
                         }
             })
         }
    
    
         func APP_UN_FRIENDS(Friends_ID: String)
         {
             let parameters = ["Friends_ID" : Friends_ID] as [String : Any]
             
             let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                           ] as [String : Any]
             
             callApi(ServiceList.SERVICE_URL+ServiceList.APP_UN_FRIENDS,
                     method: .post,
                     param: parameters,
                     extraHeader: header,
                     completionHandler: { (result) in
                         print(result)
                         if result.getBool(key: "status")
                         {
                             self.APP_MY_FRIEND()
                             showToast(uiview: self, msg: result.getString(key: "message"))
                         }
             })
         }
    
    func appGetPhoto()
           {
               let parameters = ["PlayerID" : id , "To_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                   ] as [String : Any]
               
               let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                             "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""] as [String : Any]
               
               callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_PHOTO,
                       method: .post,
                       param: parameters ,
                       extraHeader: header ,
                       completionHandler: { (result) in
                           print(result)
                           self.getPhotoDict.removeAll()
                           
                           if result.getBool(key: "status")
                           {
                               self.getPhotoDict = result["data"] as? [[String:Any]] ?? []
                               
                               print("Photo_ID: \(self.getPhotoDict)")
                               
                               DispatchQueue.main.async {
                                 self.collectionGallery.reloadData()
                               }
                           }
                               
                           else {
                              // showToast(uiview: self, msg: result.getString(key: "message"))
                           }
               })
           }
        
//    func playerListApi()
//       {
//           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
//                         ] as [String : Any]
//
//           callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_LIST,
//                   method: .get,
//                   param: [:] ,
//                   extraHeader: header ,
//                   completionHandler: { (result) in
//                       print(result)
//                       if result.getBool(key: "status")
//                       {
//                           self.playerList = result["data"] as? [[String:Any]] ?? []
//                            for i in 0..<self.playerList.count {
//
//                               let id = self.playerList[i]["id"] as? String ?? ""
//
//                               if id != UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
//                                   let PlayerName = self.playerList[i]["username"] as? String ?? ""
//                                   let School = self.playerList[i]["School"] as? String ?? ""
//                                   let imgUser = self.playerList[i]["UserProfile"] as? String ?? ""
//                                   let Hometown = self.playerList[i]["Hometown"] as? String ?? ""
//                                   let CheckedIn = self.playerList[i]["CheckedIn"] as? String ?? ""
//                                   self.playerListArray.append(Player(imgUser: imgUser, PlayerName: PlayerName, School: School, Location: Hometown, CheckedIn: CheckedIn))
//                               }
//                           }
//                           self.dataPlayer = self.playerListArray
//                           self.tblViewPlayers.reloadData()
//                       }
//           })
//       }
    func Player_Wise_Event()
       {
        
        
        
        
           let parameters = ["PlayerID" : id
             
               ] as [String : Any]
           
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]
           
           self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_WISE_EVENT,
                        method: .post,
                        param: parameters,
                        extraHeader: header) { (result) in
                        
                            print(result)
                           
                           if result.getBool(key: "status")
                           {
                               self.eventList = result["data"] as? [[String : Any]] ?? []
                           }
                           
                           self.tblViewPlayers.reloadData()
                          // showToast(uiview: self, msg: result.getString(key: "message"))
           }
       }
    
    
    func APP_FAVOURITE(To_ID: String, Form_ID: String, Photo_ID: String)
         {
             let parameters = ["To_ID" : To_ID , "Form_ID" : Form_ID, "Photo_ID" : Photo_ID] as [String : Any]
             
             let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                           ] as [String : Any]
             
             callApi(ServiceList.SERVICE_URL+ServiceList.APP_FAVOURITE,
                     method: .post,
                     param: parameters,
                     extraHeader: header,
                     completionHandler: { (result) in
                         print(result)
                         if result.getBool(key: "status")
                         {
                             
                             self.appGetPhoto()
                             showToast(uiview: self, msg: result.getString(key: "message"))
                         }
             })
         }
     
     func APP_DELETE_FAVOURITE(To_ID: String, Form_ID: String, Photo_ID: String)
     {
         let parameters = ["To_ID" : To_ID , "Form_ID" : Form_ID, "Photo_ID" : Photo_ID] as [String : Any]
         
         let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                       "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                       ] as [String : Any]
         
         callApi(ServiceList.SERVICE_URL+ServiceList.APP_FAVOURITE_DELETE,
                 method: .post,
                 param: parameters,
                 extraHeader: header,
                 completionHandler: { (result) in
                     print(result)
                     if result.getBool(key: "status")
                     {
                         self.appGetPhoto()
                         showToast(uiview: self, msg: result.getString(key: "message"))
                     }
         })
     }
    
    @objc func btnLikeAction(button: UIButton) {
        let buttonRow = button.tag
     //   tappedButtonsTags.append(button.tag)
             
       // let total = getPhotoDict[button.tag]["total"] as? String ?? ""
        
         let PlayerID = getPhotoDict[button.tag]["PlayerID"] as? String ?? ""
        if PlayerID == UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
            showToast(uiview: self, msg: "You can not like own post")
        }
        else
        {
            let status = getPhotoDict[button.tag]["status"] as? String ?? ""
                
                if status == "0" {
                    PhotoID = getPhotoDict[button.tag]["PhotoID"] as? String ?? ""
                    APP_FAVOURITE(To_ID: UserDefaults.standard.getUserDict()["id"] as? String ?? "", Form_ID: id, Photo_ID: PhotoID)
                }
                    
                else {
                     PhotoID = getPhotoDict[button.tag]["PhotoID"] as? String ?? ""
                     APP_DELETE_FAVOURITE(To_ID: UserDefaults.standard.getUserDict()["id"] as? String ?? "", Form_ID:  id, Photo_ID: PhotoID)
                }
            
                print("Like Clicked: \(buttonRow)")
        }
       
    }
        
            @objc func btnVideoAction(button: UIButton) {
                 button.isHidden = false
                let type = getPhotoDict[button.tag]["Type"] as? String ?? ""
                
                if type != "image" {
                    var videoURL = getPhotoDict[button.tag]["Photo"] as? String ?? ""
                    videoURL = ServiceList.IMAGE_URL + videoURL
                    let url : URL = URL(string: videoURL)!
                    let player = AVPlayer(url: url)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                        // playerViewController.view.semanticContentAttribute = .forceRightToLeft
                    }
                }
               
            }
        
            @objc func btnCommentAction(button: UIButton) {
                let buttonRow = button.tag
                PhotoID = getPhotoDict[buttonRow]["PhotoID"] as? String ?? ""
                
                let myalert = UIAlertController(title: "", message: "Are you sure want to delete photo?", preferredStyle: UIAlertController.Style.alert)

                myalert.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        print("Selected")
                    let parameters = ["PhotoID" : self.PhotoID ,
                                             ] as [String : Any]
                                         
                                         let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                       "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                                       ] as [String : Any]
                                         
                    self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PHOTO_DELETE,
                                                 method: .post,
                                                 param: parameters ,
                                                 extraHeader: header ,
                                                 completionHandler: { (result) in
                                                     print(result)
                                                     if result.getBool(key: "status")
                                                     {
                                                       self.appGetPhoto()
                                                     }
                                                   else
                                                     {
                                                       
                                                   }
                                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                         })
                    })
                myalert.addAction(UIAlertAction(title: "CANCEL", style: .cancel) { (action:UIAlertAction!) in
                        print("Cancel")
                    })

                    self.present(myalert, animated: true)
                
               
                
    //            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailView") as! ChatDetailView
    //            nextViewController.dic_DataDetail = getPhotoDict[buttonRow]
    //            nextViewController.currentUserName = lblUserName.text ?? ""
    //            nextViewController.currentUserimg = playerDetailResult[0]["UserProfile"] as? String ?? ""
    //            self.navigationController?.pushViewController(nextViewController, animated: true)
    //            print("Comment Clicked: \(buttonRow)")
            }
    
    //MARK:- BUTTON ACTIONS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if isfromnotification {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSettingAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingView") as! SettingView
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func btn_share(_ sender: Any) {
     
        let strShareLink = "Hey! Your first 10 points are waiting for you! Join The RUN App, the first Sports Social network where players compete in real life and online.  Use this link to download the app and register to start earning points today.#LetTheGameBegin"
              
        let strJoinCode = "\nhttps://apps.apple.com/us/app/the-run-app-1-0/id1522900307"
                   
                    
                    
                    let strfinal = "\(strShareLink)\(strJoinCode)"
                     
                    let arrShareItem = [strfinal]
                    let activityViewController = UIActivityViewController(activityItems: arrShareItem,applicationActivities: nil)
                       activityViewController.popoverPresentationController?.sourceView = self.view
                    if let popoverController = activityViewController.popoverPresentationController {
                                                                        popoverController.sourceView = self.view //to set the source of your alert
                                                                                      popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                                                                                                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                                                                                            }
                       present(activityViewController, animated: true, completion: nil)
                                  
                       
    }
    
    @IBAction func btn_feed(_ sender: Any) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func btnAddLocationAction(_ sender: UIButton) {
           
               let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
               self.navigationController?.pushViewController(nextViewController, animated: true)
           }
    
    @IBAction func btnChatAction(_ sender: UIButton) {
//               let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OpenChatView") as! OpenChatView
//               nextViewController.LocationID = playerDetailResult[0]["LocationID"] as? String ?? ""
//            //   nextViewController.commentData = playerDetailResult[0]
//               self.navigationController?.pushViewController(nextViewController, animated: true)
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatListView") as! ChatListView
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
           }
    
    @IBAction func Btn1Clicked(_ sender: UIButton) {
            
            if sender.currentTitle == "Friends" {
                  BoolFriendIs = true
                BtnTeamBadges.backgroundColor = UIColor.clear
                BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                BtnSkillBadge.backgroundColor = UIColor.red
                BtnSkillBadge.setTitleColor(UIColor.white, for: .normal)
                APP_MY_FRIEND()
            }
                
                
            else {
                
                BoolIs = true
                BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                BtnTeamBadges.imageView?.changeImageViewImageColor(color: UIColor.darkGray)
                
                
                if collectionGallery.isHidden == false
                {
                    BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    collectionGallery.reloadData()
                }
                    
                else
                {
                    BtnSkillBadge.backgroundColor = UIColor.red
                    BtnSkillBadge.setTitleColor(UIColor.white, for: .normal)
                    BtnTeamBadges.backgroundColor = UIColor.clear
                    BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
                    BtnCharBadges.backgroundColor = UIColor.clear
                    BtnCharBadges.setTitleColor(UIColor.red, for: .normal)
                    collectionGallery.isHidden = true
                }
                
            } // End Of Else
            
        }
        
        @IBAction func btnCharAction(_ sender: UIButton) {
            
            if sender.hasImage(named: "plus1.png", for: .normal) ||
                sender.hasImage(named: "plus-button", for: .normal) {
                print("YES")
                 self.showAttachmentActionSheet1(vc: self)
               // openImageVideoSelectionPicker(picker: picker)
                
            } else {
                print("No")
                sender.backgroundColor = UIColor.red
                sender.setTitleColor(UIColor.white, for: .normal)
                BtnSkillBadge.backgroundColor = UIColor.clear
                BtnSkillBadge.setTitleColor(UIColor.red, for: .normal)
                BtnTeamBadges.backgroundColor = UIColor.clear
                BtnTeamBadges.setTitleColor(UIColor.red, for: .normal)
            }
            
        }
        
        @IBAction func btn2Clicked(_ sender: UIButton) {
            
            if sender.currentTitle == "Friend Requests" {
                BoolFriendIs = false
                BtnSkillBadge.backgroundColor = UIColor.clear
                BtnSkillBadge.setTitleColor(UIColor.red, for: .normal)
                BtnTeamBadges.backgroundColor = UIColor.red
                BtnTeamBadges.setTitleColor(UIColor.white, for: .normal)
                 APP_FRIENDS_REQUEST_GET()
                print("Btn2 Clicked")
                
            }
                
            else {
                
                BoolIs = false
                BtnTeamBadges.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                BtnSkillBadge.imageView?.changeImageViewImageColor(color: UIColor.darkGray)
                
                
                if collectionGallery.isHidden == false
                {
                    BtnTeamBadges.imageView?.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
                    collectionGallery.reloadData()
                }
                    
                else
                {
                    BtnTeamBadges.backgroundColor = UIColor.red
                    BtnTeamBadges.setTitleColor(UIColor.white, for: .normal)
                    BtnSkillBadge.backgroundColor = UIColor.clear
                    BtnSkillBadge.setTitleColor(UIColor.red, for: .normal)
                    BtnCharBadges.backgroundColor = UIColor.clear
                    BtnCharBadges.setTitleColor(UIColor.red, for: .normal)
                    collectionGallery.isHidden = true
                }
                
            } // End Of Else
            
        }
    
         //MARK:- Image Action
        func imageTapped(image:UIImage){
            let newImageView = UIImageView(image: image.aspectFittedToHeight(UIScreen.main.bounds.height))
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(OrganizationProfile.dismissFullscreenImage(_:)))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
    //        self.navigationController?.isNavigationBarHidden = true
    //        self.tabBarController?.tabBar.isHidden = true
        }
        
        @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
    //        self.navigationController?.isNavigationBarHidden = false
    //        self.tabBarController?.tabBar.isHidden = false
            sender.view?.removeFromSuperview()
        }

    //MARK:- Thumbnail From path
    func getThumbnailFrom(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
            
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }

}

class indexingTableCell1: UITableViewCell {
    @IBOutlet weak var BtnConfirmFriend: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLast: UILabel!
    @IBOutlet weak var imagUser: UIImageView!
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var btnTopConfirm: NSLayoutConstraint!
    
    @IBOutlet weak var btnDeclineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnDeclineBotom: NSLayoutConstraint!
    
    override func awakeFromNib()
   
    {
        lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLast.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
    
}



extension OrganizationProfile: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   // fileprivate var currentVC: UIViewController?
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController,
                                     didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print("image picked")
             self.addPhotoApi(image: image)
            //self.imagePickedBlock?(image)
            
        } else{
            print("Something went wrong in  image")
        }
        
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL{
            print("videourl: ", videoUrl)
            //trying compression of video
            let data = NSData(contentsOf: videoUrl as URL)!
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            compressWithSessionStatusFunc(videoUrl)
        }
        else{
            print("Something went wrong in  video")
        }
        
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                   // self.videoPickedBlock?(compressedURL as NSURL)
                    self.addPhotoApi(video:compressedURL as NSURL)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    //MARK: - showAttachmentActionSheet
    // This function is used to show the attachment sheet for image, video, photo and file.
    func showAttachmentActionSheet1(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: AttachmentHandler.Constants.actionFileTypeHeading, message: AttachmentHandler.Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.video, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
            
        }))
        
//        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.file, style: .default, handler: { (action) -> Void in
//            self.documentPicker()
//        }))
        
        actionSheet.addAction(UIAlertAction(title: AttachmentHandler.Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - CAMERA PICKER
    //This function is used to open camera from the iphone and
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    func authorisationStatus(attachmentTypeEnum: AttachmentHandler.AttachmentType, vc: UIViewController) {
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentHandler.AttachmentType.camera {
                openCamera()
            }
            if attachmentTypeEnum == AttachmentHandler.AttachmentType.photoLibrary {
                photoLibrary()
            }
            
            if attachmentTypeEnum == AttachmentHandler.AttachmentType.video {
                videoLibrary()
            }
            
        case .denied:
            print("permission denied")
            //self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentHandler.AttachmentType.camera {
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentHandler.AttachmentType.photoLibrary {
                        self.photoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentHandler.AttachmentType.video {
                        self.videoLibrary()
                    }
                } else{
                    print("restriced manually")
                    //self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            //self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
}
