//
//  SignUpOrganizationView.swift
//  RunApp
//
//  Created by My Mac on 06/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CoreLocation
import SDWebImage
import NightNight




class SignUpOrganizationView: UIViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   
    @IBOutlet var baseview: UIView!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var lbltermscondition: UILabel!
   // @IBOutlet weak var btntick: UIButton!
    
    
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var imgOutlet: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtContactNo: UITextField!
    @IBOutlet weak var btnUserTypeOutlet: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPasword: UITextField!
    @IBOutlet weak var txtHomeTown: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet var txtwebsite: UITextField!
    
    @IBOutlet weak var btnCheckOutlet: UIButton!
    @IBOutlet weak var signUpOutlet: UIButton!
    var picker = UIImagePickerController()
    
    var isImage = Bool()
    var Latitude = Double()
    var Longitude = Double()
    var userTypeList = [[String : Any]]()
    
    @IBOutlet weak var lblStar: UILabel!
    var strUserTypeId,GetUserTypeId : String?
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var lblStarConfirm: UILabel!
    @IBOutlet weak var lblStarPassword: UILabel!
    @IBOutlet weak var txtHeight: NSLayoutConstraint!
    @IBOutlet weak var topHeight: NSLayoutConstraint!
    
    @IBOutlet weak var topHeightConfirmPassword: NSLayoutConstraint!
    var dictEdit = [[String : Any]]()
    @IBOutlet weak var lblChangeOutlet: UILabel!
    
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        baseview.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lblChangeOutlet.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        btnback.setMixedImage(MixedImage(normal:"left-arrow-B", night:"left-arrow"), forState: .normal)
        lblChangeOutlet.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
           txtName.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
             txtEmail.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
             txtHomeTown.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
              txtPasword.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
             txtContactNo.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
             txtConfirmPassword.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtName.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtEmail.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtHomeTown.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtPasword.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtContactNo.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtConfirmPassword.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
             lbltermscondition.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        btnCheckOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
        btnCheckOutlet.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnCheckOutlet.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        
        btnUserTypeOutlet.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
        btnUserTypeOutlet.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnUserTypeOutlet.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        
        txtwebsite.layer.mixedBorderColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        txtwebsite.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
       
        if dictEdit.count > 0 {
            lblChangeOutlet.text = "Update Account"
            self.view.layoutIfNeeded()
            txtHeight.constant = 0
            topHeight.constant = 0
            topHeightConfirmPassword.constant = 0
            lblStarPassword.isHidden = true
            lblStarConfirm.isHidden = true
            self.view.layoutIfNeeded()
            
            signUpOutlet.setTitle("Update Profile", for: .normal)
            let imgURL = ServiceList.IMAGE_URL + dictEdit[0].getString(key: "UserProfile")
            print("imgURL: \(imgURL)")
            if imgURL != "" {
                imgOutlet.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                      completed: nil)
                isImage = true
            }
            
            txtEmail.isEnabled = false
            txtEmail.isUserInteractionEnabled = false
        
            txtName.text = dictEdit[0].getString(key: "FirstName")
            txtContactNo.text = dictEdit[0].getString(key: "ContactNo")
           // txtPasword.text = dictEdit[0].getString(key: "password")
            txtHomeTown.text = dictEdit[0].getString(key: "Hometown")
           // txtConfirmPassword.text = dictEdit[0].getString(key: "FirstName")
            txtEmail.text = dictEdit[0].getString(key: "username")
            btnCheckOutlet.titleLabel?.text = dictEdit[0].getString(key: "username")
            btnUserTypeOutlet.titleLabel?.text = dictEdit[0].getString(key: "OrganizationType")
            strUserTypeId = dictEdit[0].getString(key: "RoleID")
            txtwebsite.text = dictEdit[0].getString(key: "website")
            //PossitionID, DefaultDistance
        }
        
        //btnUserTypeOutlet.isHidden = true
        //lblStar.isHidden = true
       // imgRightArrow.isHidden = true
        btnCheckOutlet.accessibilityLabel = "off"
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedHide(tapGestureRecognizer:)))
        imgOutlet.isUserInteractionEnabled = true
        imgOutlet.addGestureRecognizer(tapGestureRecognizer)
        btnCamera.setRadius(radius: btnCamera.frame.height/2)
        imgOutlet.setRadius(radius: imgOutlet.frame.height/2)
        
        btnCheckOutlet.setRadius(radius: 5)
        btnCheckOutlet.layer.borderWidth = 2.0
        btnUserTypeOutlet.setRadius(radius: 10)
        btnUserTypeOutlet.layer.borderWidth = 2.0
        signUpOutlet.setRadius(radius: signUpOutlet.frame.height/2)
        
        for view in vwMain.subviews as [UIView] {
            if let txt = view as? UITextField {
               // txt.setRadius(radius: 10)
                txt.layer.cornerRadius = 10
                 txt.layer.masksToBounds = true
               txt.layer.borderWidth = 2.0
//                txt.setRadiusBorder(color: UIColor.white)
                txt.setLeftPaddingPoints(15,strImage:"")
                
            }
        }

         picker.delegate = self

        getUserTypeList()
    }
    
    //MARK:- Gesture
    
    @objc func imageTappedHide(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
        openGalleryCameraPicker(picker: picker)
    }
    
    //MARK:- Image Picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.originalImage] as? UIImage {
            self.imgOutlet.contentMode = .scaleAspectFill
            self.imgOutlet.layer.masksToBounds = true
            self.imgOutlet.image = image
            self.isImage = true
        }
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    /*
     if let mediaType = info[UIImagePickerControllerMediaType] as? String {
     
     if mediaType  == "public.image" {
     print("Image Selected")
     }
     
     if mediaType == "public.movie" {
     print("Video Selected")
     }
     }
     */
    
    
    //MARK:- Textfield Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- Web service
    func getUserTypeList()
    {
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_ORGANIZATION_ROLE,
                method: .get,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.userTypeList = result["data"] as? [[String:Any]] ?? []
                    }
                    
                    if result.getBool(key: "status")
                                       {
                                           self.userTypeList = result["data"] as? [[String:Any]] ?? []
                                           for i in 0..<self.userTypeList.count {
                                               if self.strUserTypeId == self.userTypeList[i]["RoleID"] as? String ?? ""
                                               {
                                                   print("Matched")
                                                   self.btnUserTypeOutlet.setTitle("\(self.userTypeList[i]["RoleName"] as? String ?? "")", for: .normal)
                                               }
                                           }
                                       }
        })
    }

     @IBAction func btn_termsandconditions(_ sender: Any) {
    
         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionViewController") as! TermsAndConditionViewController
         self.navigationController?.pushViewController(nextViewController, animated: true)
         
     }
    // let address = "Rio de Janeiro, Brazil"
    //let address = "8787 Snouffer School Rd, Montgomery Village, MD 20879"
    
    func RegisterOrganationService()
    {
        
         var parameters:[String : Any]
        
         if (StoredData.shared.playerId != nil) {
            parameters = ["OrganizationName" : txtName.text!,
                          "Password" : txtPasword.text!,
                          "EmailID" : txtEmail.text!,
                          "Address": txtHomeTown.text!,
                          "ContactNo" : txtContactNo.text!,
                          "ConfirmPassword" : txtConfirmPassword.text! ,
                          "Latitude" : Latitude ,
                          "Longitude" : Longitude ,
                          "Player_ID" : StoredData.shared.playerId!,
                           "RoleID" : strUserTypeId!,
                          "Website" : txtwebsite.text ?? ""

                ] as [String : Any]
        }
            
         else {
            parameters = ["OrganizationName" : txtName.text!,
                          "Password" : txtPasword.text!,
                          "EmailID" : txtEmail.text!,
                          "Address": txtHomeTown.text!,
                          "ContactNo" : txtContactNo.text!,
                          "ConfirmPassword" : txtConfirmPassword.text! ,
                          "Latitude" : Latitude ,
                          "Longitude" : Longitude ,
                          "Player_ID" : "1234",
                "RoleID" : strUserTypeId!,
                 "Website" : txtwebsite.text ?? ""
                ] as [String : Any]
        }
        
        let imgdata = imgOutlet.image?.jpegData(compressionQuality: 0.5)!
        
        self.callApi(ServiceList.SERVICE_URL+ServiceList.ORGANIZATION,
                     method: .post,
                     param: parameters ,
                       data: [imgdata!],
                       dataKey: ["Profile_Image"]) { (result) in
                        
                        print(result)
                        
                        if result.getBool(key: "status")
                        {
//                            var dict = result.getDictionary(key: "data")
//                                for (key, value) in dict {
//                                let val : NSObject = value as! NSObject;
//                                dict[key] = val.isEqual(NSNull()) ? "" : value
//                                }
//
//                            dict["login_token"] = result.getString(key: "login_token")
//                            dict["id"] = result.getString(key: "id")
//
//                            print(dict)
//                            UserDefaults.standard.setUserDict(value: dict)
//                            UserDefaults.standard.setIsLogin(value: true)
                            
//                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
//                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        
                        self.showDialougeWithTitle(andMessage: result.getString(key: "message"))
                        //showToast(uiview: self, msg: result.getString(key: "message"))
        }
    }
   
    func showDialougeWithTitle(andMessage: String)  {
        let alertController = UIAlertController(title: ConstantVariables.Constants.Project_Name, message:
            andMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func UpdateOrganationService()
    {
        
        let parameters = ["UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "OrganizationName" : txtName.text!,
                          "EmailID" : txtEmail.text!,
                          "Address": txtHomeTown.text!,
                          "ContactNo" : txtContactNo.text!,
                          "Latitude" : Latitude ,
                          "Longitude" : Longitude,
            "RoleID" : strUserTypeId!,
             "Website" : txtwebsite.text ?? ""
            ] as [String : Any]
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        let imgdata = imgOutlet.image?.jpegData(compressionQuality: 0.5)!
        
        self.callApi(ServiceList.SERVICE_URL+ServiceList.EDIT_ORGANIZATION,
                     method: .post,
                     param: parameters ,
                     extraHeader: header ,
                     data: [imgdata!] ,
                     dataKey: ["Profile_Image"]) { (result) in
                        
                        print(result)
                        
                        if result.getBool(key: "status")
                        {
                            self.ShowAlertUpdate(msg: result.getString(key: "message"), isTwoButton: false)
                        }
                            
                        else {
                           // showToast(uiview: self, msg: result.getString(key: "message"))
                            showAlert(uiview: self, msg: result.getString(key: "message"), isTwoButton: false)
                        }
        }
    }
    
    //MARK:- ALERT CONTROLLER
    func ShowAlertUpdate(msg : String , isTwoButton : Bool) {
    
        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: msg)
    
        let action1 = EMAlertAction(title: "Ok", style: .normal) {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
            // Perform Action
        }
        
        alert.addAction(action: action1)
        if isTwoButton {
            alert.addAction(action: action2)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- ViewCircular
    
    func makeViewCircular(view: UIView) {
        view.layer.cornerRadius = view.bounds.size.width / 2.0
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.white.cgColor
    }
    
    //MARK:- BUTTON METHODS
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCheckTermsAction(_ sender: UIButton) {
        if btnCheckOutlet.accessibilityLabel == "on"
        {
            btnCheckOutlet.accessibilityLabel = "off"
            btnCheckOutlet.setTitle("", for: .normal)
        }
            
        else
        {
            btnCheckOutlet.accessibilityLabel = "on"
            btnCheckOutlet.setTitle("✓", for: .normal)
        }
    }
    
    @IBAction func btnUserTypeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if userTypeList.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Select user type", rows: userTypeList.map({ $0["RoleName"] ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnUserTypeOutlet.setTitle("\(values!)", for: .normal)
                self.strUserTypeId = self.userTypeList[indexes]["RoleID"] as? String ?? ""
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        
        let btnTitle = sender.titleLabel?.text
        if btnTitle == "SIGN UP" {
             validation()
        }
            
        else {
             validation1()
        }
    }
    
    //MARK:- Other Method
    
    func validation()
    {
        self.view.endEditing(true)
       
        let name = txtName.text!
        
        if !isImage && txtName.text == "" && txtContactNo.text == "" && txtEmail.text == "" && txtPasword.text == "" && txtConfirmPassword.text == "" && txtHomeTown.text == "" && btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
         
        else if !isImage
        {
            showAlert(uiview: self, msg: "Please select profile image", isTwoButton: false)
        }
            
        else if txtName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter Organization name", isTwoButton: false)
        }
            
        else if name.count < 2
        {
            showAlert(uiview: self, msg: "Name should have minimum 2 characters And maximum 18 characters", isTwoButton: false)
        }
            
        else if name.count > 18
        {
            showAlert(uiview: self, msg: "Name should have maximum 18 characters", isTwoButton: false)
        }
            
        else if txtContactNo.text == ""
        {
            showAlert(uiview: self, msg: "Please Enter Contact No", isTwoButton: false)
        }
            
        else if txtEmail.text == ""
        {
            showAlert(uiview: self, msg: "Please enter email id", isTwoButton: false)
        }
            
        else if !txtEmail.isValidEmail()
        {
            showAlert(uiview: self, msg: "Please enter valid email id", isTwoButton: false)
        }
            
        else if txtPasword.text == ""
        {
            showAlert(uiview: self, msg: "Please enter password", isTwoButton: false)
        }
    
        else if txtConfirmPassword.text == ""
        {
            showAlert(uiview: self, msg: "Please enter confirm password", isTwoButton: false)
        }
            
        else if txtPasword.text?.lowercased() != txtConfirmPassword.text?.lowercased()
        {
            showAlert(uiview: self, msg: "Both password does not match", isTwoButton: false)
        }
            
        else if txtHomeTown.text == ""
        {
            showAlert(uiview: self, msg: "Please enter hometown", isTwoButton: false)
        }

        else if btnUserTypeOutlet.currentTitle == "Select user type"
        {
            showAlert(uiview: self, msg: "Please select user type", isTwoButton: false)
        }
        
        else if btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please accept terms & condition", isTwoButton: false)
        }
        
        else
        {
            let address = txtHomeTown.text!
            let geocoder = CLGeocoder()
            geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                if((error) != nil){
                    print("Error", error ?? "")
                }
                if let placemark = placemarks?.first {
                    let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                    self.Latitude = coordinates.latitude
                    self.Longitude = coordinates.longitude
                    print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                    
                }
                 self.RegisterOrganationService()
            })
            
        }
    }
    
    func validation1()
    {
        self.view.endEditing(true)
        
        let name = txtName.text!
        
        if !isImage && txtName.text == "" && txtContactNo.text == "" && txtEmail.text == "" && txtHomeTown.text == "" && btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
            
        else if !isImage
        {
            showAlert(uiview: self, msg: "Please select profile image", isTwoButton: false)
        }
            
        else if txtName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter Organization name", isTwoButton: false)
        }
            
        else if name.count < 2
        {
            showAlert(uiview: self, msg: "Organization should have minimum 2 characters And maximum 18 characters", isTwoButton: false)
        }
            
        else if name.count > 18
        {
            showAlert(uiview: self, msg: "Organization should have maximum 18 characters", isTwoButton: false)
        }
            
        else if txtContactNo.text == ""
        {
            showAlert(uiview: self, msg: "Please Enter Contact No", isTwoButton: false)
        }
            
        else if txtEmail.text == ""
        {
            showAlert(uiview: self, msg: "Please enter email id", isTwoButton: false)
        }
            
        else if !txtEmail.isValidEmail()
        {
            showAlert(uiview: self, msg: "Please enter valid email id", isTwoButton: false)
        }
            
        else if txtHomeTown.text == ""
        {
            showAlert(uiview: self, msg: "Please enter hometown", isTwoButton: false)
        }
            
        else if btnUserTypeOutlet.currentTitle == "Select user type"
        {
            showAlert(uiview: self, msg: "Please select user type", isTwoButton: false)
        }
            
        else if btnCheckOutlet.accessibilityLabel == "off"
        {
            showAlert(uiview: self, msg: "Please accept terms & condition", isTwoButton: false)
        }
            
        else
        {
            let address = txtHomeTown.text!
            let geocoder = CLGeocoder()
            geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                if((error) != nil){
                    print("Error", error ?? "")
                }
                if let placemark = placemarks?.first {
                    let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                    self.Latitude = coordinates.latitude
                    self.Longitude = coordinates.longitude
                    print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                }
                
                self.UpdateOrganationService()
            })
        }
    }
    
}
