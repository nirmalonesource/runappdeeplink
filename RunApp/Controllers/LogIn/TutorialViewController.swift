//
//  TutorialViewController.swift
//  RunApp
//
//  Created by Mac on 19/05/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var collectionViewTutorial: UICollectionView!
      
      @IBOutlet weak var buttonSkip: UIButton!
      
    @IBOutlet weak var btnLeft: UIButton!
    
    @IBOutlet weak var btnright: UIButton!
    
    @IBOutlet weak var buttonGotIt: UIButton!
    
    
    var IsFromReg: String  = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnLeft.transform = CGAffineTransform(rotationAngle:CGFloat(M_PI));
        // Do any additional setup after loading the view.
    }
    
  
    var arrTutorialImages = [
        UIImage(named: "1"),
        UIImage(named: "2"),
        UIImage(named: "3"),
        UIImage(named: "4"),
        UIImage(named: "5"),
        UIImage(named: "6"),
        UIImage(named: "7"),
        UIImage(named: "8"),
        UIImage(named: "9"),
        UIImage(named: "10"),
        UIImage(named: "11"),
        UIImage(named: "12"),
        UIImage(named: "13"),
        UIImage(named: "14"),
        UIImage(named: "15"),
        UIImage(named: "16"),
        UIImage(named: "17"),
        UIImage(named: "18")
    ]
    
    var currentIndex = Int()
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    @IBAction func btn_back(_ sender: Any) {
        if IsFromReg == "true"
        {
            
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
             self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else{
       self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func btn_rightAction(_ sender: Any) {
        let visibleItems: NSArray = self.collectionViewTutorial.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
               if nextItem.row < arrTutorialImages.count {
            self.collectionViewTutorial.scrollToItem(at: nextItem, at: .left, animated: true)
    }
    }
    
    @IBAction func btn_leftAction(_ sender: Any) {
        let visibleItems: NSArray = self.collectionViewTutorial.indexPathsForVisibleItems as NSArray
                    let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
                    let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
                    if nextItem.row < arrTutorialImages.count && nextItem.row >= 0{
                        self.collectionViewTutorial.scrollToItem(at: nextItem, at: .right, animated: true)
    }
    
}
}
extension TutorialViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTutorialImages.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionViewTutorial.frame.width, height: self.collectionViewTutorial.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewTutorial.dequeueReusableCell(withReuseIdentifier: "TutorialCVC", for: indexPath) as! TutorialCVC
        cell.imageTutorial.image = arrTutorialImages[indexPath.row]
        cell.imageTutorial.contentMode = .scaleAspectFit
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = collectionViewTutorial.frame.width
        let currentPage = collectionViewTutorial.contentOffset.x / pageWidth
        
        if (0.0 != fmodf(Float(currentPage), 1.0))
        {
            currentIndex = Int(currentPage + 1);
        }
        else
        {
            currentIndex = Int(currentPage);
        }
        
        if currentIndex == (arrTutorialImages.count - 1) {
            
        } else {
            
        }
    }
    
    
}

//MARK: - Collection View Cell Class
class TutorialCVC: UICollectionViewCell {
    
    @IBOutlet weak var imageTutorial: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
