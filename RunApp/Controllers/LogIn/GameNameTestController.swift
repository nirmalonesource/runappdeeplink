//
//  GameNameTestController.swift
//  RunApp
//
//  Created by My Mac on 07/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import CoreLocation

class GameNameCollectonCell: UICollectionViewCell {
    @IBOutlet weak var imgPlayers: UIImageView!
}

class GameNameTestController: UIViewController ,UICollectionViewDelegate, UICollectionViewDataSource,CLLocationManagerDelegate {

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var lblGameName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblPrivateDolar: UILabel!
    @IBOutlet weak var lblRunSince2: UILabel!
    @IBOutlet weak var lblRunSince1: UILabel!
    
    @IBOutlet weak var btnChecInOutlet: UIButton!
    
    @IBOutlet weak var collectionViewPlayer: UICollectionView!
    
    var playerList = [[String : Any]]()
    var LocationID: String?
    var EventID = ""
    var dictLocation = [String : Any]()
    var lagitude = Double()
      var logitude = Double()
      var locationManager = CLLocationManager()
     var flag = Bool()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
         {
            if flag {
                flag = false
                if let location = locations.last
                {
                    //print("Found user's location: \(location)")
                    print("latitude: \(location.coordinate.latitude)")
                    print("longitude: \(location.coordinate.longitude)")
                    lagitude = location.coordinate.latitude
                    logitude = location.coordinate.longitude
                    locationManager.stopUpdatingLocation()
                   // getlocationinfo()
                }
            }
                
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
               print("Unable to access your current location")
           }

    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("LocationID: \(LocationID!)")
      
        self.view.layoutIfNeeded()
        getPlaces()
        
       let usertype = UserDefaults.standard.getUserDict()["usertype"] as? String ?? ""
        if usertype == "Organization" {
            btnChecInOutlet.isHidden = true
        }
            
        else {
            btnChecInOutlet.isHidden = false
        }
        
       // playerListApi()
        
        btnChecInOutlet.setRadius(radius: btnChecInOutlet.frame.height/2)
//        imgBG.setGredient()
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        self.view.layoutIfNeeded()
        
        if CLLocationManager.locationServicesEnabled() == true {
                         
                         if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                             locationManager.requestWhenInUseAuthorization()
                         }
                         
                         locationManager.desiredAccuracy = kCLLocationAccuracyBest
                         locationManager.delegate = self
                         locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                         locationManager.requestWhenInUseAuthorization()
                         locationManager.startUpdatingLocation()
                         locationManager.requestLocation()
                         flag = true
                         // mapVW.mapType = .hybrid
                     } else {
                         print("Please turn on location services or GPS")
                     }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playerList.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayersCollectionCell", for: indexPath) as! GameNameCollectonCell
        cell.imgPlayers.setRadius(radius: cell.imgPlayers.frame.height/2)
        let imgURL = ServiceList.IMAGE_URL + "\(playerList[indexPath.row]["UserProfile"] as? String ?? "")"
        cell.imgPlayers.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                  completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
        
        if playerList[indexPath.row].count > 0 {
            nextViewController.playerListDetail = playerList[indexPath.row]
           self.navigationController?.pushViewController(nextViewController, animated: true)
        }
            
        else {
            showAlert(uiview: self, msg: "Player Record Not Yet", isTwoButton: false)
        }
    }
    
    //MARK:- WEB SERVICE
    
    func getPlaces()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOCATIONS,
                method: .post,
                param: ["LocationID" : LocationID!] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        let GetData:NSArray = result["data"] as! NSArray
                     //   self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
                        self.parseLocation(LocationDict: GetData.object(at: 0) as? [String : Any] ?? [:])
                        let playerjoin:NSArray = GetData.value(forKey: "playerjoin") as! NSArray
                        self.playerList = playerjoin.object(at: 0) as? [[String:Any]] ?? []
                        
                    }
                    
                    DispatchQueue.main.async {
                        self.collectionViewPlayer.reloadData()
                    }
          })
    }
    
    func parseLocation(LocationDict : [String:Any]) {
        
        self.lblGameName.text = LocationDict["LocationName"] as? String ?? ""
        self.lblAddress.text = LocationDict["LocationAddress"] as? String ?? ""
        self.EventID = LocationDict["EventID"] as? String ?? ""
        
        let cretedOn = LocationDict["RegistrationDate"] as? String ?? ""
        
        if cretedOn != "" {
            let Since = cretedOn.toDate3()
            self.lblRunSince1.text = "Run site Since: \(Since)"
        }
     
        //                            let imgPin = placeList[i]["product_image"] as? String ?? ""
        //                            let Distance = placeList[i]["product_image"] as? String ?? ""
        self.lblRunSince2.text = "\(LocationDict["LocMode"] as? String ?? "")"
        print("LocationDict: \(LocationDict)")
    }
   
    func playerListApi()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_LIST,
                method: .get,
                param: [:] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.playerList = result["data"] as? [[String:Any]] ?? []
                        self.collectionViewPlayer.reloadData()
                    }
        })
    }
    
    func appPlayerCheckIn()
    {
        let parameters = ["Location_Id": LocationID ?? "" ,
                          "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                          "Event_ID": EventID,
                          "Lat" : lagitude,
                          "Long" : logitude
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_CHECK_IN,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                       showToast(uiview: self, msg: result.getString(key: "message"))
                    }
                    else
                    {
                         showToast(uiview: self, msg: result.getString(key: "message"))
                    }
        })
    }
   
    //MARK: BUTTON ACTION METHODS
    
    @IBAction func BtnCheckAction(_ sender: UIButton) {
        if EventID != "" {
            appPlayerCheckIn()
        }

        else {
            print("Event id is Empty")
             showToast(uiview: self, msg:"Event not found.")
        }
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddLocationAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchEventController") as! SearchEventController
    self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnAddNewEventAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewEventViewController") as! AddNewEventViewController
        nextViewController.LocationID = LocationID
       // nextViewController.delegate = self as? DataProtocolGreen
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
