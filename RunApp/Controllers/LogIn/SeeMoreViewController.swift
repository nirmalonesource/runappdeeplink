//
//  SeeMoreViewController.swift
//  RunApp

//
//  Created by My Mac on 15/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import NightNight
import Alamofire
import CoreLocation

class UpcomingRunViewCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPm: UILabel!
    @IBOutlet weak var lblYoute: UILabel!
    @IBOutlet weak var btnRsvp: UIButton!
    @IBOutlet weak var lblspectatorfees: UILabel!
    
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblplayerfees: UILabel!
    
}

class SeeMoreViewController: UIViewController , UITableViewDataSource , UITableViewDelegate,CLLocationManagerDelegate{
    
    var lagitude = Double()
    var logitude = Double()
    var locationManager = CLLocationManager()
    var flag = Bool()
    
    var RSVPTitle = ""
    
    
    @IBOutlet weak var lblamounttobepaid: UILabel!
    @IBOutlet weak var lblupcomingevent: UILabel!
    
    
    
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var vwPopup: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    
    @IBOutlet weak var btnSpectatorOutlet: UIButton!

    @IBOutlet weak var btnJoinOutlet: UIButton!
    
    var rsvpDictList = [[String : Any]]()
    var SpectatorArray = [String]()
    var LocationID: String?
    var EventID = ""
    
    var Amt = ""
    var PlayerAmt = ""
    var AmountGet = ""
    var PlayerType = ""
    var isfromnotification = false
    
// MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        vwPopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lblupcomingevent.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        lblamounttobepaid.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        lblamounttobepaid.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        
        
        if CLLocationManager.locationServicesEnabled() == true {
                              
                              if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                                  locationManager.requestWhenInUseAuthorization()
                              }
                              
                              locationManager.desiredAccuracy = kCLLocationAccuracyBest
                              locationManager.delegate = self
                              locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                              locationManager.requestWhenInUseAuthorization()
                              locationManager.startUpdatingLocation()
                              locationManager.requestLocation()
                              flag = true
                              // mapVW.mapType = .hybrid
                          } else {
                              print("Please turn on location services or GPS")
                          }
        
        SpectatorArray = ["Spectator","Player"]
        self.view.layoutIfNeeded()
        vwPopupTop.setRadius(radius: 10)
        tblMenu.tableFooterView = UIView()
        self.view.layoutIfNeeded()
        appGetRsvpLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
          {
             if flag {
                 flag = false
                 if let location = locations.last
                 {
                     //print("Found user's location: \(location)")
                     print("latitude: \(location.coordinate.latitude)")
                     print("longitude: \(location.coordinate.longitude)")
                     lagitude = location.coordinate.latitude
                     logitude = location.coordinate.longitude
                     locationManager.stopUpdatingLocation()
                    // getlocationinfo()
                 }
             }
                 
         }
         
         func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
                print("Unable to access your current location")
            }
    // MARK:- TABLE VIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rsvpDictList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingRunViewCell", for: indexPath) as! UpcomingRunViewCell
        cell.lblYoute.text = rsvpDictList[indexPath.row]["EvenetName"] as? String
        let cretedOn = rsvpDictList[indexPath.row]["EventDate"] as? String ?? ""
        cell.lblDate.text = cretedOn.toDate5()
        cell.lblPm.text = "Max Player: " + String(rsvpDictList[indexPath.row]["MaxPlayer"] as? String ?? "")
            
        cell.lblLoc.text =   "LOC " + String(rsvpDictList[indexPath.row]["CLVLTitle"] as? String ?? "")
          
        cell.btnRsvp.layer.cornerRadius = 5.0
        cell.btnRsvp.layer.masksToBounds = true
        cell.btnRsvp.tag = indexPath.row
        
     cell.lblspectatorfees.text = "Spectator Price: " + String(rsvpDictList[indexPath.row]["Amt"] as? String ?? "")
     cell.lblplayerfees.text = "Player Price: " + String(rsvpDictList[indexPath.row]["PlayerAmt"] as? String ?? "")
        
        
        if UserDefaults.standard.getUserDict()["usertype"] as? String ?? "" != "Organization" {
            cell.btnRsvp.isHidden = false
            cell.btnRsvp.addTarget(self, action: #selector(btnRsvpAction), for: .touchUpInside)
            
             let RSVPD = rsvpDictList[indexPath.row]["RSVPD"] as? String ?? ""
            if RSVPD == "1"
            {
//                cell.btnRsvp.setTitle("RSVP'd", for: .normal)
//                cell.btnRsvp.isEnabled = false
                  cell.btnRsvp.setTitle("Cancel", for: .normal)
                   cell.btnRsvp.isEnabled = true
            }
            else
            {
                cell.btnRsvp.setTitle("RSVP", for: .normal)
                cell.btnRsvp.isEnabled = true
            }
            
            
            let IsEventOneHour = rsvpDictList[indexPath.row]["IsEventOneHour"] as? Int ?? 3
                
                if IsEventOneHour == 1
            {
                cell.btnRsvp.setTitle("EventCheckIn", for: .normal)
                
            }
           let IsEventCheckedIn = rsvpDictList[indexPath.row]["IsCheckedIn"] as? Int ?? 3
                                           
                    if IsEventCheckedIn == 1
                            {
                                           cell.btnRsvp.setTitle("CheckedIn", for: .normal)
                                           cell.btnRsvp.isEnabled = false
                            }
        }
            
        else
        {
            cell.btnRsvp.isHidden = true
        }
        
        
        cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        cell.lblDate.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        cell.lblPm.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        cell.lblYoute.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
         cell.lblplayerfees.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        cell.lblspectatorfees.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        cell.lblLoc.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let IR = rsvpDictList[indexPath.row]["IR"] as? String ?? ""
        
        if IR == "ON"
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "IREventDetailViewController") as! IREventDetailViewController
                         nextViewController.Event_ID = rsvpDictList[indexPath.row]["EventID"] as? String ?? ""
                         nextViewController.Event_Name = rsvpDictList[indexPath.row]["EvenetName"] as? String ?? ""
                   
                         self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
        else
        {
    
          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailView") as! EventDetailView
              nextViewController.Event_ID = rsvpDictList[indexPath.row]["EventID"] as? String ?? ""
              nextViewController.Event_Name = rsvpDictList[indexPath.row]["EvenetName"] as? String ?? ""
        
              self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
    }
  // MARK:- BUTTON METHODS
    
    @objc func btnRsvpAction(button: UIButton) {
        btnJoinOutlet.tag = button.tag
        
        
        RSVPTitle =   button.currentTitle!
        
        
        let buttonRow = button.tag
        print("\(buttonRow): Btn Cell is Pressed")
        self.Amt = rsvpDictList[button.tag]["Amt"] as? String ?? ""
        self.PlayerAmt = rsvpDictList[button.tag]["PlayerAmt"] as? String ?? ""
        self.EventID = rsvpDictList[button.tag]["EventID"] as? String ?? ""
        self.PlayerType = "Spectator"
        self.AmountGet = self.Amt
        self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
        
        let BookingStatus = rsvpDictList[button.tag]["BookingStatus"] as? String ?? ""
        
        if self.rsvpDictList[button.tag]["EventType"] as! String == "Paid"
        {
               if  self.rsvpDictList[button.tag]["IsPayment"] as! Int == 0
            {
               self.btnJoinOutlet.setTitle("PAY NOW", for: .normal)
               }
            else
               {
              self.btnJoinOutlet.setTitle("CHECK IN", for: .normal)
               }
            
            
        }
        else
        {
            self.btnJoinOutlet.setTitle("JOIN", for: .normal)
        }
        
        if BookingStatus == "0" {
            showViewWithAnimation(vw: vwPopup, img: imgBlur)
        }
            
        else {
            showAlert(uiview: self, msg: "Booking Closed", isTwoButton: false)
        }
        
        let RSVPD = rsvpDictList[button.tag]["RSVPD"] as? String ?? ""
        
        
              
        if RSVPTitle == "Cancel"
               {
                   appCancelSpectator()
               }
        
        
        
//              if RSVPD == "1"
//              {
//                   appCancelSpectator()
//              }
      //  showViewWithAnimation(vw: vwPopup, img: imgBlur)
    }
    
    @IBAction func btnJoinSpectatorClicked(_ sender: UIButton) {
        
//        if self.rsvpDictList[sender.tag]["EventType"] as! String == "Paid"
//        {
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
//            nextViewController.Amount = AmountGet
//            nextViewController.EventID = EventID
//            nextViewController.PlayerType = PlayerType
//            nextViewController.LocationID = LocationID
//
//            self.navigationController?.pushViewController(nextViewController, animated: true)
//        }
//        else
//        {
//            //self.btnPayOutlet.setTitle("Join", for: .normal)
//            appSpectatorJoin()
//        }
        
       if self.btnSpectatorOutlet.currentTitle! != "Please Select One"
       {
        
     
           
                if RSVPTitle == "RSVP"
                {
                 
            if AmountGet == "0"
            
            {
                   appSpectatorJoin()
                
            }
            else
            {
                if self.rsvpDictList[sender.tag]["EventType"] as! String == "Paid"
            {
                
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
                nextViewController.Amount = AmountGet
                nextViewController.EventID = EventID
                nextViewController.PlayerType = PlayerType
                nextViewController.LocationID = LocationID
                
                
                nextViewController.rsvpDictList1 = rsvpDictList[sender.tag]
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
                
            }
            else
            {
                //self.btnPayOutlet.setTitle("Join", for: .normal)
                
                
                appSpectatorJoin()
                
            }
            }
                }
                else if RSVPTitle == "EventCheckIn" {
                    if AmountGet == "0"
                    {
                        appEventCheckin()
                    }
                    else
                    {
                    if self.rsvpDictList[sender.tag]["EventType"] as! String == "Paid"
                        {
                            
                        if self.rsvpDictList[sender.tag]["IsPayment"] as! Int == 0
                        {
                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
                            nextViewController.Amount = AmountGet
                            nextViewController.EventID = EventID
                            nextViewController.PlayerType = PlayerType
                            nextViewController.LocationID = LocationID
                            nextViewController.rsvpDictList1 = rsvpDictList[sender.tag]
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                            
                            }
                        else{
                            
                             appEventCheckin()
                            
                            }
                        }
                        else
                        {
                            //self.btnPayOutlet.setTitle("Join", for: .normal)
                            appEventCheckin()
                        }
                }
        }
        }
       else
       {
        showToast(uiview:self, msg:"Please Select One")
        }
  
    }
    
    
    func appEventCheckin()
           {
               if EventID != "" {
                   let parameters = ["Location_Id": LocationID ?? "" ,
                                     "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                     "Event_ID": EventID, "PlayerType" : PlayerType,"Lat":lagitude,"Long":logitude
                                     
                       ] as [String : Any]
                   
                   let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                 "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                 ] as [String : Any]
                   
                   callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerEventCheckIn,
                           method: .post,
                           param: parameters ,
                           extraHeader: header ,
                           completionHandler: { (result) in
                               print(result)
                               self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                               showToast(uiview: self, msg: result.getString(key: "message"))
                               if result.getBool(key: "status")
                               {
                                   
                                  
                               }
                               else
                               {
                                  // showToast(uiview: self, msg: result.getString(key: "message"))
                               }
                                self.appGetRsvpLocation()
                   })
               }
                   
               else {
                   print("Event id is Empty")
               }
               
           }
       
    
    @IBAction func btnSpectatorAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if SpectatorArray.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Please Select One", rows: SpectatorArray, initialSelection: 0, doneBlock: {
                picker, indexes, values in
                
                self.btnSpectatorOutlet.setTitle("\(values!)", for: .normal)
                
                self.PlayerType  =  values as! String
                
                
                                if self.btnSpectatorOutlet.currentTitle == "Spectator" {
                                    self.AmountGet = self.Amt
                                    self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
                                    self.PlayerType = "Spectator"
                                }
                                    
                                else if self.btnSpectatorOutlet.currentTitle == "Player" {
                                    self.AmountGet = self.PlayerAmt
                                    self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
                                    self.PlayerType = "Player"
                                }
                                
                                else {
//                                    self.AmountGet = self.PlayerAmt
//                                    self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
//                                    self.PlayerType = "Both"
                                       showToast(uiview: self, msg: "Please Select One")
                                    
                                }
                
                
                                return
                            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
                
        }
                //                        if self.btnSpectatorOutlet.currentTitle == "Spectator" {
                //                            self.btnJoinOutlet.setTitle("JOIN NOW", for: .normal)
                //                        }
                //
                //                        else {
                //                           self.btnJoinOutlet.setTitle("PAY NOW", for: .normal)
                //                        }
                
                //self.strStateID = self.stateData[indexes]["state_id"] as? String ?? ""
      
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
       // self.navigationController?.popViewController(animated: true)
        if isfromnotification {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
  
    
    // MARK:- TOUCHES BEGAN
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        hideViewWithAnimation(vw: vwPopup, img: imgBlur)
    
    }
    
//    func appSpectatorJoin()
//    {
//        let parameters = ["Location_Id": LocationID ?? "" ,
//                          "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
//                          "Event_ID": EventID
//            ] as [String : Any]
//
//        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
//                      ] as [String : Any]
//
//        callApi(ServiceList.SERVICE_URL+ServiceList.APP_SPECTATOR_JOIN,
//                method: .post,
//                param: parameters ,
//                extraHeader: header ,
//                completionHandler: { (result) in
//                    print(result)
//                    if result.getBool(key: "status")
//                    {
//
//
//                    }
//                    self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
//                    showToast(uiview: self, msg: result.getString(key: "message"))
//                    self.appGetRsvpLocation()
//        })
//    }
    
    func appSpectatorJoin()
      {
          let parameters = ["Location_Id": LocationID ?? "" ,
                            "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                            "Event_ID": EventID,
                            "PlayerType" : PlayerType
              ] as [String : Any]
          
          let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                        "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                        ] as [String : Any]
          
          callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerJoin,
                  method: .post,
                  param: parameters ,
                  extraHeader: header ,
                  completionHandler: { (result) in
                      print(result)
                      if result.getBool(key: "status")
                      {
                        
                        
                      }
                      self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                      showToast(uiview: self, msg: result.getString(key: "message"))
                      self.appGetRsvpLocation()
          })
      }
    
    
    
    func appCancelSpectator()
     {
         if EventID != "" {
             let parameters = ["Location_Id": LocationID ?? "" ,
                               "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                               "Event_ID": EventID
                 ] as [String : Any]
             
             let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                           ] as [String : Any]
             
             callApi(ServiceList.SERVICE_URL+ServiceList.APP_SPECTATOR_CANCEL,
                     method: .post,
                     param: parameters ,
                     extraHeader: header ,
                     completionHandler: { (result) in
                         print(result)
                         self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                         showToast(uiview: self, msg: result.getString(key: "message"))
                         if result.getBool(key: "status")
                         {
                             
                            // self.btnPayOutlet.setTitle("PAY NOW", for: .normal)
                         }
                         else
                         {
                            // showToast(uiview: self, msg: result.getString(key: "message"))
                         }
                          self.appGetRsvpLocation()
                        
             })
         }
             
         else {
             print("Event id is Empty")
         }
         
     }
    
    
    
    
    
    func appGetRsvpLocation()
    {
        let parameters = ["LocationID": LocationID  ?? "","EventID":EventID,"PlayerID": UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
            ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_RSVP_LOCATION,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        //let data = result["data"] as? [[String:Any]] ?? []
                         self.rsvpDictList = [[String : Any]]()
                        let GetData:NSDictionary = result["data"] as! NSDictionary
                        self.rsvpDictList = GetData.value(forKey: "event") as? [[String:Any]] ?? []
                        print("rsvpListGet:\(self.rsvpDictList)")
                     //   let jsonarr:NSArray = GetData.value(forKey: "event") as! NSArray
                     //   let dict1:NSDictionary = jsonarr.lastObject as! NSDictionary
                        //   let dict2:NSDictionary = jsonarr.object(at: jsonarr.count-2) as! NSDictionary
                      //  self.rsvpDictList.append(jsonarr as! [String : Any])
                        // self.rsvpList.append(dict2 as! [String : Any])
                       
                        
//                        self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
//                        self.parseLocation(LocationDict: self.dictLocation)
                    }
                      self.tblMenu.reloadData()
//                    DispatchQueue.main.async {
//
//                        //  self.constTblUpcomingHeight.constant = self.tblUpcomingView.contentSize.height
//                    }
        })
    }
   
}


