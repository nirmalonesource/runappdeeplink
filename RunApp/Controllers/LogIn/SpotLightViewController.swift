//
//  SpotLightViewController.swift
//  RunApp

///
//  Created by My Mac on 11/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//



import UIKit
import NightNight
import AVFoundation
import AVKit
import CoreLocation

class CollectionViewCell1: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    override func awakeFromNib() {
        lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLine.mixedTextColor =  MixedColor(normal: 0xffffff, night:0x000000 )
    }
}

class CollectionViewCell2: UICollectionViewCell {
    @IBOutlet weak var imgName: UIImageView!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
}

class tableViewCell1: UITableViewCell {
    @IBOutlet weak var img1Name: UIImageView!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var img2Name: UIImageView!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnLikeOutlet: UIButton!
    
    @IBOutlet weak var btnChatOutlet: UIButton!
    
    @IBOutlet var imgplay: UIImageView!
    @IBOutlet weak var btnEmailOutlet: UIButton!
    @IBOutlet var btnshare: UIButton!
    @IBOutlet var btnVideo: UIButton!
    override func awakeFromNib() {
        lblFirst.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
       // btnChatOutlet.setMixedImage(MixedImage(normal:"chat", night:"chat-W"), forState: .normal)
         btnChatOutlet.setMixedImage(MixedImage(normal:"chat-W", night:"chat-W"), forState: .normal)
         btnEmailOutlet.setMixedImage(MixedImage(normal:"Email2", night:"email"), forState: .normal)
    }
    
    @IBAction func btnLikeClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func btnChatClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func btnEmail(_ sender: UIButton) {
        
    }
}

class ColletionViewCell3: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
      
    }
}

class tableViewCell2: UITableViewCell {
  
    @IBOutlet weak var imgName: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnReadOutlet: UIButton!
    @IBOutlet weak var btnDotsOutlet: UIButton!
    @IBOutlet var btnshare: UIButton!
    
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    
}

class SpotLightViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource ,UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate {

    
    @IBOutlet var btnImgUser: UIButton!
    
    var LocationDetail = [String : Any]()
    
    var newsFeedDict = [[String:Any]]()
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var Collection1ndView: UICollectionView!
    
    @IBOutlet weak var Collection2ndView: UICollectionView!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var vwPopup2ndView: UIView!
    
    @IBOutlet weak var Collection3rdView: UICollectionView!
    
    @IBOutlet weak var tblView2: UITableView!
    
    @IBOutlet var imgUser: UIImageView!
    
    var lagitude = Double()
           var logitude = Double()
           var locationManager = CLLocationManager()
          var flag = Bool()
       
       func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
            {
               if flag {
                   flag = false
                   if let location = locations.last
                   {
                       //print("Found user's location: \(location)")
                       print("latitude: \(location.coordinate.latitude)")
                       print("longitude: \(location.coordinate.longitude)")
                       lagitude = location.coordinate.latitude
                       logitude = location.coordinate.longitude
                       locationManager.stopUpdatingLocation()
                      // getlocationinfo()
                   }
               }
                   
           }
           
           func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
                  print("Unable to access your current location")
              }
    
    
    func APP_USER_PROFILE_DETAIL()
       {
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]
           
           callApi(ServiceList.SERVICE_URL+ServiceList.APP_USER_PROFILE,
                   method: .post,
                   param: ["UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "", "FromID": "0"] ,
                   extraHeader: header ,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                           let DataDict = result["data"] as? [[String:Any]] ?? []
                           print("playerDetailResult: \(DataDict)")
                           
                           let imgURL = ServiceList.IMAGE_URL + "\(DataDict[0]["UserProfile"] as? String ?? "")"
                        self.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,completed: nil)
                        
                     
                        
                           
                       //    let imgURL =  ServiceList.IMAGE_URL + result.getString(key: "UserProfile")
                                             UserDefaults.standard.set(imgURL, forKey: "userprofileimg")
                                             //self.imgUser.sd_setShowActivityIndicatorView(true)
                                        //     self.imgUser.sd_setIndicatorStyle(.gray)
                                        //     self.imgUser.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "user"))
                       }
           })
       }
       
    
    
    var selectedItem = Int()
    var type: String?
    
    var titleArray = [String]()
    
    // MARK: VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         imgUser.setRadius(radius: imgUser.frame.height/2)
        
        APP_USER_PROFILE_DETAIL()
        if CLLocationManager.locationServicesEnabled() == true {
                            
                            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                                locationManager.requestWhenInUseAuthorization()
                            }
                            
                            locationManager.desiredAccuracy = kCLLocationAccuracyBest
                            locationManager.delegate = self
                            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                            locationManager.requestWhenInUseAuthorization()
                            locationManager.startUpdatingLocation()
            
                            locationManager.requestLocation()
            
            let currentLoc = locationManager.location
            lagitude =  (currentLoc?.coordinate.latitude ?? 0)
            logitude = (currentLoc?.coordinate.longitude ?? 0)
            
                            flag = true
                            // mapVW.mapType = .hybrid
                        } else {
                            print("Please turn on location services or GPS")
                        }
      //  imgBG.mixedImage = MixedImage(normal: UIImage(named: "imgAppBG1") ?? UIImage(), night: UIImage(named: "imgAppBG1-W") ?? UIImage())
        vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        
        vwPopup2ndView.isHidden = true
        
        tblView.isHidden = false
        
        
        print("newsFeedDict: \(newsFeedDict)")
      //  Spotlight / RUN TV / News / Scores / Events
      //  titleArray = ["RUN TV","News","Scores","Events","Spotlight"]
       // titleArray = ["Spotlight","RUN TV","News","Scores","Events"]
        
        
        titleArray = ["Games","Spotlight","RUN TV","News","Scores"]
        
        //titleArray = ["Tweet","News","Scores","Adv"]

//        if type == "Tweet" {
//            TweetApi(type: type)
//          //  selectedItem = 0
//             vwPopup2ndView.isHidden = true
//            tblView.isHidden = false
//        }
        
         if type == "tv"  {
            
            TweetApi(type: type)
            selectedItem = 2
           vwPopup2ndView.isHidden = false
            tblView.isHidden = true
         
         }
        
            else if type == "Adv"
                   {
                        AdvApi(type: type)
                        selectedItem = 1
                        vwPopup2ndView.isHidden = true
                        tblView.isHidden = false
                   }
            
        else if type == "News" {
            
            NewsApi(type: type)
            selectedItem = 3
            vwPopup2ndView.isHidden = false
            tblView.isHidden = true
            
        }
            
        else if type == "Scores"
         {
            ScoresApi(type: type)
            selectedItem = 4
            vwPopup2ndView.isHidden = false
            tblView.isHidden = true
            
        }
            
       
        
        else if type == "Games"
        {
             EventApi(type: type)
             selectedItem = 0
            vwPopup2ndView.isHidden = false
            tblView.isHidden = true
        }
       
        self.view.layoutIfNeeded()
        
//        imgBG.setGredient()
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        tblView.tableFooterView = UIView()
        tblView2.tableFooterView = UIView()
        self.view.layoutIfNeeded()
    }
    
    // MARK: COLLECTION VIEW METHODS
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == Collection1ndView {
            return titleArray.count
        }
            
        else if collectionView == Collection2ndView {
            return 3
        }
            
        else {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == Collection1ndView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colletionCell1", for: indexPath) as! CollectionViewCell1
            
            let name = titleArray[indexPath.row]
            
            cell.lblName.text = name
            
                if indexPath.row == selectedItem {
                    
                    cell.lblLine.backgroundColor = UIColor.red
                }
                    
                else {
                    
                    cell.lblLine.backgroundColor = UIColor.white
                    
                }
            
            return cell
        }
            
        else if collectionView == Collection2ndView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colletionCell2", for: indexPath) as! CollectionViewCell2
            return cell
        }
            
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colletionCell3", for: indexPath) as! ColletionViewCell3
            return cell
        }
    }
    
    //["Tweet","News","Scores","Adv"]

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == Collection1ndView
        {
            selectedItem = indexPath.row
            
            type = titleArray[indexPath.row]
            collectionView.reloadData()
            
//            if indexPath.row == 0
//            {
//                vwPopup2ndView.isHidden = true
//                tblView.isHidden = false
//               // tblView.reloadData()
//                AdvApi(type: "Adv")
//                selectedItem = 0
//            }
//
//            else {
//                vwPopup2ndView.isHidden = false
//                tblView.isHidden = true
//                if type == "Tweet" {
//                    TweetApi(type: type)
//                   // selectedItem = 0
//                }
//
//                else if type == "News" {
//                    NewsApi(type: type)
//                    selectedItem = 1
//                }
//
//                else if type == "Scores" {
//                    ScoresApi(type: type)
//                    selectedItem = 2
//                }
//                else if type == "Events" {
//                   EventApi(type: type)
//                   selectedItem = 3
//                }
//            }
            
            if indexPath.row == 1
                    {
                        vwPopup2ndView.isHidden = true
                        tblView.isHidden = false
                       // tblView.reloadData()
                        AdvApi(type: "Adv")
                        selectedItem = 1
                    }
                        
                    else {
                        vwPopup2ndView.isHidden = false
                        tblView.isHidden = true
                
                        if type == "RUN TV" {
                            type = "tv"
                            TweetApi(type: type)
                            selectedItem = 2
                        }
                            
                        else if type == "News" {
                            NewsApi(type: type)
                            selectedItem = 3
                        }
                            
                        else if type == "Scores" {
                            ScoresApi(type: type)
                            selectedItem = 4
                        }
                        else if type == "Games" {
                           EventApi(type: type)
                           selectedItem = 0
                        }
                    }
            
            
            
            
            
//            else if indexPath.row == 1 {
//                vwPopup2ndView.isHidden = false
//                tblView.isHidden = true
//            }
        
        }
    }
    
    
    
    //Convert datetime
     func toDate10(strDate : String) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           let date = dateFormatter.date(from: strDate)
           //"MM-dd-yy hh:mm a"
           dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
           let newDate = dateFormatter.string(from: date ?? Date())
           return newDate
       }
       
    
   
     //MARK:- Web service
    
    func ShareApi()
    {
        let parameters = [:] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
      
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_SHARE,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
        })
    }
    
    func EventApi(type: String?)
    {
        let parameters = ["type" : type!,"Lat":lagitude,"Long":logitude] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "", "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        self.newsFeedDict.removeAll()
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_TICKER,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    self.newsFeedDict =  [[:]]
                    if result.getBool(key: "status")
                    {
                    
                    self.newsFeedDict = result["data"] as? [[String:Any]] ?? []
                        
                    }
                    else
                    {
                        showToast(uiview: self, msg: result.getString(key: "message"))
                    }
                    DispatchQueue.main.async {
                       self.tblView2.reloadData()

                    }
        })
    }
    
    func NewsApi(type: String?)
    {
        let parameters = ["type" : type!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        self.newsFeedDict.removeAll()
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_TICKER,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.newsFeedDict = result["data"] as? [[String:Any]] ?? []
                        DispatchQueue.main.async {
                            self.tblView2.reloadData()
                        }
                    }
                    
                   // showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func TweetApi(type: String?)
    {
        let parameters = ["type" : type!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
          self.newsFeedDict.removeAll()
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_TICKER,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.newsFeedDict = result["data"] as? [[String:Any]] ?? []
                        DispatchQueue.main.async {
                            self.tblView2.reloadData()
                        }
                    }
                    
                  //  showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func AdvApi(type: String?)
    {
        let parameters = ["type" : type!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        self.newsFeedDict.removeAll()
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_TICKER,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.newsFeedDict = result["data"] as? [[String:Any]] ?? []
                        DispatchQueue.main.async {
                            self.tblView.reloadData()
                        }
                    }
                    
                   // showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func ScoresApi(type: String?)
    {
        let parameters = ["type" : type!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
          self.newsFeedDict.removeAll()
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_TICKER,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    
                    if result.getBool(key: "status")
                    {
                        self.newsFeedDict = result["data"] as? [[String:Any]] ?? []
                        DispatchQueue.main.async {
                            self.tblView2.reloadData()
                        }
                    }
                    
                  //  showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    //    {
    //        return CGSize(width: collectionView.frame.width/3, height:collectionView.frame.width/3)
    //    }
    
    // MARK: TABLE VIEW METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView {
            return newsFeedDict.count
        }
            
        else {
            return newsFeedDict.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! tableViewCell1
            //cell.lblFirst.text = newsFeedDict[indexPath.row][""]
            cell.btnEmailOutlet.imageView?.changeImageViewImageColor(color: UIColor.gray)
            //            cell.img2Name.sizeToFit()
            //            cell.img2Name.layoutIfNeeded()
            cell.btnChatOutlet.tag = indexPath.row
            
            let dic:NSDictionary = newsFeedDict[indexPath.row] as NSDictionary
           cell.lblFirst.text = dic.value(forKey: "TickerTitle") as? String
           cell.lblSecond.text = dic.value(forKey: "TickerDesc") as? String
          
            
            let imgURL2 =  ServiceList.IMAGE_URL + (dic.value(forKey: "UserProfile") as? String ?? "")
            cell.img1Name.sd_setImage(with: URL(string: imgURL2), placeholderImage: UIImage(named: "user"))
            cell.img1Name.setRadius(radius: cell.img1Name.frame.height/2)
            
            let imgURL = ServiceList.IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                            
                               let type = (dic.value(forKey: "Type") as? String ?? "")
                              
                               if type == "image" {
                                     let imgURL1 = ServiceList.IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                                            cell.img2Name.sd_setImage(with: URL(string : (imgURL1.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,completed: nil)
                                   cell.btnVideo.isHidden = true
                                cell.imgplay.isHidden = true
                                   
                               }
                               
                               else {
                                   let url = URL(string: imgURL)
                                   cell.btnVideo.isHidden = false
                                cell.imgplay.isHidden = false
                                   if let videoThumbnail = getThumbnailFrom(path: url!) {
                                       cell.img2Name.image = videoThumbnail
                                   }
                               }
                               
            cell.btnVideo.tag = indexPath.row
            cell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
            
            cell.btnChatOutlet.addTarget(self, action: #selector(self.btnCellMessageAction(_:)), for: .touchUpInside)
            cell.btnshare.tag = indexPath.row
            cell.btnshare.addTarget(self, action: #selector(self.btnCellShareAction(_:)), for: .touchUpInside)
            return cell
        }
            
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell2", for: indexPath) as! tableViewCell2

            cell.imgName.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
            
            let dic:NSDictionary = newsFeedDict[indexPath.row] as NSDictionary
            
            //let type = dic.value(forKey: "type") as? String
            
             if type == "Scores"
            {
                let TickerName1 = dic.value(forKey: "TickerName1") as? String
                let TickerName2 = dic.value(forKey: "TickerName2") as? String
                let TikcerScore1 = dic.value(forKey: "TikcerScore1") as? String
                let TickerScore2 = dic.value(forKey: "TickerScore2") as? String
                cell.lblName.text = String(format: "%@-%@",TickerName1 ?? "",TickerName2 ?? "" )
                cell.lblDescription.text = String(format: "%@ | %@",TikcerScore1 ?? "",TickerScore2 ?? "" )
                
                cell.lbllocation.isHidden = true
                cell.lblLoc.isHidden = true
                
                cell.imgName.setRadius(radius: cell.imgName.frame.height/2)
                let imgURL = ServiceList.ADMIN_IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                cell.imgName.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                          completed: nil)
                
                cell.btnshare.isHidden = false
                cell.btnshare.tag = indexPath.row
                cell.btnshare.addTarget(self, action: #selector(self.btnCellShareAction(_:)), for: .touchUpInside)

            }
                
            else if type == "News"
            {
                cell.lblName.text = dic.value(forKey: "TickerTitle") as? String
                cell.lblDescription.text = dic.value(forKey: "TickerDesc") as? String
                cell.imgName.setRadius(radius: cell.imgName.frame.height/2)
                let imgURL = ServiceList.IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                cell.imgName.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                          completed: nil)
                cell.btnshare.isHidden = false
                cell.btnshare.tag = indexPath.row
                cell.btnshare.addTarget(self, action: #selector(self.btnCellShareAction(_:)), for: .touchUpInside)
            }
                
            else if type == "Tweet"
            {
                let TickerName1 = dic.value(forKey: "TickerName1") as? String
                //let TickerName2 = dic.value(forKey: "TickerName2") as? String
                cell.lblName.text = TickerName1
                cell.lblDescription.text = dic.value(forKey: "TickerDesc") as? String
                cell.imgName.setRadius(radius: cell.imgName.frame.height/2)
                cell.btnshare.isHidden = false
                cell.btnshare.tag = indexPath.row
                cell.btnshare.addTarget(self, action: #selector(self.btnCellShareAction(_:)), for: .touchUpInside)
               // let imgURL = ServiceList.ADMIN_IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
              //cell.imgName.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad, completed: nil)
            }
             else if type == "tv"
                     {
                        cell.lblName.text = dic.value(forKey: "TickerTitle") as? String
                    cell.lblDescription.text = dic.value(forKey: "TickerDesc") as? String
                                       cell.imgName.setRadius(radius: cell.imgName.frame.height/2)
                                       let imgURL = ServiceList.IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
                                       cell.imgName.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                                                 completed: nil)
                                       cell.btnshare.isHidden = false
                                       cell.btnshare.tag = indexPath.row
                                       cell.btnshare.addTarget(self, action: #selector(self.btnCellShareAction(_:)), for: .touchUpInside)
                     }
           else if type == "Games"
                    {
                        cell.lblName.text = dic.value(forKey: "EvenetName") as? String
                        // cell.lblDescription.text = (dic.value(forKey: "EventFromDate") as? String ?? "")
                        
                        
                        let cretedOn = (dic.value(forKey: "EventDate") as? String ?? "")
                               

                               if cretedOn != "" {
                            
                         //   let cretedOn = (dic.value(forKey: "EventFromDate") as? String ?? "")
                                
                                cell.lblDescription.text = toDate10(strDate:cretedOn)
                               }
                               
                        
//                 let lbldate = (dic.value(forKey: "EventFromDate") as? String ?? "")
//
//                        if lbldate  != ""
//                        {
//
//            cell.lblDescription.text = (dic.value(forKey: "EventFromDate") as? String ?? "") != "0000-00-00 00:00:00" ? self.setDateWithFormat(strDate:
//
//                dic.value(forKey: "EventFromDate") as? String ?? "") : ""
//
//
//                        }
        cell.lbllocation.text = dic.value(forKey: "LocationName") as? String
        cell.lblLoc.text = dic.value(forKey: "CLVLTitle") as? String
        cell.lbllocation.isHidden = false
        cell.lblLoc.isHidden = false
                        
       cell.imgName.setRadius(radius: cell.imgName.frame.height/2)
                        
                        
                        

//     let imgURL = ServiceList.IMAGE_URL + "\(dic.value(forKey: "TickerImage") as? String ?? "")"
//                        cell.imgName.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
//                                                  completed: nil)
                        cell.imgName.image = UIImage(named: "redLocation")
                        cell.btnshare.isHidden = true
                    }
            
            return cell
        }
    }
    
    @objc func btnVideoAction(button: UIButton) {
        
        button.isHidden = false
        let dic:NSDictionary = newsFeedDict[button.tag] as NSDictionary
               let type = (dic.value(forKey: "Type") as? String ?? "")
               
               if type != "image" {
                   var videoURL = (dic.value(forKey: "TickerImage") as? String ?? "")
                   videoURL = ServiceList.IMAGE_URL + videoURL
                   let url : URL = URL(string: videoURL)!
                   let player = AVPlayer(url: url)
                   let playerViewController = AVPlayerViewController()
                   playerViewController.player = player
                   self.present(playerViewController, animated: true) {
                       playerViewController.player!.play()
                       // playerViewController.view.semanticContentAttribute = .forceRightToLeft
                   }
               }
              
           }
    
    //MARK:- Thumbnail From path
           func getThumbnailFrom(path: URL) -> UIImage? {
               do {
                   let asset = AVURLAsset(url: path , options: nil)
                   let imgGenerator = AVAssetImageGenerator(asset: asset)
                   imgGenerator.appliesPreferredTrackTransform = true
                   let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                   let thumbnail = UIImage(cgImage: cgImage)
                   return thumbnail
                   
               } catch let error {
                   print("*** Error generating thumbnail: \(error.localizedDescription)")
                   return nil
               }
           }
    
    @objc func btnCellShareAction(_ sender : UIButton)
          {
            let index = sender.tag
            print("SELECTEDINDEX:",index)
          
         //   let dic:NSDictionary = newsFeedDict[index] as NSDictionary
             let WebURL = newsFeedDict[index]["TickerURL"] as? String
            if (WebURL != nil) {
                ShareApi()
                let textToShare = [ WebURL ]
                let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
                           activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

                           // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

                           // present the view controller
                           self.present(activityViewController, animated: true, completion: nil)
                       }
//              if type == "Scores"
//              {
//
//              }
//
//              else if type == "News"
//              {
//
//              }
//
//              else if type == "Tweet"
//              {
//
//              }
//             else if type == "Events"
//              {
//
//              }
          }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
         if tableView == tblView {
            
//            let NewsURL = newsFeedDict[indexPath.row]["TickerURL"] as? String
//              if (NewsURL != nil)
//              {
//                  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "webView") as! webView
//                  nextViewController.urlString = NewsURL!
//                  self.navigationController?.pushViewController(nextViewController, animated: true)
//              }
            
            let usertype = newsFeedDict[indexPath.row]["usertype"] as? String
                                                
                                                if usertype == "Organization"
                                                {
                                                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                                    nextViewController.playerOrglResult = newsFeedDict[indexPath.row]
                                                       nextViewController.isfromsearch = true;
                                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    
                                                    
                                                }
                                                else
                                                {
                                                    
                                                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                                       nextViewController.playerListDetail = newsFeedDict[indexPath.row]
                                                    nextViewController.isfromsearch = true;
                                                  self.navigationController?.pushViewController(nextViewController, animated: true)
                                                }
            
         }
            
         else {
            
            let NewsURL = newsFeedDict[indexPath.row]["TickerURL"] as? String
            let Ticker1ID = newsFeedDict[indexPath.row]["Ticker1ID"] as? String
            
            if (NewsURL != nil) {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "webView") as! webView
                nextViewController.urlString = NewsURL!
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
            
            if Ticker1ID != nil {
              
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                nextViewController.id = Ticker1ID!
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
            if type == "Games" {
                
                let EventStatus = newsFeedDict[indexPath.row]["EventStatus"] as? String
                let LocationID = newsFeedDict[indexPath.row]["LocationID"] as? String
                if EventStatus == "1"
                       {
                           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                            nextViewController.LocationID = LocationID ?? ""
                        nextViewController.Eventstatus = EventStatus ?? ""
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                       }
                           
                       else if EventStatus == "2"
                       {
//                           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GameNameTestController") as! GameNameTestController
//                           nextViewController.LocationID = LocationID ?? ""
//                           self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                        nextViewController.LocationID = LocationID ?? ""
                        nextViewController.Eventstatus = EventStatus ?? ""
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                        
                       }
                           
                       else if EventStatus == "0"
                       {
                           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
                           nextViewController.LocationID = LocationID ?? ""
                        nextViewController.EventStatus = EventStatus ?? ""
                           nextViewController.LocationDetail =  newsFeedDict[indexPath.row]
                        //   nextViewController.firstViewCont = self
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                       }
                
            }
            
        }
    }
    
    
    /*
     
     "Ticker1ID": "1",
     "TickerName1": "Donaldo",
     "Ticker2ID": "2",
     
    */
    
    
    
    // MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
    
    let  UserType = UserDefaults.standard.getUserDict()["usertype"] as? String ?? ""
                 if UserType == "Organization" {
                   
                   print("Organization Login")
                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                          self.navigationController?.pushViewController(nextViewController, animated: true)
                   
                 }
                 else
                 {
                   
                   print("Player Login")
                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                          self.navigationController?.pushViewController(nextViewController, animated: true)
                   
                 }
          
        
    }
    
    @IBAction func BtnLocationAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
         self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func btn_mapView(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
        self.navigationController?.pushViewController(nextViewController, animated: true)
            
        
    }
    
    @IBAction func BtnSearchAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchEventController") as! SearchEventController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    //MARK:- Button Cell Action
    
    @objc func btnCellMessageAction(_ sender : UIButton)
    {
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailView") as! ChatDetailView
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm a"
        
      
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        return Date24

    }
}

