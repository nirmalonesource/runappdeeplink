//
//  ChatAllListView.swift
//  RunApp
//
//  Created by My Mac on 05/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight

class ChatAllListCell: UITableViewCell {
    
    
    
    
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var leadingConstrants: NSLayoutConstraint!
    
    @IBOutlet weak var lblTime: UILabel!
}

class ChatAllListView: UIViewController , UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate,UIGestureRecognizerDelegate{

    @IBOutlet weak var lblTitlechatboard: UILabel!
    
    @IBOutlet var runlogo: UIImageView!
    
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var tblChatView: UITableView!
    
    @IBOutlet weak var vwPopupTopComment: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtWriteComment: UITextField!
    
    @IBOutlet weak var imgCommentUser: UIImageView!
    @IBOutlet weak var lblCommentUser: UILabel!
    @IBOutlet weak var lblCommentTitle: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    
    
    
    
    var commentData = [String:Any]()
    var subCommentList = [[String:Any]]()
    var LocationID: String?
    
    //MARK:- Web service
    func getComment()
    {
        let parameters = ["Comment_ID" : commentData["Comment_ID"] as? String ?? "0" ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_SUB_COMMENT_LIST,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        let arrDict = result["data"] as? [[String:Any]] ?? []
                        let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")

                        self.subCommentList = sortedArray
                        self.tblChatView.reloadData()
                    }
        })
    }
    
    /*
     let str = "Happy to help you \U0001F431"
     
     let data : NSData = str.dataUsingEncoding(NSNonLossyASCIIStringEncoding)!
     let valueUnicode : String = String(data: data as Data, encoding: String.Encoding.utf8)!
     
     let dataa   : NSData = valueUnicode.data(using: String.Encoding.utf8)! as NSData
     let valueEmoj : String = String(data: dataa as Data, encoding: String.Encoding.nonLossyASCII)!
   */
    
    func addComment()
    {
//        let str = txtWriteComment.text!
//
//        let data : NSData = str.data(using: String.Encoding.nonLossyASCII)! as NSData
//        let valueUnicode : String = String(data: data as Data, encoding: String.Encoding.utf8)!
        let data =  txtWriteComment.text!.data(using: .nonLossyASCII)
        let valueUnicode : String = String.init(data: data!, encoding: .utf8)!
        
        let parameters = ["Location_ID" : LocationID! ,
                          "User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                          "Sub_ID" : commentData["Comment_ID"] as? String ?? "0" ,
                          "Comment" : valueUnicode] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.ADD_COMMENT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.getComment()
                        self.txtWriteComment.text = ""
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
        
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
             
             
        
        
       // hideKeyboardWhenTappedAround()
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lblTitlechatboard.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblCommentUser.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblCommentTitle.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblCount.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtWriteComment.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
        print("commentData: \(commentData)")
        
        self.view.layoutIfNeeded()
        txtWriteComment.setLeftPaddingPoints(30, strImage: "")
        txtWriteComment.layer.masksToBounds = true
        txtWriteComment.layer.cornerRadius = 20.0

        txtWriteComment.setRadiusBorder(color: UIColor.red)
        
//        imgBG.setGredient()
        
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        tblChatView.tableFooterView = UIView()
        getComment()
        
        lblCommentUser.text = commentData["FirstName"] as? String ?? "Unknown name"
        lblCommentTitle.text = commentData["Comment"] as? String
        lblCount.text =  "\(commentData["total"] as? String ?? "")Replies"
        imgUser.setRadius(radius: imgUser.frame.height/2)
       
        imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        imgCommentUser.setRadius(radius: imgCommentUser.frame.height/2)
        let imgURL = ServiceList.IMAGE_URL + "\(commentData["UserProfile"] as? String ?? "")"
        imgCommentUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,completed: nil)
        self.view.layoutIfNeeded()
    
        
        
               let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
                       tapGestureRecognizer2.delegate = self
                       runlogo.isUserInteractionEnabled = true
                       runlogo.addGestureRecognizer(tapGestureRecognizer2)
        
        
        
    }
    
    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
       
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
         nextViewController.type = "Adv"
        self.navigationController?.pushViewController(nextViewController, animated: true)
      
        
    }
    //MARK: - TextField Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtWriteComment.text == ""
        {
            showAlert(uiview: self, msg: "Please enter comment", isTwoButton: false)
        }
        else
        {
            addComment()
            
        }
    }
    
    //MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCommentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatAllListCell", for: indexPath) as! ChatAllListCell
        
        cell.lblName.text = subCommentList[indexPath.row]["FirstName"] as? String ?? "Unknown name"
        
        let getCommet = subCommentList[indexPath.row]["Comment"] as? String
        
//        let dataa : NSData = getCommet!.data(using: String.Encoding.utf8)! as NSData
//        cell.lblDescription.text = String(data: dataa as Data, encoding: String.Encoding.nonLossyASCII)!
        let emojData = getCommet!.data(using: .utf8)
        cell.lblDescription.text = String.init(data: emojData!, encoding: .nonLossyASCII)
        
        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        cell.imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        
        let imgURL = ServiceList.IMAGE_URL + "\(subCommentList[indexPath.row]["UserProfile"] as? String ?? "")"
        cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                 completed: nil)
        cell.lblTime.text = "\(subCommentList[indexPath.row]["CreatedOn"] as? String ?? "")".toDate().timeAgoSinceDate()
        
        
        
        cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        cell.lblName.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        
        
        cell.selectionStyle = .none
        return cell
        
    }

}

