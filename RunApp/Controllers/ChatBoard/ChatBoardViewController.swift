//
//  ChatBoardViewController.swift
//  RunApp
//
//  Created by My Mac on 13/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
///

import UIKit
import NightNight
import CoreLocation
import ActionSheetPicker_3_0


struct cellData {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class chatTableCell: UITableViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var leadingConstrants: NSLayoutConstraint!
    @IBOutlet weak var lblReplies: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
}

class UpcomingRunViewCell1: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPm: UILabel!
    @IBOutlet weak var lblYoute: UILabel!
    @IBOutlet weak var btnRsvp: UIButton!
    @IBOutlet weak var lblspectatorfees: UILabel!
    
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblplayerfees: UILabel!
    
}




class ChatBoardViewController: UIViewController , UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate {
    
    
    var Amt = ""
    var PlayerAmt = ""
    var AmountGet = ""
    var PlayerType = ""
    var RSVPTitle = ""
    var shareurl = String()
    
    @IBOutlet weak var btnSpectatorOutlet: UIButton!

    @IBOutlet weak var btnJoinOutlet: UIButton!
       
    @IBOutlet weak var lblamounttobepaid: UILabel!
    
    @IBOutlet var imgBLur: UIImageView!
    
    @IBOutlet var vwPopup: UIView!
    
    @IBOutlet var tblupcomingheight: NSLayoutConstraint!
    
    var SpectatorArray = [String]()
    
    @IBOutlet var runlogo: UIImageView!
    @IBOutlet weak var lblChartboard: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    
    @IBOutlet var tblupcoming: UITableView!
    
    
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var lblAddressDetail: UILabel!
    
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblAvg: UILabel!
    @IBOutlet weak var lblRunSince: UILabel!
    
    @IBOutlet weak var tblChatView: UITableView!
    
    @IBOutlet weak var vwPopupTopComment: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtWriteComment: UITextField!
    @IBOutlet weak var TableHeightConstraints: NSLayoutConstraint!
    
    
    
    var rsvpDictList = [[String : Any]]()
    var rsvpList = [[String : Any]]()
    var rsvpListGet = [[String : Any]]()
     var EventID = ""
    var commentList = [[String:Any]]()
    var LocationDetail = [String : Any]()
    var tableViewData = [cellData]()
    var LocationID: String?
    var EventStatus:String?
    var dictLocation = [String : Any]()
    var firstViewCont:HomeView!
   
    
    var lagitude = Double()
      var logitude = Double()
      var locationManager = CLLocationManager()
     var flag = Bool()
    var isfromnotification = false
    
      
    
   @IBOutlet weak var btnCheckInOutlet: UIButton!
    @IBOutlet var btncheckmark: UIButton!
    
    //MARK:- Web service
    
    func getPlaces()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOCATIONS,
                method: .post,
                param: ["LocationID" : LocationID!] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        let placeList = result["data"] as? [[String:Any]] ?? []
                        //2019-05-01 07:07:16
                        
                        for i in 0..<placeList.count {
                            self.lblTitle.text = placeList[i]["LocationName"] as? String ?? ""
                            self.lblAddressDetail.text = placeList[i]["LocationAddress"] as? String ?? ""
//                            let imgPin = placeList[i]["product_image"] as? String ?? ""
//                            let Distance = placeList[i]["product_image"] as? String ?? ""
                             let cretedOn = placeList[i]["RegistrationDate"]as? String ?? ""
                             self.lblRunSince.text = "Run site Since: \(cretedOn)"
                           //  let Since = cretedOn.toDate3()
                             self.lblRunSince.text = "Run site Since: \(cretedOn)"
                             self.lblAvg.text = "\(placeList[i]["LocMode"] as? String ?? "")"
                        }
                        
                       // self.tblView2.reloadData()
                    }
        })
    }
    
    func getComment()
    {
        
        let parameters = ["Location_ID" : LocationID ?? ""] as [String : Any]

        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_MAIN_COMMENT_LIST,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    
                    if result.getBool(key: "status")
                    {
                        //let arrDict = result["data"] as? [[String:Any]] ?? []
                        let GetData:NSDictionary = result["data"] as? NSDictionary ?? [:]
                        let arrDict = GetData.value(forKey: "comment") as? [[String:Any]] ?? []
                        self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
                        self.parseLocation(LocationDict: self.dictLocation)
                    
                        let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
                        self.commentList = sortedArray
                        self.tblChatView.reloadData()
                        self.view.layoutIfNeeded()
                        self.TableHeightConstraints.constant = self.tblChatView.contentSize.height
                        self.view.layoutIfNeeded()
                    }
        })
    }
    
    func parseLocation(LocationDict : [String:Any]) {
        
        self.lblTitle.text = LocationDict["LocationName"] as? String ?? ""
        self.lblAddressDetail.text = LocationDict["LocationAddress"] as? String ?? ""
        self.EventID = LocationDict["EventID"] as? String ?? ""
        let cretedOn = LocationDict["RegistrationDate"] as? String ?? ""
        let Since = cretedOn.toDate3()
        self.lblRunSince.text = "Run site Since: \(Since)"
        
        //                            let imgPin = placeList[i]["product_image"] as? String ?? ""
        //                            let Distance = placeList[i]["product_image"] as? String ?? ""
        
        self.lblAvg.text = "\(LocationDict["LocMode"] as? String ?? "")"
        print("LocationDict: \(LocationDict)")
    }
    
    
    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
            
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
              nextViewController.type = "Adv"
             self.navigationController?.pushViewController(nextViewController, animated: true)
           
             
         }
    
    func addComment()
    {
        let parameters = ["Location_ID" : LocationID! ,
                          "User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                          "Sub_ID" : 0 ,
                          "Comment" : txtWriteComment.text!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.ADD_COMMENT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.getComment()
                        self.txtWriteComment.text = ""
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func AppCheckInAdd()
    {
        let parameters = ["Location_ID" : LocationID! ,
                          "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "Lat" : lagitude,
                          "Long" : logitude
                          ] as [String : Any]
      
        print(parameters)
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_ADD,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                     //  showToast(uiview: self, msg: result.getString(key: "message"))
                        self.btnCheckInOutlet.setTitle("Checked In", for: .normal)
                                               self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
                                               self.imgLocation.image = UIImage(named: "map_green")
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "checkinlist") as! checkinlist;
                                           nextViewController.LocationID = self.LocationID
                                           self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                        
                    else {
                        showToast(uiview: self, msg: result.getString(key: "message"))
//                        self.btnCheckInOutlet.setTitle("Checked In  ", for: .normal)
//                        self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
//                        self.imgLocation.image = UIImage(named: "map_green")
                        
                    }
                    
        })
    }
    
    func AppCheckInList()
    {
        let parameters = ["Location_ID" : LocationID! ,
                          "Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_CHECK_IN_LIST,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.btnCheckInOutlet.setTitle("Checked In", for: .normal)
                        self.btnCheckInOutlet.setTitleColor(UIColor.green, for: .normal)
                      // self.imgLocation.image = UIImage(named: "map_green")
                    }
                        
                    else {
                        self.btnCheckInOutlet.setTitle("Check In", for: .normal)
                        self.btnCheckInOutlet.setTitleColor(UIColor.red, for: .normal)
                       // self.imgLocation.image = UIImage(named: "redLocation")
                    }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
      {
         if flag {
             flag = false
             if let location = locations.last
             {
                 //print("Found user's location: \(location)")
                 print("latitude: \(location.coordinate.latitude)")
                 print("longitude: \(location.coordinate.longitude)")
                 lagitude = location.coordinate.latitude
                 logitude = location.coordinate.longitude
                 locationManager.stopUpdatingLocation()
                // getlocationinfo()
             }
         }
             
     }
     
     func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("Unable to access your current location")
        }
    
    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
     
        appGetRsvpLocationForUpcoming()
        
        
        
    //    vwPopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        //lblamounttobepaid.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
        
        
        SpectatorArray = ["Spectator","Player"]
//        btnSpectatorOutlet.setRadius(radius: 12)
//              btnSpectatorOutlet.setRadiusBorder(color: UIColor.gray)
//              btnSpectatorOutlet.roundCorners(corners: .allCorners, radius: 2)
//
//              vwPopup.setRadius(radius: 10)
//
//        btnJoinOutlet.setRadius(radius: btnJoinOutlet.frame.height/2)
        
             // btnAddEventOutlet.imageView?.changeImageViewImageColor(color: UIColor.gray)
             // app_Event_Status_Api()
              
        
        
        
        self.view.layoutIfNeeded()
        vwPopupTop.setRadius(radius: 10)
      //  tblupcoming.tableFooterView = UIView()
        self.view.layoutIfNeeded()
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
               tapGestureRecognizer.delegate = self
               runlogo.isUserInteractionEnabled = true
               runlogo.addGestureRecognizer(tapGestureRecognizer)
        
        
        
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lblTitle.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
       // vwPopupTopComment.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtWriteComment.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblChartboard.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        
        txtWriteComment.setLeftPaddingPoints(30, strImage: "")
        txtWriteComment.layer.masksToBounds = true
        txtWriteComment.layer.cornerRadius = 20.0
        
        txtWriteComment.setRadiusBorder(color: UIColor.red)
        
//        imgBG.setGredient()
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
       // tblView.tableFooterView = UIView()
        self.view.layoutIfNeeded()
        
        if CLLocationManager.locationServicesEnabled() == true {
                   
                   if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                       locationManager.requestWhenInUseAuthorization()
                   }
                   
                   locationManager.desiredAccuracy = kCLLocationAccuracyBest
                   locationManager.delegate = self
                   locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                   locationManager.requestWhenInUseAuthorization()
                   locationManager.startUpdatingLocation()
                   locationManager.requestLocation()
                   flag = true
                   // mapVW.mapType = .hybrid
               } else {
                   print("Please turn on location services or GPS")
               }
        if EventStatus == "0"
        {
            imgLocation.image = UIImage(named: "redLocation")
        }
        else if EventStatus == "1"
        {
            imgLocation.image = UIImage(named: "yellowLocation")
        }
        else
        {
           imgLocation.image = UIImage(named: "map_green")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appGetRsvpLocation()
        if EventStatus == "0"
               {
                   imgLocation.image = UIImage(named: "redLocation")
               }
               else if EventStatus == "1"
               {
                   imgLocation.image = UIImage(named: "yellowLocation")
               }
               else
               {
                  imgLocation.image = UIImage(named: "map_green")
               }
         getComment()
         AppCheckInList()
        self.view.layoutIfNeeded()
        
        
    }
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        self.view.layoutIfNeeded()
        super.viewDidLayoutSubviews()
        
             TableHeightConstraints.constant = tblChatView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    // MARK: - TextField Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
     // MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if isfromnotification {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    
    @IBAction func btnSendPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtWriteComment.text == ""
        {
            showAlert(uiview: self, msg: "Please enter comment", isTwoButton: false)
        }
        else
        {
            addComment()
        }
    }
    
    @IBAction func btnAddLocationAction(_ sender: UIButton) {
       
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    @IBAction func btn_gym(_ sender: Any) {

        if let link = NSURL(string: shareurl)
                          {
                           let objectsToShare = [link] as [Any]
                              let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                           activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                           self.present(activityVC, animated: true, completion: nil)
                          }
        
        
        
    }
    //
//    @IBAction func btn_seemore(_ sender: Any) {
//     
//         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SeeMoreViewController") as! SeeMoreViewController
//               nextViewController.LocationID = LocationID
//               nextViewController.EventID = EventID
//               nextViewController.rsvpDictList = rsvpListGet
//               self.navigationController?.pushViewController(nextViewController, animated: true)
//    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
       
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchEventController") as! SearchEventController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btn_checkinlist(_ sender: Any) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventCheckInListViewController") as! EventCheckInListViewController
      
        nextViewController.locationid = LocationID 
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    @IBAction func btnEventAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewEventViewController") as! AddNewEventViewController
        nextViewController.LocationDetail = LocationDetail
        nextViewController.LocationID = LocationID
        nextViewController.FViewCont = self.firstViewCont
        nextViewController.EventID1 = EventID
        nextViewController.rsvpListGet1 = rsvpListGet
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
//    @IBAction func BtnGroupAction(_ sender: UIButton) {
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatCommentsListView") as! ChatCommentsListView
//        nextViewController.LocationID = LocationID
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//    }
    
     @IBAction func BtnCheckedInAction(_ sender: UIButton) {
        print(sender.title)
       (btnCheckInOutlet.currentTitle)
        
        if btnCheckInOutlet.currentTitle != "Checked In" {
            AppCheckInAdd()
        }
        else{
            
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "checkinlist") as! checkinlist;
        nextViewController.LocationID = self.LocationID
        self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
    }
    
    //Already Checkedin
    
    // MARK: - TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableViewData[section].opened == true {
//            return tableViewData[section].sectionData.count + 1
//        }
//        else{
//            return 1
//        }
        
        if tableView == tblChatView {
        
        return commentList.count > 6 ?  5 : commentList.count
            
        }
        else
        {
            return rsvpDictList.count
        }
            
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblChatView
        {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatTableCell", for: indexPath) as! chatTableCell
        
        cell.lblName.text = commentList[indexPath.row]["FirstName"] as? String ?? "Unknown name"
        cell.lblDescription.text = commentList[indexPath.row]["Comment"] as? String
        cell.lblReplies.text = "\(commentList[indexPath.row]["total"] as? String ?? "")Replies"
        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        cell.imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        
        let imgURL = ServiceList.IMAGE_URL + "\(commentList[indexPath.row]["UserProfile"] as? String ?? "")"
        cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                  completed: nil)
            
            let gesture = UITapGestureRecognizer()
              gesture.addTarget(self, action: #selector(userImageClicked(_:)))
              gesture.numberOfTapsRequired = 1
              gesture.numberOfTouchesRequired = 1
            cell.imgUser.addGestureRecognizer(gesture)
            cell.imgUser.tag = indexPath.row
            cell.imgUser.isUserInteractionEnabled = true
            
        cell.lblTime.text = "\(commentList[indexPath.row]["CreatedOn"] as? String ?? "")".toDate().timeAgoSinceDate()

        cell.lblName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
//        let dataIndex = indexPath.row - 1
//        if  indexPath.row == 0 {
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "chatTableCell") as? chatTableCell else { return UITableViewCell()}
//            cell.lblName.text = tableViewData[indexPath.section].title
//            cell.leadingConstrants.constant = 0
//            cell.selectionStyle = .none
//            return cell
//        }
//        else{
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "chatTableCell") as? chatTableCell else { return UITableViewCell()}
//           // cell.textLabel?.text = tableViewData[indexPath.section].sectionData[dataIndex]
//            cell.lblName.text = tableViewData[indexPath.section].sectionData[dataIndex]
//            cell.leadingConstrants.constant = 40
//            //cell.removeSeparatorLeftPadding()
//            cell.selectionStyle = .none
//        }
        
//        cell.mixedBackgroundColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
            
        return cell
        }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingRunViewCell1", for: indexPath) as! UpcomingRunViewCell1
                        cell.lblYoute.text = rsvpDictList[indexPath.row]["EvenetName"] as? String
                        let cretedOn = rsvpDictList[indexPath.row]["EventDate"] as? String ?? ""
                        cell.lblDate.text = cretedOn.toDate5()
                    //    cell.lblPm.text = "Max Player: " + String(rsvpDictList[indexPath.row]["MaxPlayer"] as? String ?? "")
                            
                        cell.lblLoc.text =   "LOC " + String(rsvpDictList[indexPath.row]["CLVLTitle"] as? String ?? "")
                          
                        cell.btnRsvp.layer.cornerRadius = 5.0
                        cell.btnRsvp.layer.masksToBounds = true
                        cell.btnRsvp.tag = indexPath.row
                        
                     cell.lblspectatorfees.text = "Spectator Price: " + String(rsvpDictList[indexPath.row]["Amt"] as? String ?? "")
                     cell.lblplayerfees.text = "Player Price: " + String(rsvpDictList[indexPath.row]["PlayerAmt"] as? String ?? "")
                        
                        
                        if UserDefaults.standard.getUserDict()["usertype"] as? String ?? "" != "Organization" {
                          //  cell.btnRsvp.isHidden = false
                        //    cell.btnRsvp.addTarget(self, action: #selector(btnRsvpAction), for: .touchUpInside)
                            
                             let RSVPD = rsvpDictList[indexPath.row]["RSVPD"] as? String ?? ""
                            if RSVPD == "1"
                            {
                //                cell.btnRsvp.setTitle("RSVP'd", for: .normal)
                //                cell.btnRsvp.isEnabled = false
                                  cell.btnRsvp.setTitle("Cancel", for: .normal)
                                   cell.btnRsvp.isEnabled = true
                            }
                            else
                            {
                                cell.btnRsvp.setTitle("RSVP", for: .normal)
                                cell.btnRsvp.isEnabled = true
                            }
                            
                            
                            let IsEventOneHour = rsvpDictList[indexPath.row]["IsEventOneHour"] as? Int ?? 3
                                
                                if IsEventOneHour == 1
                            {
                                cell.btnRsvp.setTitle("EventCheckIn", for: .normal)
                                
                            }
                           let IsEventCheckedIn = rsvpDictList[indexPath.row]["IsCheckedIn"] as? Int ?? 3
                                                           
                                    if IsEventCheckedIn == 1
                                            {
                                                           cell.btnRsvp.setTitle("CheckedIn", for: .normal)
                                                           cell.btnRsvp.isEnabled = false
                                            }
                        }
                            
                        else
                        {
                            cell.btnRsvp.isHidden = true
                        }
                        
                        
                        cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
                        cell.lblDate.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
                        //cell.lblPm.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
                        cell.lblYoute.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
                         cell.lblplayerfees.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
                        cell.lblspectatorfees.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
                        cell.lblLoc.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
                        
                        
                        return cell
            }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblChatView
        {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatAllListView") as! ChatAllListView
        nextViewController.LocationID = LocationID
        nextViewController.commentData = commentList[indexPath.row]
        self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            
            let IR = rsvpDictList[indexPath.row]["IR"] as? String ?? ""
            
            if IR == "ON"
            {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "IREventDetailViewController") as! IREventDetailViewController
                             nextViewController.Event_ID = rsvpDictList[indexPath.row]["EventID"] as? String ?? ""
                             nextViewController.Event_Name = rsvpDictList[indexPath.row]["EvenetName"] as? String ?? ""
                       
                             self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
            else
            {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailView") as! EventDetailView
                  nextViewController.Event_ID = rsvpDictList[indexPath.row]["EventID"] as? String ?? ""
                  nextViewController.Event_Name = rsvpDictList[indexPath.row]["EvenetName"] as? String ?? ""
            
                  self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatCommentsListView") as! ChatCommentsListView
//        nextViewController.LocationID = LocationID
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    
//        if indexPath.row == 0 {
//            if tableViewData[indexPath.section].opened == true {
//                tableViewData[indexPath.section].opened = false
//                let sections = IndexSet.init(integer:indexPath.section)
//                tableView.reloadSections(sections, with: .none) //play around with this
//            }
//
//            else {
//                //use diffent cell identifier if needed
//                tableViewData[indexPath.section].opened = true
//                let sections = IndexSet.init(integer:indexPath.section)
//                tableView.reloadSections(sections, with: .none) //play around with this
//            }
//        }
//
//         tblChatView.reloadData()
//         self.view.layoutIfNeeded()
//         TableHeightConstraints.constant = tblChatView.contentSize.height
//         self.view.layoutIfNeeded()
        
    }
    
    @objc func userImageClicked(_ sender: UITapGestureRecognizer) {
        print("image clicked",sender.view?.tag ?? 0)
        let index = sender.view?.tag ?? 0
        
        guard let usertype = commentList[index]["usertype"] as? String else {return}
                                            if usertype == "Organization"
                                            {
                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                                nextViewController.playerOrglResult = commentList[index]
                                                   nextViewController.isfromsearch = true;
                                               self.navigationController?.pushViewController(nextViewController, animated: true)


                                            }
                                            else
                                            {

                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                                   nextViewController.playerListDetail = commentList[index]
                                                nextViewController.isfromsearch = true;
                                              self.navigationController?.pushViewController(nextViewController, animated: true)
                                            }
    }
    
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
    
    func appGetRsvpLocation()
       {
           let parameters = ["LocationID": LocationID ?? "","EventID":EventID,"PlayerID": UserDefaults.standard.getUserDict()["id"] as? String ?? ""
               ] as [String : Any]
           
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]

           callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_RSVP_LOCATION,
                   method: .post,
                   param: parameters ,
                   extraHeader: header ,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                           //let data = result["data"] as? [[String:Any]] ?? []
                           
                           let GetData:NSDictionary = result["data"] as! NSDictionary
                        self.rsvpListGet = GetData.value(forKey: "event") as? [[String:Any]] ?? []
                           print("rsvpListGet:\(self.rsvpListGet)")
                           let jsonarr:NSArray = GetData.value(forKey: "event") as! NSArray
                           if jsonarr.count > 0
                            
                            
                           {
                                let dict1:NSDictionary = jsonarr.lastObject as! NSDictionary
                               self.rsvpList = [[String : Any]]()
                               self.rsvpList.append(dict1 as! [String : Any])
                           }
                          
                        //   let dict2:NSDictionary = jsonarr.object(at: jsonarr.count-2) as! NSDictionary
                          
                          // self.rsvpList.append(dict2 as! [String : Any])
                          
                           
                           self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
                            self.parseLocation(LocationDict: self.dictLocation)
                        self.shareurl = result.getString(key: "RedirectURL") as? String ?? ""
                       }
                       else
                       {
                           showToast(uiview: self, msg: result.getString(key: "message"))
                       }
                       
                       DispatchQueue.main.async {
                           self.tblChatView.reloadData()
                         //  self.constTblUpcomingHeight.constant = self.tblUpcomingView.contentSize.height
                       }
           })
       }
    
    
    
    
    
    //upcoming
    @objc func btnRsvpAction(button: UIButton) {
            btnJoinOutlet.tag = button.tag
            RSVPTitle =   button.currentTitle!
            
            let buttonRow = button.tag
            print("\(buttonRow): Btn Cell is Pressed")
            self.Amt = rsvpDictList[button.tag]["Amt"] as? String ?? ""
            self.PlayerAmt = rsvpDictList[button.tag]["PlayerAmt"] as? String ?? ""
            self.EventID = rsvpDictList[button.tag]["EventID"] as? String ?? ""
            self.PlayerType = "Spectator"
            self.AmountGet = self.Amt
            self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
            
            let BookingStatus = rsvpDictList[button.tag]["BookingStatus"] as? String ?? ""
            
            if self.rsvpDictList[button.tag]["EventType"] as! String == "Paid"
            {
                   if  self.rsvpDictList[button.tag]["IsPayment"] as! Int == 0
                {
                   self.btnJoinOutlet.setTitle("PAY NOW", for: .normal)
                   }
                else
                   {
                  self.btnJoinOutlet.setTitle("CHECK IN", for: .normal)
                   }
                
                
            }
            else
            {
                self.btnJoinOutlet.setTitle("JOIN", for: .normal)
            }
            
            if BookingStatus == "0" {
                //showViewWithAnimation(vw: vwPopup, img: imgBLur)
            }
                
            else {
                showAlert(uiview: self, msg: "Booking Closed", isTwoButton: false)
            }
            
            let RSVPD = rsvpDictList[button.tag]["RSVPD"] as? String ?? ""
            
            
                  
            if RSVPTitle == "Cancel"
                   {
                       appCancelSpectator()
                   }
            
            
            
    //              if RSVPD == "1"
    //              {
    //                   appCancelSpectator()
    //              }
          //  showViewWithAnimation(vw: vwPopup, img: imgBlur)
        }
        
        @IBAction func btnJoinSpectatorClicked(_ sender: UIButton) {
            
    //        if self.rsvpDictList[sender.tag]["EventType"] as! String == "Paid"
    //        {
    //            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
    //            nextViewController.Amount = AmountGet
    //            nextViewController.EventID = EventID
    //            nextViewController.PlayerType = PlayerType
    //            nextViewController.LocationID = LocationID
    //
    //            self.navigationController?.pushViewController(nextViewController, animated: true)
    //        }
    //        else
    //        {
    //            //self.btnPayOutlet.setTitle("Join", for: .normal)
    //            appSpectatorJoin()
    //        }
            
           
            
         
               
                    if RSVPTitle == "RSVP"
                    {
                     
                if AmountGet == "0"
                
                {
                       appSpectatorJoin()
                    
                }
                else
                {
                    if self.rsvpDictList[sender.tag]["EventType"] as! String == "Paid"
                {
                    
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
                    nextViewController.Amount = AmountGet
                    nextViewController.EventID = EventID
                    nextViewController.PlayerType = PlayerType
                    nextViewController.LocationID = LocationID
                    
                    
                    nextViewController.rsvpDictList1 = rsvpDictList[sender.tag]
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                    
                }
                else
                {
                    //self.btnPayOutlet.setTitle("Join", for: .normal)
                    appSpectatorJoin()
                    
                }
                }
                    }
                    else if RSVPTitle == "EventCheckIn" {
                        if AmountGet == "0"
                        {
                            appEventCheckin()
                        }
                        else
                        {
                        if self.rsvpDictList[sender.tag]["EventType"] as! String == "Paid"
                            {
                                
                            if self.rsvpDictList[sender.tag]["IsPayment"] as! Int == 0
                            {
                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
                                nextViewController.Amount = AmountGet
                                nextViewController.EventID = EventID
                                nextViewController.PlayerType = PlayerType
                                nextViewController.LocationID = LocationID
                                nextViewController.rsvpDictList1 = rsvpDictList[sender.tag]
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                
                                }
                            else{
                                
                                 appEventCheckin()
                                
                                }
                            }
                            else
                            {
                                //self.btnPayOutlet.setTitle("Join", for: .normal)
                                appEventCheckin()
                            }
                    }
            }
                
      
        }
        
        
        func appEventCheckin()
               {
                   if EventID != "" {
                       let parameters = ["Location_Id": LocationID ?? "" ,
                                         "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                         "Event_ID": EventID, "PlayerType" : PlayerType,"Lat":lagitude,"Long":logitude
                                         
                           ] as [String : Any]
                       
                       let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                     "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                     ] as [String : Any]
                       
                       callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerEventCheckIn,
                               method: .post,
                               param: parameters ,
                               extraHeader: header ,
                               completionHandler: { (result) in
                                   print(result)
                                   self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBLur)
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                                   if result.getBool(key: "status")
                                   {
                                       
                                      
                                   }
                                   else
                                   {
                                      // showToast(uiview: self, msg: result.getString(key: "message"))
                                   }
                                    self.appGetRsvpLocation()
                       })
                   }
                       
                   else {
                       print("Event id is Empty")
                   }
                   
               }
           
        
        @IBAction func btnSpectatorAction(_ sender: UIButton) {
            self.view.endEditing(true)
            if SpectatorArray.count != 0
            {
                ActionSheetStringPicker.show(withTitle: "Select Type", rows: SpectatorArray, initialSelection: 0, doneBlock: {
                    picker, indexes, values in
                    
                    self.btnSpectatorOutlet.setTitle("\(values!)", for: .normal)
                    
                    self.PlayerType  =  values as! String
                    
                    
                                    if self.btnSpectatorOutlet.currentTitle == "Spectator" {
                                        self.AmountGet = self.Amt
                                        self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
                                        self.PlayerType = "Spectator"
                                    }
                                        
                                    else if self.btnSpectatorOutlet.currentTitle == "Player" {
                                        self.AmountGet = self.PlayerAmt
                                        self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
                                        self.PlayerType = "Player"
                                    }
                                    
                                    else {
                                        self.AmountGet = self.PlayerAmt
                                        self.lblamounttobepaid.text = "Amount to be paid : $\(self.AmountGet)"
                                        self.PlayerType = "Both"
                                    }
                    
                    
                                    return
                                }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
                    
            }
                    //                        if self.btnSpectatorOutlet.currentTitle == "Spectator" {
                    //                            self.btnJoinOutlet.setTitle("JOIN NOW", for: .normal)
                    //                        }
                    //
                    //                        else {
                    //                           self.btnJoinOutlet.setTitle("PAY NOW", for: .normal)
                    //                        }
                    
                    //self.strStateID = self.stateData[indexes]["state_id"] as? String ?? ""
          
        }
        
       
        
        // MARK:- TOUCHES BEGAN
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            
        //    hideViewWithAnimation(vw: vwPopup, img: imgBLur)
        
        }
        
    //    func appSpectatorJoin()
    //    {
    //        let parameters = ["Location_Id": LocationID ?? "" ,
    //                          "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
    //                          "Event_ID": EventID
    //            ] as [String : Any]
    //
    //        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
    //                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
    //                      ] as [String : Any]
    //
    //        callApi(ServiceList.SERVICE_URL+ServiceList.APP_SPECTATOR_JOIN,
    //                method: .post,
    //                param: parameters ,
    //                extraHeader: header ,
    //                completionHandler: { (result) in
    //                    print(result)
    //                    if result.getBool(key: "status")
    //                    {
    //
    //
    //                    }
    //                    self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
    //                    showToast(uiview: self, msg: result.getString(key: "message"))
    //                    self.appGetRsvpLocation()
    //        })
    //    }
        
        func appSpectatorJoin()
          {
              let parameters = ["Location_Id": LocationID ?? "" ,
                                "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                "Event_ID": EventID,
                                "PlayerType" : PlayerType
                  ] as [String : Any]
              
              let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                            "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                            ] as [String : Any]
              
              callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerJoin,
                      method: .post,
                      param: parameters ,
                      extraHeader: header ,
                      completionHandler: { (result) in
                          print(result)
                          if result.getBool(key: "status")
                          {
                            
                            
                          }
                    //      self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBLur)
                          showToast(uiview: self, msg: result.getString(key: "message"))
                          self.appGetRsvpLocation()
              })
          }
        
        
        
        func appCancelSpectator()
         {
             if EventID != "" {
                 let parameters = ["Location_Id": LocationID ?? "" ,
                                   "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                   "Event_ID": EventID
                     ] as [String : Any]
                 
                 let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                               "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                               ] as [String : Any]
                 
                 callApi(ServiceList.SERVICE_URL+ServiceList.APP_SPECTATOR_CANCEL,
                         method: .post,
                         param: parameters ,
                         extraHeader: header ,
                         completionHandler: { (result) in
                             print(result)
                         //    self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBLur)
                             showToast(uiview: self, msg: result.getString(key: "message"))
                             if result.getBool(key: "status")
                             {
                                 
                                // self.btnPayOutlet.setTitle("PAY NOW", for: .normal)
                             }
                             else
                             {
                                // showToast(uiview: self, msg: result.getString(key: "message"))
                             }
                              self.appGetRsvpLocation()
                            
                 })
             }
                 
             else {
                 print("Event id is Empty")
             }
             
         }
    
    
       func appGetRsvpLocationForUpcoming()
        {
            let parameters = ["LocationID": LocationID  ?? "","EventID":EventID,"PlayerID": UserDefaults.standard.getUserDict()["id"] as? String ?? ""
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                ] as [String : Any]
            
            callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_RSVP_LOCATION,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)
                        if result.getBool(key: "status")
                        {
                            //let data = result["data"] as? [[String:Any]] ?? []
                             self.rsvpDictList = [[String : Any]]()
                            let GetData:NSDictionary = result["data"] as! NSDictionary
                            self.rsvpDictList = GetData.value(forKey: "event") as? [[String:Any]] ?? []
                            print("rsvpListGet:\(self.rsvpDictList)")
                            
                            let eventarr = GetData.value(forKey: "event") as? [[String:Any]] ?? []
                            if eventarr.count > 0 {
                                self.shareurl = eventarr[0]["RedirectURL"] as? String ?? ""
                            }
                            
                            
                         //   let jsonarr:NSArray = GetData.value(forKey: "event") as! NSArray
                         //   let dict1:NSDictionary = jsonarr.lastObject as! NSDictionary
                            //   let dict2:NSDictionary = jsonarr.object(at: jsonarr.count-2) as! NSDictionary
                          //  self.rsvpDictList.append(jsonarr as! [String : Any])
                            // self.rsvpList.append(dict2 as! [String : Any])
                           
                            
    //                        self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
    //                        self.parseLocation(LocationDict: self.dictLocation)
                        }
                         // self.tblupcoming.reloadData()
                        DispatchQueue.main.async {
                            if self.rsvpDictList.count == 0
                            {
                             // self.tblupcomingheight.constant = 0
                              //  self.tblupcoming.reloadData()
                        }
                        }
            })
        }
        
}

extension UITableViewCell
{
    func removeSeparatorLeftPadding() -> Void
    {
        if self.responds(to: #selector(setter: separatorInset)) // Safety check
        {
            self.separatorInset = UIEdgeInsets.zero
        }
        if self.responds(to: #selector(setter: layoutMargins)) // Safety check
        {
            self.layoutMargins = UIEdgeInsets.zero
        }
    }
}
