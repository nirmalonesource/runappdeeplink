//
//  OpenChatView.swift
//  RunApp
//
//  Created by My Mac on 19/11/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight

class AllForumCell: UITableViewCell {
    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblcreatedby: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
            lblQuestion.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
            lblcreatedby.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
            lblTime.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
       }
}


class OpenChatView: UIViewController, UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate,UITextViewDelegate {

    
    @IBOutlet var vwnewquestion: UIView!
    @IBOutlet var lblquestion: UILabel!
    @IBOutlet var txtquestion: UITextView!
    @IBOutlet var lbldesc: UILabel!
    @IBOutlet var txtdesc: UITextView!
    @IBOutlet var scrollview: UIScrollView!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var tblChatView: UITableView!
    
  
  //  var commentData = [String:Any]()
    var tablearr = [[String:Any]]()
     var allcomentarr = [[String:Any]]()
     var mycommentarr = [[String:Any]]()
    var LocationID: String?
    
    var imgBG1 = UIImageView()
    
    
    //MARK:- Web service
    func GetAllForums()
    {
        let parameters = [:] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_ALL_FORUMS,
                method: .get,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        let arrDict = result["data"] as? [[String:Any]] ?? []
                      //  let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")

                        self.allcomentarr = arrDict
                        self.tablearr = self.allcomentarr
                        self.tblChatView.reloadData()
                    }
        })
    }
    
    
    func GetMyForums()
      {
          let parameters = [:] as [String : Any]
          
          let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                        "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                        ] as [String : Any]
          
          callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_MY_FORUMS,
                  method: .get,
                  param: parameters ,
                  extraHeader: header ,
                  completionHandler: { (result) in
                      print(result)
                      if result.getBool(key: "status")
                      {
                          let arrDict = result["data"] as? [[String:Any]] ?? []
                        //  let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")

                          self.mycommentarr = arrDict
                         
                      }
          })
      }
    
    func AddQuestion()
         {
             self.view.endEditing(true)
            let parameters = ["QuestionName" : txtquestion.text,"QuestionDescription":txtdesc.text ] as [String : String]
             
             let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                           ] as [String : Any]
             
             callApi(ServiceList.SERVICE_URL+ServiceList.APP_ADD_FORUM,
                     method: .post,
                     param: parameters ,
                     extraHeader: header ,
                     completionHandler: { (result) in
                         print(result)
                         if result.getBool(key: "status")
                         {
                            self.txtquestion.text = ""
                            self.txtdesc.text = ""
                            self.hideViewWithAnimation(vw: self.vwnewquestion, img: self.imgBG1)
                            self.GetAllForums()
                            self.GetMyForums()
                         }
             })
         }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // hideKeyboardWhenTappedAround()
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        vwnewquestion.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        txtquestion.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        txtdesc.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lblquestion.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lbldesc.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         txtquestion.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         txtdesc.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        imgBG1.image = UIImage(named: "imgAppBG1")
        
        self.view.layoutIfNeeded()

        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        tblChatView.tableFooterView = UIView()
        tblChatView.mixedSeparatorColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        self.GetAllForums()
        self.GetMyForums()
        vwnewquestion.isHidden = true
        txtquestion.delegate = self
        txtdesc.delegate = self
        self.view.layoutIfNeeded()
    
        
//        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
//                             tapGestureRecognizer2.delegate = self
//                             runlogo.isUserInteractionEnabled = true
//                             runlogo.addGestureRecognizer(tapGestureRecognizer2)
              
              
              
          }
          
          @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
             
              let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
               nextViewController.type = "Adv"
              self.navigationController?.pushViewController(nextViewController, animated: true)
            
              
          }
    
    
    //MARK: - TextField Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    //MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tablearr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllForumCell", for: indexPath) as! AllForumCell
        
        cell.lblQuestion.text = tablearr[indexPath.row]["QuestionName"] as? String ?? "Unknown name"
        cell.lblcreatedby.text = "Created by: \(tablearr[indexPath.row]["FirstName"] as? String ?? "")"
        cell.lblTime.text = "Created on: " + "\(tablearr[indexPath.row]["Created"] as? String ?? "")".toDate().timeAgoSinceDate()
        
  
        cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuestionDetailView") as! QuestionDetailView
        nextViewController.commentData = tablearr[indexPath.row]
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func myforum_click(_ sender: UIButton) {
        if sender.titleLabel?.text == "My Forums" {
             sender.setTitle("All Forums", for: .normal)
             self.tablearr = self.mycommentarr
        }
        else
        {
             sender.setTitle("My Forums", for: .normal)
             self.tablearr = self.allcomentarr
        }
        self.tblChatView.reloadData()
    }
    @IBAction func newforum_click(_ sender: UIButton) {
        
       showViewWithAnimation(vw: vwnewquestion, img: imgBG1)
       
    }
//    func textViewDidChange(_ textView: UITextView)
//{
//    if textView.contentSize.height >= 100
//    {
//        textView.isScrollEnabled = true
//    }
//    else
//        {
//        textView.frame.size.height = textView.contentSize.height
//            textView.isScrollEnabled = false // textView.isScrollEnabled = false for swift 4.0
//    }
//}
    
    @IBAction func save_click(_ sender: UIButton) {
        if txtquestion.text.trimmingString().count > 0
        {
            AddQuestion()
        }
        else
        {
             showAlert(uiview: self, msg: "Please Enter Question", isTwoButton: false)
        }
        
    }
    @IBAction func cancel_click(_ sender: UIButton) {
         self.view.endEditing(true)
        hideViewWithAnimation(vw: vwnewquestion, img: imgBG1)
    }
    @IBAction func refresh_click(_ sender: UIButton) {
        self.GetAllForums()
        self.GetMyForums()
    }
    
}
