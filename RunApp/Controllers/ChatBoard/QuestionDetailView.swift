//
//  QuestionDetailView.swift
//  RunApp
//
//  Created by My Mac on 21/11/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight

class QuestionReplyCell: UITableViewCell {
    
    @IBOutlet weak var lblreply: UILabel!
    @IBOutlet weak var lblcreatedby: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet var imguser: UIImageView!
    
    override func awakeFromNib() {
            lblreply.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
            lblcreatedby.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
            lblTime.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
       }
}
class QuestionDetailView: UIViewController,UITableViewDataSource, UITableViewDelegate
{

    @IBOutlet var vwnewquestion: UIView!
    @IBOutlet var lblquestion: UILabel!
    @IBOutlet var txtquestion: UITextView!
    @IBOutlet var lbldesc: UILabel!
    @IBOutlet var txtdesc: UITextView!
   // @IBOutlet var scrollview: UIScrollView!
    @IBOutlet weak var vwPopupTopComment: UIView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var txtWriteComment: UITextField!
    @IBOutlet weak var tblReply: UITableView!

        var commentData = [String:Any]()
        var tablearr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
              vwnewquestion.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
              txtquestion.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
              txtdesc.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
              lblquestion.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
              lbldesc.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
               txtquestion.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
               txtdesc.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
               txtWriteComment.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
               txtWriteComment.setLeftPaddingPoints(30, strImage: "")
               txtWriteComment.layer.masksToBounds = true
               txtWriteComment.layer.cornerRadius = 20.0
               txtWriteComment.setRadiusBorder(color: UIColor.red)
              
              self.view.layoutIfNeeded()

              vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
              tblReply.tableFooterView = UIView()
              tblReply.mixedSeparatorColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
              self.GetAllReply()
              
        
        txtquestion.text = commentData["QuestionName"] as? String
        txtdesc.text = commentData["QuestionDescription"] as? String
             
              self.view.layoutIfNeeded()
    }
    
    func GetAllReply()
         {
            let parameters = ["QuestionID" : commentData["QuestionID"] ] as! [String : String]
            
             let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                           ] as [String : Any]
             
             callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_REPLY,
                     method: .post,
                     param: parameters ,
                     extraHeader: header ,
                     completionHandler: { (result) in
                         print(result)
                         if result.getBool(key: "status")
                         {
                             let arrDict = result["data"] as? [[String:Any]] ?? []
                           //  let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
                             self.tablearr = arrDict
                            self.tblReply.reloadData()
                         }
             })
         }
       
       func AddReply()
            {
                let parameters = ["QuestionID" : commentData["QuestionID"],"Answer":txtWriteComment.text ] as! [String : String]
                
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                              ] as [String : Any]
                
                callApi(ServiceList.SERVICE_URL+ServiceList.APP_ADD_REPLY,
                        method: .post,
                        param: parameters ,
                        extraHeader: header ,
                        completionHandler: { (result) in
                            print(result)
                            if result.getBool(key: "status")
                            {
                                self.txtWriteComment.text = ""
                               self.GetAllReply()
                            }
                })
            }
    
    func numberOfSections(in tableView: UITableView) -> Int {
          return 1
      }
      
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return tablearr.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionReplyCell", for: indexPath) as! QuestionReplyCell
        
            let imgurl = tablearr[indexPath.row]["UserImage"] as? String ?? ""
            cell.imguser.setRadius(radius: cell.imguser.frame.height/2)
            cell.imguser.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
          
          cell.lblreply.text = tablearr[indexPath.row]["Answer"] as? String ?? "Unknown"
          cell.lblcreatedby.text = "Created by: \(tablearr[indexPath.row]["FirstName"] as? String ?? "")"
          cell.lblTime.text = "Created on: " + "\(tablearr[indexPath.row]["Created"] as? String ?? "")".toDate().timeAgoSinceDate()
          
          cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
          cell.selectionStyle = .none
          return cell
          
      }
    @IBAction func btnBackAction(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
    @IBAction func btnSendPressed(_ sender: UIButton) {
           self.view.endEditing(true)
        if txtWriteComment.text?.trimmingString().count ?? 0 > 0
           {
                 AddReply()
           }
           else
           {
               showAlert(uiview: self, msg: "Please enter comment", isTwoButton: false)
           }
       }
   
}
