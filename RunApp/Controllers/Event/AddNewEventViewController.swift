//
//  AddNewEventViewController.swift
//  RunApp

//
//  Created by My Mac on 08/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
///

import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0
import NightNight
import CoreLocation

class RecentUpcomingTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPm: UILabel!
    @IBOutlet weak var lblYouth: UILabel!
    @IBOutlet weak var btnRsvp: UIButton!
    @IBOutlet var lblspactatorprice: UILabel!
    @IBOutlet var lblplayerprice: UILabel!
    
    @IBOutlet weak var lblLOC: UILabel!
    
    
    
    override func awakeFromNib() {
        
        lblDate.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblYouth.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblspactatorprice.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblplayerprice.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        lblLOC.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
    }
}




class AddNewEventViewController: UIViewController , UIGestureRecognizerDelegate
{
    
       @IBOutlet weak var constTblUpcomingHeight: NSLayoutConstraint!
    
    @IBOutlet var btn_seemore: UIButton!
    @IBOutlet var constliverun: NSLayoutConstraint!
    
    @IBOutlet var txtplayersheight: NSLayoutConstraint!
    
    @IBOutlet var txtteamsheight: NSLayoutConstraint!
    
   
    
    var SpectatorArray = [String]()
    var  IRArray = [String]()
    
    @IBOutlet weak var vwPopup: UIView!
     @IBOutlet weak var imgBlur: UIImageView!
       
    @IBOutlet weak var btnSpectatorOutlet: UIButton!
       
       @IBOutlet weak var btnPayOutlet: UIButton!
       
    
    var lagitude = Double()
    var logitude = Double()
    var locationManager = CLLocationManager()
    var flag = Bool()
    
    
    var RSVPTitle = ""
    var Amt = ""
       var PlayerAmt = ""
       var AmountGet = ""
       var PlayerType = ""
        var IRType = ""
    
     var rsvpList = [[String : Any]]()
    var rsvpListGet1 = [[String : Any]]()
     var EditParam = [String : Any]()
    var EventID1 = ""
    var LocationID1 = ""
    var IsEdit =  ""
    
    
    @IBOutlet var txtTotalteams: UITextField!
    
    @IBOutlet var txttotalplayeras: UITextField!
    
    
    @IBOutlet weak var lbladdnewevent: UILabel!
     @IBOutlet weak var lblPaid: UILabel!
    
    @IBOutlet weak var tblUpcomingView: UITableView!
    @IBOutlet var runlogo: UIImageView!
    
    @IBOutlet weak var btnupcomingrun: UIButton!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var scrlVW: UIScrollView!
    @IBOutlet weak var btnMaxPlayer: UIButton!
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtPlayer: UITextField!
    
    @IBOutlet weak var btnPaid: UIButton!
    @IBOutlet weak var txtFess: UITextField!
    @IBOutlet weak var btnDateTime: UIButton!
    @IBOutlet weak var btnLoc: UIButton!
    @IBOutlet weak var btnAddEvent: UIButton!
    @IBOutlet weak var btnEndDate: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet var btn_IR: UIButton!
    
    
    
    @IBOutlet weak var textTopHeight: NSLayoutConstraint!
    @IBOutlet weak var textHeight: NSLayoutConstraint!
    @IBOutlet weak var playerTopHeight: NSLayoutConstraint!
    
    @IBOutlet weak var playerHeight: NSLayoutConstraint!
    
    var levelOfCompositionList = [[String : Any]]()
    var strSelectedDate , strEndDate , strLOCId : String?
    var LocationDetail = [String : Any]()
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    var LocationID: String?
    
    var FViewCont:HomeView!
  
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           hideViewWithAnimation(vw: vwPopup, img: imgBlur)
       }
    //MARK:- Web Service
    
    func addEvent()
    {
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
    
    let parameters = [
                      "EventFromDate" : strEndDate ?? "" ,
                      "CreationUserID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","clvlid":strLOCId!,"TotalTeam":txtTotalteams.text!,
                      "PlayerPerTeam" :txttotalplayeras.text!,
                       "EvenetName":txtName.text ?? "" ,"LocationID":LocationID ?? "","MaxPlayer":btnMaxPlayer.currentTitle ?? "" ,
                            "EventType":btnPaid.currentTitle ?? "" ,
                            "Amt":txtFess.text ?? "" ,
                            "PlayerAmt":txtPlayer.text! ,
                            "EventDate" : strSelectedDate ?? "",
                            "EventID" : EventID1,
                            "IR" : IRType
                             
                          //"event_to_date" : strEndDate ?? "" ,
                         
        ]
        
        
        
        print(parameters)
        
        callApi(ServiceList.SERVICE_URL+ServiceList.ADD_EVENT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        for vc in (self.navigationController?.viewControllers ?? []) {
                            if vc is HomeView {
                               // self.FViewCont.a = "Yellow"
                               // self.FViewCont.startDate = self.strSelectedDate ?? ""
                                //self.FViewCont.EndDate = self.strEndDate ?? ""
                                StoredData.shared.EventStatus = "Event"
                                _ = self.navigationController?.popToViewController(vc, animated: true)
                                break
                                
                            }
                        }
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))
        })
    }
    
    func getLOCList()
    {
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOC_LIST,
                method: .get,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.levelOfCompositionList = result["data"] as? [[String:Any]] ?? []
                    }
        })
    }
    
    
    @IBAction func btn_seemorepressed(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SeeMoreViewController") as! SeeMoreViewController
        
                nextViewController.LocationID = LocationID
                nextViewController.EventID = EventID1
            nextViewController.rsvpDictList = rsvpListGet1
            self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.txtplayersheight.constant = 0
        self.txtteamsheight.constant = 0
        
        appGetRsvpLiveLocation()
        
        SpectatorArray = ["Spectator","Player"]
        IRArray = ["ON","OFF"]
               // print("LocationID: \(LocationID)")
               
        
               btnSpectatorOutlet.setRadius(radius: 12)
               btnSpectatorOutlet.setRadiusBorder(color: UIColor.gray)
               btnSpectatorOutlet.roundCorners(corners: .allCorners, radius: 2)
               lblPaid.mixedTextColor = MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont)
               vwPopup.setRadius(radius: 10)
              // btnAddEventOutlet.imageView?.changeImageViewImageColor(color: UIColor.gray)
              // app_Event_Status_Api()
               
               btnPayOutlet.setRadius(radius: btnPayOutlet.frame.height/2)
               self.view.layoutIfNeeded()
        
        
        
        
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
           tapGestureRecognizer.delegate = self
           runlogo.isUserInteractionEnabled = true
           runlogo.addGestureRecognizer(tapGestureRecognizer)
        
        
         vwPopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        lbladdnewevent.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtFess.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtPlayer.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         txttotalplayeras.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
         txtTotalteams.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        btnLoc.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnPaid.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnEndDate.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
                btnAddEvent.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
                btnDateTime.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
                btnLoc.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
        btnMaxPlayer.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
          btnupcomingrun.setMixedTitleColor(MixedColor(normal:FontColor.dayfont, night: FontColor.nightfont), forState: .normal)
         btnBack.setMixedImage(MixedImage(normal:"left-arrow-B", night:"left-arrow"), forState: .normal)
       // btnupcomingrun.layer.borderWidth = 0
        
        
        //  lblName.text = LocationDetail["LocationName"]as? String ?? ""
        //  lblAddress.text = LocationDetail["LocationAddress"]as? String ?? ""
        
        
        
        self.view.layoutIfNeeded()
        let userororganization = UserDefaults.standard.getUserDict()["usertype"] as?
                   String ?? ""
                   
                  if userororganization == "User"
                  {
                   
                   btnPaid.isUserInteractionEnabled = false
                   self.btnPaid.setTitle("Free", for: .normal)
                   self.textHeight.constant = 0
                   self.playerHeight.constant = 0
                   self.textTopHeight.constant = 0
                   self.playerTopHeight.constant = 0
                              
                                  
                      
               }
//        imgBG.setGredient()
        
        for view in scrlVW.subviews as [UIView] {
            if let txt = view as? UITextField {
                txt.setLeftPaddingPoints(15, strImage: "")
                txt.setRadius(radius: 10)
                txt.setRadiusBorder(color: UIColor.gray)
            }
        }
        
        for view in scrlVW.subviews as [UIView] {
            if let btn = view as? UIButton {
                if btn != btnAddEvent || btn != btnupcomingrun
                {
                    btn.setRadius(radius: 10)
                    btn.setRadiusBorder(color: UIColor.gray)
                }
            }
        }
        
        vwPopupTop.setRadius(radius: 10)
        btnAddEvent.setRadius(radius: btnAddEvent.frame.height/2)
        btnupcomingrun.setRadius(radius: btnupcomingrun.frame.height/2)
        
        let gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        gestureRecognizer.delegate = self
        gestureRecognizer.direction = .right
        self.view.addGestureRecognizer(gestureRecognizer)
        
        getLOCList()
        print(EditParam)
        if IsEdit == "true"
        {
            LocationID =   EditParam["LocationID"] as? String ?? ""
            txtName.text = EditParam["EvenetName"] as? String ?? ""
            let values = EditParam["MaxPlayer"] as? String ?? ""
            self.btnMaxPlayer.setTitle("\(values)", for: .normal)
            
            let strstartdate = EditParam["EventDate"] as? String ?? ""
            let enddate = EditParam["EventFromDate"] as? String ?? ""
            
            
           let dateFormatter1 = DateFormatter()
          dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm:ss"
            
            if strstartdate != "" {
//                let date: Date? = dateFormatter1.date(from: "\(strstartdate as! String)")!
//                btnDateTime.setTitle(dateFormatter1.string(from: date!), for: .normal)
                 let strstartdate = self.EditParam["EventDate"] as? String ?? "" != "0000-00-00 00:00:00" ? self.setDateWithFormat(strDate:self.EditParam["EventDate"] as? String ?? "" ?? "") : ""
                  btnDateTime.setTitle(strstartdate, for: .normal)
                strSelectedDate = self.EditParam["EventDate"] as? String ?? ""
                
            }
            if enddate != ""
            {
              let enddate = self.EditParam["EventFromDate"] as? String ?? "" != "0000-00-00 00:00:00" ? self.setDateWithFormat(strDate:self.EditParam["EventFromDate"] as? String ?? "" ?? "") : ""
                btnEndDate.setTitle(enddate, for: .normal)
                strEndDate = self.EditParam["EventFromDate"] as? String ?? ""
            }
           let IR =  EditParam["IR"] as? String ?? ""
            if IR != ""
            {
                btn_IR.setTitle(IR, for: .normal)
                self.IRType = IR
                 if IR == "ON"
                 {
                    
                    self.txtplayersheight.constant  = 50
                    self.txtteamsheight.constant = 50
                    self.txtTotalteams.text = EditParam["TotalTeam"] as? String ?? ""
                     self.txttotalplayeras.text = EditParam["PlayerPerTeam"] as? String ?? ""
                }
                else
                 {
                    self.txtplayersheight.constant  = 0
                    self.txtteamsheight.constant = 0
                }
            }
            
            let Loc = EditParam["CLVLTitle"] as? String ?? ""
            self.btnLoc.setTitle("\(Loc)", for: .normal)
            self.strLOCId = EditParam["CLVLID"] as? String ?? ""
            
            
            EventID1 = EditParam["EventID"] as? String ?? ""
            
            let userororganization = UserDefaults.standard.getUserDict()["usertype"] as?
             String ?? ""
             
            if userororganization != "User"
            {
                txtFess.text =  self.EditParam["Amt"] as? String ?? ""
                txtPlayer.text = self.EditParam["PlayerAmt"] as? String ?? ""
                txtFess.isUserInteractionEnabled = false
                txtPlayer.isUserInteractionEnabled = false
            }
            btnAddEvent.setTitle("Update Event", for: .normal)
        }
        
        self.view.layoutIfNeeded()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appGetRsvpLiveLocation()
    }
    
    @IBAction func btn_upcomingRun(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SeeMoreViewController") as! SeeMoreViewController
    
            nextViewController.LocationID = LocationID
            nextViewController.EventID = EventID1
        nextViewController.rsvpDictList = rsvpListGet1
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
          let IR = rsvpListGet1[indexPath.row]["IR"] as? String ?? ""
              
              if IR == "ON"
              {
                  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "IREventDetailViewController") as! IREventDetailViewController
                               nextViewController.Event_ID = rsvpListGet1[indexPath.row]["EventID"] as? String ?? ""
                               nextViewController.Event_Name = rsvpListGet1[indexPath.row]["EvenetName"] as? String ?? ""
                         
                               self.navigationController?.pushViewController(nextViewController, animated: true)
                  
              }
              else
              {
          
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailView") as! EventDetailView
                    nextViewController.Event_ID = rsvpListGet1[indexPath.row]["EventID"] as? String ?? ""
                    nextViewController.Event_Name = rsvpListGet1[indexPath.row]["EvenetName"] as? String ?? ""
              
                    self.navigationController?.pushViewController(nextViewController, animated: true)
              }
    }
    
    func appGetRsvpLiveLocation()
    {
        let parameters = ["LocationID": LocationID ?? "","EventID":EventID1,"PlayerID": UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""
                      ] as [String : Any]

        callApi(ServiceList.SERVICE_URL+ServiceList.APP_GET_RSVP_LIVE_LOCATION,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        //let data = result["data"] as? [[String:Any]] ?? []
                        
                    let GetData:NSDictionary = result["data"] as! NSDictionary
                     self.rsvpListGet1 = GetData.value(forKey: "event") as? [[String:Any]] ?? []
                        print("rsvpListGet:\(self.rsvpListGet1)")
                        let jsonarr:NSArray = GetData.value(forKey: "event") as! NSArray
                        if jsonarr.count > 0
                        {
                             let dict1:NSDictionary = jsonarr.lastObject as! NSDictionary
                            self.rsvpList = [[String : Any]]()
                            self.rsvpList.append(dict1 as! [String : Any])
                            
                        }
                       
                     //   let dict2:NSDictionary = jsonarr.object(at: jsonarr.count-2) as! NSDictionary
                       
                       // self.rsvpList.append(dict2 as! [String : Any])
                       
                        
                       // self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
                        // self.parseLocation(LocationDict: self.dictLocation)
                        if self.rsvpList.count == 0
                        {
                            self.constTblUpcomingHeight.constant = 0
                           // self.constliverun.constant = 0
                            self.btn_seemore.isHidden = true
                            
                        }
                        
                    }
                    else
                    {
                        showToast(uiview: self, msg: result.getString(key: "message"))
                    }
                    
                    DispatchQueue.main.async {
                        self.tblUpcomingView.reloadData()
                      //  self.constTblUpcomingHeight.constant = self.tblUpcomingView.contentSize.height
                    }
        })
    }

    
    func parseLocation(LocationDict : [String:Any]) {
        
        self.lblName.text = LocationDict["LocationName"] as? String ?? ""
        self.lblAddress.text = LocationDict["LocationAddress"] as? String ?? ""
        //self.EventID = LocationDict["EventID"] as? Int ?? 0
        let cretedOn = LocationDict["RegistrationDate"] as? String ?? ""
        let Since = cretedOn.toDate3()
        
        
        //                        let imgPin = placeList[i]["product_image"] as? String ?? ""
        //                            let Distance = placeList[i]["product_image"] as? String ?? ""
       // self.lblLoc.text = "\(LocationDict["LocMode"] as? String ?? "")"
        
        
        
        print("LocationDict: \(LocationDict)")
    }
    //MARK:- Button Action
    
    @IBAction func btnAddClicked(_ sender: UIButton) {
        validation()
    }
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "MMM dd , yyyy h:mm a"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        return Date24

    }
    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
            
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
              nextViewController.type = "Adv"
             self.navigationController?.pushViewController(nextViewController, animated: true)
           
             
         }
    
    @IBAction func btn_IR(_ sender: Any) {
        self.view.endEditing(true)
               
        
        if IRArray.count != 0
                {
                    ActionSheetStringPicker.show(withTitle: "Select IR", rows: IRArray, initialSelection: 0, doneBlock: {
                        picker, indexes, values in
                        
                        self.btn_IR.setTitle("\(values!)", for: .normal)
                        
                        if self.btn_IR.currentTitle == "ON" {
                          
                          
                            self.IRType = "ON"
                            self.txtteamsheight.constant = 50
                            self.txtplayersheight.constant = 50
                        }
                            
                        else if self.btn_IR.currentTitle == "OFF" {
                            
                            
                            self.IRType = "OFF"
                            self.txtteamsheight.constant = 0
                            self.txtplayersheight.constant = 0
                            
                        }
                        
                       
                        return
                    }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
                }
        
    }
    
    
    @IBAction func btnMaxPlayerAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var arrPlayer = [Int]()
        for i in 1..<100
        {
            arrPlayer.append(i)
        }
        
        if arrPlayer.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Maximum No of Player", rows: arrPlayer as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnMaxPlayer.setTitle("\(values!)", for: .normal)
                
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnPaidAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
       
       
        ActionSheetStringPicker.show(withTitle: "Select Type", rows: ["Paid" , "Free"] , initialSelection: 0, doneBlock: {
            
            picker, indexes, values in
            self.btnPaid.setTitle("\(values!)", for: .normal)
            if self.btnPaid.currentTitle == "Free" {
                self.textHeight.constant = 0
                self.playerHeight.constant = 0
                self.textTopHeight.constant = 0
                self.playerTopHeight.constant = 0
                
            }
                
            else {
                
                self.textHeight.constant = 50
                self.playerHeight.constant = 50
                self.textTopHeight.constant = 15
                self.playerTopHeight.constant = 15
                
            }
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    
    
 
    
    
   
    
    @IBAction func btnDateTimeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "Select Start Date:", datePickerMode: .dateAndTime, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd , yyyy h:mm a"
            
            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
//            let dateString = String(describing: value!)
//            let date: Date = dateFormatterGet.date(from:dateString)!
            print(dateFormatter.string(from: date!))
            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm"
        //    dateFormatter1.timeZone = TimeZone(identifier: "UTC")
            
            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
            self.strSelectedDate = (dateFormatter1.string(from: date1!))
            print("strSelectedDate: \(String(describing: self.strSelectedDate))")
            
            return
        }, cancel: { ActionStringCancelBlock in return },
           origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func btnEndDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let datePicker = ActionSheetDatePicker(title: "Select End Date:", datePickerMode: .dateAndTime, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            let dateFormatter = DateFormatter()
           // dateFormatter.dateFormat = "MMM dd , yyyy HH:mm"
             dateFormatter.dateFormat = "MMM dd , yyyy h:mm a"
            
            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
            print(dateFormatter.string(from: date!))
            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm"
            
            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
            self.strEndDate = (dateFormatter1.string(from: date1!))
            
            return
        }, cancel: { ActionStringCancelBlock in return },
           origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func btnLOCAction(_ sender: UIButton) {
        self.view.endEditing(true)
//        if levelOfCompositionList.count != 0
//        {
//            ActionSheetStringPicker.show(withTitle: "Select LOC", rows: levelOfCompositionList.map({ $0["CLVLDesc"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
//                picker, indexes, values in
//                self.btnLoc.setTitle("\(values!)", for: .normal)
//                self.strLOCId = self.levelOfCompositionList[indexes]["CLVLID"] as? String ?? ""
//
//                return
//            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
//        }
        
        if levelOfCompositionList.count != 0
               {
                   ActionSheetStringPicker.show(withTitle: "Select LOC", rows: levelOfCompositionList.map({ $0["CLVLTitle"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                       picker, indexes, values in
                       self.btnLoc.setTitle("\(values!)", for: .normal)
                       self.strLOCId = self.levelOfCompositionList[indexes]["CLVLID"] as? String ?? ""
                       
                       return
                   }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
               }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSpectatorAction(_ sender: UIButton) {
           self.view.endEditing(true)
                  
           
           if SpectatorArray.count != 0
                   {
                       ActionSheetStringPicker.show(withTitle: " Please Select One", rows: SpectatorArray, initialSelection: 0, doneBlock: {
                           picker, indexes, values in
                           
                           self.btnSpectatorOutlet.setTitle("\(values!)", for: .normal)
                           
                           if self.btnSpectatorOutlet.currentTitle == "Spectator" {
                               self.AmountGet = self.Amt
                               self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
                               self.PlayerType = "Spectator"
                           }
                               
                           else if self.btnSpectatorOutlet.currentTitle == "Player" {
                               self.AmountGet = self.PlayerAmt
                               self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
                               self.PlayerType = "Player"
                           }
                           
                           else {
//                               self.AmountGet = self.PlayerAmt
//                               self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
//                               self.PlayerType = "Both"
                            showToast(uiview: self, msg: "Please Select One")
                            
                           }
                           return
                       }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
                   }
       }
       
    
    @IBAction func btn_irinfo(_ sender: Any) {
        let alertController = UIAlertController(title: "Intelligent Roster System", message: "Using the Intelligent Roster System will allow the app to generate suggested teams for your RUN based on limited data gathered on the players who sign up to play.", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            self.dismiss(animated: true, completion: nil)
            
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    
    @IBAction func btnPayActionClicked(_ sender: UIButton) {
             
         if self.btnSpectatorOutlet.currentTitle != "Please Select One"
         {
             
                 if RSVPTitle == "RSVP"
                 {
                     
                 if self.rsvpList[sender.tag]["EventType"] as! String == "Paid"
             {
                 let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
                 nextViewController.Amount = AmountGet
                 nextViewController.EventID = EventID1
                 nextViewController.PlayerType = PlayerType
                 nextViewController.LocationID = LocationID
                 nextViewController.rsvpDictList1 = rsvpList[sender.tag]

                 self.navigationController?.pushViewController(nextViewController, animated: true)
                 
                 
             }
             else
             {
                 //self.btnPayOutlet.setTitle("Join", for: .normal)
                 appSpectatorJoin()
                 
             }
             }
                

             else if RSVPTitle == "EventCheckIn" {
                 if self.rsvpList[sender.tag]["EventType"] as! String == "Paid"
                     {
                     if self.rsvpList[sender.tag]["IsPayment"] as! Int == 0
                     {
                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PayNowView") as! PayNowView
                         nextViewController.Amount = AmountGet
                         nextViewController.EventID = EventID1
                         nextViewController.PlayerType = PlayerType
                         nextViewController.LocationID = LocationID
                         nextViewController.rsvpDictList1 = rsvpList[sender.tag]
                         
                         self.navigationController?.pushViewController(nextViewController, animated: true)
                         }
                     else{
                          appEventCheckin()
                         
                         }
                     }
                     else
                     {
                         //self.btnPayOutlet.setTitle("Join", for: .normal)
                         appEventCheckin()
                     }
             }
             
        }
        else
         {
            showToast(uiview: self, msg: "Please Select One")
        }
             
             
             
                 }
    
    
       @objc func btnRsvpAction(button: UIButton) {
              
              btnPayOutlet.tag = button.tag
              
               RSVPTitle =   button.currentTitle!
               
                
              
              self.Amt = rsvpList[button.tag]["Amt"] as? String ?? ""
              self.PlayerAmt = rsvpList[button.tag]["PlayerAmt"] as? String ?? ""
              self.EventID1 = rsvpList[button.tag]["EventID"] as? String ?? ""
              self.PlayerType = "Spectator"
              self.AmountGet = self.Amt
              self.lblPaid.text = "Amount to be paid : $\(self.AmountGet)"
              
              let BookingStatus = rsvpList[button.tag]["BookingStatus"] as? String ?? ""
              
              
              if self.rsvpList[button.tag]["EventType"] as! String == "Paid"
              {
                 self.btnPayOutlet.setTitle("PAY NOW", for: .normal)
              }
              else
              {
                  self.btnPayOutlet.setTitle("JOIN", for: .normal)
              }
              
              if BookingStatus == "0" {
                  showViewWithAnimation(vw: vwPopup, img: imgBlur)
              }
                  
              else {
                  showAlert(uiview: self, msg: "Booking Closed", isTwoButton: false)
              }
              
                  let RSVPD = rsvpList[button.tag]["RSVPD"] as? String ?? ""
              
              
              if RSVPTitle == "Cancel"
                            {
                                appCancelSpectator()
                            }
                     
              

          }
       
     
            

             func appSpectatorJoin()
               {
                   if EventID1 != "" {
                       let parameters = ["Location_Id": LocationID ?? "" ,
                                         "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                         "Event_ID": EventID1, "PlayerType" : PlayerType
                                         
                           ] as [String : Any]
                       
                       let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                     "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                     ] as [String : Any]
                       
                       callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerJoin,
                               method: .post,
                               param: parameters ,
                               extraHeader: header ,
                               completionHandler: { (result) in
                                   print(result)
                                   self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                                   if result.getBool(key: "status")
                                   {
                                       
                                      
                                   }
                                   else
                                   {
                                      // showToast(uiview: self, msg: result.getString(key: "message"))
                                   }
                                    self.appGetRsvpLiveLocation()
                       })
                   }
                       
                   else {
                       print("Event id is Empty")
                   }
                   
               }
       
             
             func appEventCheckin()
                 {
                     if EventID1 != "" {
                         let parameters = ["Location_Id": LocationID ?? "" ,
                                           "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                           "Event_ID": EventID1, "PlayerType" : PlayerType,"Lat":lagitude,"Long":logitude
                                           
                             ] as [String : Any]
                         
                         let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                       "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                       ] as [String : Any]
                         
                         callApi(ServiceList.SERVICE_URL+ServiceList.AppSpectororPlayerEventCheckIn,
                                 method: .post,
                                 param: parameters ,
                                 extraHeader: header ,
                                 completionHandler: { (result) in
                                     print(result)
                                     self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                     if result.getBool(key: "status")
                                     {
                                         
                                        
                                     }
                                     else
                                     {
                                        // showToast(uiview: self, msg: result.getString(key: "message"))
                                     }
                                      self.appGetRsvpLiveLocation()
                         })
                     }
                         
                     else {
                         print("Event id is Empty")
                     }
                     
                 }
             
             func appCancelSpectator()
              {
                  if EventID1 != "" {
                      let parameters = ["Location_Id": LocationID ?? "" ,
                                        "Player_ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "" ,
                                        "Event_ID": EventID1
                          ] as [String : Any]
                      
                      let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                    "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                    ] as [String : Any]
                      
                      callApi(ServiceList.SERVICE_URL+ServiceList.APP_SPECTATOR_CANCEL,
                              method: .post,
                              param: parameters ,
                              extraHeader: header ,
                              completionHandler: { (result) in
                                  print(result)
                                  self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                                  showToast(uiview: self, msg: result.getString(key: "message"))
                                  if result.getBool(key: "status")
                                  {
                                      
                                   //   self.btnPayOutlet.setTitle("PAY NOW", for: .normal)
                                  }
                                  else
                                  {
                                     // showToast(uiview: self, msg: result.getString(key: "message"))
                                  }
                                   self.appGetRsvpLiveLocation()
                                 
                      })
                  }
                      
                  else {
                      print("Event id is Empty")
                  }
                  
              }
          
    
    
             
    //MARK:- Other method
    func validation()
    {
        view.endEditing(true)
        
        if txtName.text == "" && btnMaxPlayer.currentTitle == "Max Player" && txtFess.text == "" && strSelectedDate == "" && strEndDate == "" && strLOCId == ""
        {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
        else if txtName.text == ""
        {
            showAlert(uiview: self, msg: "Please enter name", isTwoButton: false)
        }
        else if btnMaxPlayer.currentTitle == "Max Player"
        {
            showAlert(uiview: self, msg: "Please select maximum no of player", isTwoButton: false)
        }
        else
        {
            if btnPaid.currentTitle ?? "" == "Paid"
            {
                if txtFess.text == ""
                {
                    showAlert(uiview: self, msg: "Please enter Spectator fees", isTwoButton: false)
                }
                    
                else if txtPlayer.text == ""
                {
                    showAlert(uiview: self, msg: "Please enter Player fees", isTwoButton: false)
                }
                    
                else
                {
                    innerValidation()
                }
            }
            else
            {
                innerValidation()
            }
        }
        
    }
    
    func innerValidation()
    {
        if strSelectedDate == nil
        {
            showAlert(uiview: self, msg: "Please select start date", isTwoButton: false)
        }
        else if strEndDate == nil
        {
            showAlert(uiview: self, msg: "Please select end date", isTwoButton: false)
        }
        else if strLOCId == nil
        {
            showAlert(uiview: self, msg: "Please select LOC", isTwoButton: false)
        }
        else
        {
            addEvent()
        }
    }
    
    @objc func handleSwipe(_ gestureRecognizer: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension AddNewEventViewController: UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return rsvpList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "upcomingRunCell", for: indexPath) as! RecentUpcomingTableViewCell
            self.view.layoutIfNeeded()
            //Amt
            cell.btnRsvp.layer.cornerRadius = 5.0
            cell.btnRsvp.layer.masksToBounds = true
            cell.btnRsvp.tag = indexPath.row
//            self.EventID = rsvpList[indexPath.row]["EventID"] as? String ?? ""
//            self.Amt = rsvpList[indexPath.row]["Amt"] as? String ?? ""
//            self.PlayerAmt = rsvpList[indexPath.row]["PlayerAmt"] as? String ?? ""
            //PlayerAmt
        cell.lblYouth.text = (rsvpList[indexPath.row]["EvenetName"] as? String ?? "")
        cell.lblLOC.text =  "LOC:" + (rsvpList[indexPath.row]["CLVLTitle"] as? String ?? "")
        
        cell.lblspactatorprice.text = "Spectator Price: " + String(rsvpList[indexPath.row]["Amt"] as? String ?? "")
        cell.lblplayerprice.text = "Player Price: " + String(rsvpList[indexPath.row]["PlayerAmt"] as? String ?? "")
        
            let cretedOn = rsvpList[indexPath.row]["EventDate"] as? String ?? ""
            
            print("EventID: \(self.EventID1)")
            
            if cretedOn != "" {
                 cell.lblDate.text = cretedOn.toDate5()
            }
            
            if UserDefaults.standard.getUserDict()["usertype"] as? String ?? "" != "Organization" {
                 
                cell.btnRsvp.isHidden = false
             
                cell.btnRsvp.addTarget(self, action: #selector(btnRsvpAction), for: .touchUpInside)
               
                
                let RSVPD = rsvpList[indexPath.row]["RSVPD"] as? String ?? ""
                
                if RSVPD == "1"
                {
//                     cell.btnRsvp.setTitle("RSVP'd", for: .normal)
//                    cell.btnRsvp.isEnabled = false
                    
                     cell.btnRsvp.setTitle("Cancel", for: .normal)
                     cell.btnRsvp.isEnabled = true
                     
                }
                else
                {
                    cell.btnRsvp.setTitle("RSVP", for: .normal)
                    cell.btnRsvp.isEnabled = true
                  
                }
                
                let IsEventOneHour = rsvpList[indexPath.row]["IsEventOneHour"] as? Int ?? 3
                    
                    if IsEventOneHour == 1
                {
                    cell.btnRsvp.setTitle("EventCheckIn", for: .normal)
                   
                    //btncheckmark.isHidden = true
                   // btnCheckInOutlet.isHidden = true
                    
                }
                let IsEventCheckedIn = rsvpList[indexPath.row]["IsCheckedIn"] as? Int ?? 3
                                  
                                  if IsEventCheckedIn == 1
                              {
                                  cell.btnRsvp.setTitle("CheckedIn", for: .normal)
                                  cell.btnRsvp.isEnabled = false
                                
                              }
                
                
            }
                
            else
            {
                cell.btnRsvp.isHidden = true
            }
        
        
        
        cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night: FontColor.dayfont)
        
        
        
        constTblUpcomingHeight.constant = tblUpcomingView.contentSize.height + 30
            self.view.layoutIfNeeded()
            return cell
    }
    
   
    
    
}
