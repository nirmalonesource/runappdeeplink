//
//  IREventDetailViewController.swift
//  RunApp
//
//  Created by My Mac on 08/02/21.
//  Copyright © 2021 My Mac. All rights reserved.
//

import UIKit
import NightNight

class IRTableViewCell : UITableViewCell {
    
    
    
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//           return 3
//       }
//
//       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IRCollectionViewCell", for: indexPath) as! IRCollectionViewCell
//        cell.contentView.mixedBackgroundColor =  MixedColor(normal:FontColor.nightfont, night:0x000000)
//        cell.lblName.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
//           return cell
//
//       }
    
    
@IBOutlet var collIRDetail: UICollectionView!
    
    override func awakeFromNib() {
//        collIRDetail.delegate = self
//        collIRDetail.dataSource = self
//         collIRDetail.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
//        collIRDetail.reloadData()
      }
    
}
class IRCollectionViewCell : UICollectionViewCell {
   
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgImage: UIImageView!
    
    
    
    
    override func awakeFromNib() {
         lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
       
        
        
        //imgImage.mixedTextColor =  MixedColor(normal: 0xffffff, night:0x000000 )
    }
}



class IREventDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    
    @IBOutlet weak var imgBlur: UIImageView!
          @IBOutlet weak var vwPopup: UIView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     
        return (eventList[collectionView.tag] as! NSArray).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                 
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IRCollectionViewCell", for: indexPath) as! IRCollectionViewCell
        //         collIRDetail.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
               cell.contentView.mixedBackgroundColor =  MixedColor(normal:FontColor.nightfont, night:0x000000)
               cell.lblName.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:0x000000)
        
        let dic:NSArray = eventList.object(at: collectionView.tag) as! NSArray
        let dic1: NSDictionary = dic[indexPath.row] as? NSDictionary ?? [:]
        
        
        cell.lblName.text = dic1.value(forKey:"FirstName") as? String
               
        let imgURL = (dic1.value(forKey: "UserProfile") as? String ?? "")
        
        cell.imgImage.layer.cornerRadius = 20
        cell.imgImage.layer.borderColor = UIColor.white.cgColor
        cell.imgImage.layer.borderWidth = 2
        
        cell.imgImage.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                              completed: nil)
               
             
        
                  return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dic:NSArray = eventList.object(at: collectionView.tag) as! NSArray
        let dic1: NSDictionary = dic[indexPath.row] as? NSDictionary ?? [:]
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
        nextViewController.playerListDetail = dic1 as! [String : Any]
         // nextViewController.isfromsearch = true;
          self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
       let yourWidth = collectionView.bounds.width/5.0
       

       return CGSize(width: yourWidth, height: 96)
   }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    var eventList = NSArray()
    var data = [[String : Any]]()
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var scrlVW: UIScrollView!
    
    @IBOutlet var tblIRDetail: UITableView!
    
    
    
    @IBOutlet var totalteams: UITextField!
    @IBOutlet var txtnoofplayers: UITextField!
    
    var Event_ID = ""
       var Event_Name = ""
    
    
    @IBOutlet var lblname: UILabel!
    
    
    
    
    @IBAction func btn_recreate(_ sender: Any) {
          showViewWithAnimation(vw: vwPopup, img: imgBlur)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "IRTableViewCell", for: indexPath) as! IRTableViewCell
         cell.contentView.mixedBackgroundColor =  MixedColor(normal:FontColor.nightfont, night:0x000000)
        cell.collIRDetail.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        cell.collIRDetail.tag = indexPath.row
        cell.collIRDetail.reloadData()
        
        return cell
        
    }
    


    
    
    override func viewDidLoad() {
        super.viewDidLoad()

            vwPopup.setRadius(radius: 10)
            vwPopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
        App_Player_Wise_Event_DetailsIR()
         tblIRDetail.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
         
        vwPopupTop.setRadius(radius: 10)
        txtnoofplayers.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        totalteams.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtnoofplayers.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        totalteams.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
         imgBG.mixedImage = MixedImage(normal: UIImage(named: "imgAppBG1-W") ?? UIImage(), night: UIImage(named: "imgAppBG1") ?? UIImage())
        
    }
    

    @IBAction func btnBackAction(_ sender: UIButton) {
    

        self.navigationController?.popViewController(animated: true)
    
  

}
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
              hideViewWithAnimation(vw: vwPopup, img: imgBlur)
          }
    
    func App_Player_Wise_Event_DetailsIR()
       {
        print(Event_ID)
           let parameters = ["EventID" : Event_ID
               
               ] as [String : Any]
           
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]
           
           self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_WISE_EVENT_DETAILSIR,
                        method: .post,
                        param: parameters,
                        extraHeader: header) { (result) in
                           
                           print(result)
                           
                           if result.getBool(key: "status")
                           {
                            
                            self.data = result["data"] as? [[String : Any]] ?? []
                            
                            if self.data.count > 0
                            {
                            self.eventList = self.data[0]["Team"] as? NSArray ?? []
                            
                            
                            self.lblname.text =  self.data[0]["EventName"] as? String
                            
                            print(self.data[0]["Team"])
                            print(self.eventList)
                                
                            }
                            else
                            {
                                showToast(uiview: self, msg:"Team Not available")
                            }
//                               self.player = self.eventList["player"] as! NSArray
//                               self.spectator = self.eventList["spectator"] as! NSArray
                               
                               
                           }
                           DispatchQueue.main.async {
                           
                            self.tblIRDetail.reloadData()
                             
                            //    self.tblPlayerJoined.reloadData()
                            //  self.tblSpectatorJoined.reloadData()
                            
                           }
                           
                          
                          // showToast(uiview: self, msg: result.getString(key: "message"))
           }
       }
    
    @IBAction func btn_updateTeam(_ sender: Any) {
      if  totalteams.text == ""
      {
          showToast(uiview: self, msg: "Please Enter Total Teams")
        }
      else if txtnoofplayers.text == ""
      {
         showToast(uiview: self, msg: "Please Enter Total players in team")
        }
        else
      {
        updateEvent()
        }
    }
    
    
       func updateEvent()
          {
              
              let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                            "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                            ] as [String : Any]
          
          let parameters = [
                           
            "EventID" : Event_ID,
            "NoOfTeam" : totalteams.text!,
            "PlayerPerTeam" : txtnoofplayers.text!
            
                                //"event_to_date" : strEndDate ?? "" ,
                               
              ]
              
              
              
              print(parameters)
              
              callApi(ServiceList.SERVICE_URL+ServiceList.UPdate_IR,
                      method: .post,
                      param: parameters ,
                      extraHeader: header ,
                      completionHandler: { (result) in
                          print(result)
                          if result.getBool(key: "status")
                          {
                              self.data = result["data"] as? [[String : Any]] ?? []
                                                        
                                                        if self.data.count > 0
                                                        {
                                                        self.eventList = self.data[0]["Team"] as? NSArray ?? []
                                                        
                                                        
                                                        self.lblname.text =  self.data[0]["EventName"] as? String
                                                        
                                                        print(self.data[0]["Team"])
                                                        print(self.eventList)
                                                            
                                                        }
                                                        else
                                                        {
                                                            showToast(uiview: self, msg:"Team Not available")
                                                        }
                            //                               self.player = self.eventList["player"] as! NSArray
                            //                               self.spectator = self.eventList["spectator"] as! NSArray
                                                           
                                                           
                                                       }
                                                       DispatchQueue.main.async {
                                                       
                                                        self.tblIRDetail.reloadData()
                                                        self.hideViewWithAnimation(vw: self.vwPopup, img: self.imgBlur)
                                                        //    self.tblPlayerJoined.reloadData()
                                                        //  self.tblSpectatorJoined.reloadData()
                                                        
                                                       }
                                                       
                                                      
                                                      // showToast(uiview: self, msg: result.getString(key: "message"))
                                       })
          
    }
}
