//
//  LeaderBoardViewController.swift
//  RunApp
//
//  Created by My Mac on 20/01/21.
//  Copyright © 2021 My Mac. All rights reserved.
//

import UIKit
import NightNight


class Leaderboard : UITableViewCell {
    
    @IBOutlet var lblrnk: UILabel!
    @IBOutlet weak var imguser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblRealname : UILabel!
    @IBOutlet weak var lblscore : UILabel!
}

class LeaderBoardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    @IBOutlet var imgRunlogo: UIImageView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLeaderboardList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "Leaderboard", for: indexPath) as! Leaderboard
//
        
        let userid = arrLeaderboardList[indexPath.row]["id"] as? String
             
            
         if userid ==  UserDefaults.standard.getUserDict()["id"] as? String ?? ""
        
         {
            cell.lblUsername.textColor = UIColor.yellow
            cell.lblRealname.textColor = UIColor.yellow
            cell.lblscore.textColor =  UIColor.yellow
            cell.lblrnk.textColor = UIColor.yellow
            lblrank.textColor = UIColor.yellow
            lblrank.text = "#" + "\(indexPath.row + 1)"
            
        }
        else
         {
            cell.lblUsername.textColor = UIColor.white
            cell.lblRealname.textColor = UIColor.gray
            cell.lblscore.textColor =  UIColor.green
            cell.lblrnk.textColor = UIColor.green
             //lblrank.text = "#" + "--"
            
        }
        
        
               cell.imguser.setRadius(radius: cell.imguser.frame.height/2)
               cell.imguser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
             

        
        
        
        
        
               cell.lblUsername.text = arrLeaderboardList[indexPath.row]["username"] as? String
               cell.lblRealname.text = "\(arrLeaderboardList[indexPath.row]["FirstName"] as? String ?? "")"

               cell.lblscore.text = "\(arrLeaderboardList[indexPath.row]["TotalPoints"] as? String ?? "")"
        
        cell.lblrnk.text = "\(indexPath.row + 1 )"
        
        
               let imgURL = ServiceList.IMAGE_URL + "\(arrLeaderboardList[indexPath.row]["UserProfile"] as? String ?? "")"
        cell.imguser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                                   completed: nil)


               
         cell.lblUsername.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
         cell.lblrnk.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
         cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
               
               cell.selectionStyle = .none
               return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let usertype = arrLeaderboardList[indexPath.row]["usertype"] as? String

                                              if usertype == "Organization"
                                              {
                                                  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                                nextViewController.playerOrglResult = arrLeaderboardList[indexPath.row]
                                                     nextViewController.isfromsearch = true;
                                                 self.navigationController?.pushViewController(nextViewController, animated: true)


                                              }
                                              else
                                              {

                                                  let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                                nextViewController.playerListDetail = arrLeaderboardList[indexPath.row]
                                                  nextViewController.isfromsearch = true;
                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                              }

        
    }
    
    @IBOutlet var lbltotal: UILabel!
    @IBOutlet var lblweek: UILabel!
    @IBOutlet var lblmonth: UILabel!
    
    @IBOutlet var lblrank: UILabel!
    
    
    @IBOutlet weak var imgBG: UIImageView!
       @IBOutlet weak var vwPopupTop: UIView!
       @IBOutlet weak var tblLeaderBord: UITableView!
       var arrLeaderboardList = [[String : Any]]()
        var totalPoint = ""
        
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
                                tapGestureRecognizer2.delegate = self
                                imgRunlogo.isUserInteractionEnabled = true
                                imgRunlogo.addGestureRecognizer(tapGestureRecognizer2)
                 
        
        
         vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
                
        //        imgBG.setGredient()
                vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
                tblLeaderBord.tableFooterView = UIView()
               
       
        GetFeedback(FedBackType: "all")
    }
    
    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
               
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
                 nextViewController.type = "Adv"
                self.navigationController?.pushViewController(nextViewController, animated: true)
              
                
            }
    @IBAction func btn_total(_ sender: Any) {
        
        lbltotal.isHidden =  false
        lblweek.isHidden = true
        lblmonth.isHidden = true
          GetFeedback(FedBackType: "year")
        
    }
    
    @IBAction func btn_week(_ sender: Any) {
        lbltotal.isHidden =  true
        lblweek.isHidden = false
        lblmonth.isHidden = true
        GetFeedback(FedBackType: "week")
    }
    
    @IBAction func btn_month(_ sender: Any) {
        lbltotal.isHidden =  true
        lblweek.isHidden = true
        lblmonth.isHidden = false
        GetFeedback(FedBackType: "month")
    }
    
    
    
    @IBAction func btn_back(_ sender: Any) {
      
//        let  UserType = UserDefaults.standard.getUserDict()["usertype"] as? String ?? ""
//                       if UserType == "Organization" {
//
//                         print("Organization Login")
//                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
//                                self.navigationController?.pushViewController(nextViewController, animated: true)
//
//                       }
//                       else
//                       {
//
//                         print("Player Login")
//                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
//                                self.navigationController?.pushViewController(nextViewController, animated: true)
//
//                       }
        
            self.navigationController?.popViewController(animated: true)
        
    }
    
       func GetFeedback(FedBackType : String)
        {
            let parameters = ["type" : FedBackType
                              ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ]
   
            callApi(ServiceList.SERVICE_URL+ServiceList.FeddBack,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)

                        if result.getBool(key: "status")
                        {
                            
                            self.arrLeaderboardList = result["data"] as? [[String:Any]] ?? []
                            
                          //  showToast(uiview: self, msg: result.getString(key: "message"))

                            self.tblLeaderBord.reloadData()
                             
                        }
                        showToast(uiview: self, msg: result.getString(key: "message"))

            })
        }
        
    
      @IBAction func btnChatAction(_ sender: UIButton) {
    //            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OpenChatView") as! OpenChatView
    //            nextViewController.LocationID = playerDetailResult[0]["LocationID"] as? String ?? ""
    //       //already commented previously  // nextViewController.commentData = playerDetailResult[0]
    //            self.navigationController?.pushViewController(nextViewController, animated: true)
            
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatListView") as! ChatListView
                       self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
            @IBAction func btnAddLocationAction(_ sender: UIButton) {
            
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
  

}
