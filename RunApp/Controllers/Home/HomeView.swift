//
//  HomeView.swift
//  RunApp

//
//  Created by My Mac on 18/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
///Volumes/Untitled/Run APP git/RunApp.xcworkspace

import UIKit
import MapKit
import SDWebImage
import SVProgressHUD
import Alamofire
import CoreLocation
import NightNight
import FirebaseAnalytics
import Firebase


struct EventLocation {
    let LocationAddress: String
    let LocationID: String
    let EventStatus: String
    let latitude: Double
    let longitude: Double
    let isevent:Bool
    init(LocationAddress: String, LocationID: String, EventStatus: String, latitude: Double, longitude: Double, isevent: Bool) {
        self.LocationAddress = LocationAddress
        self.LocationID = LocationID
        self.EventStatus = EventStatus
        self.latitude = latitude
        self.longitude = longitude
        self.isevent = isevent
    }
 }


class HomeView: UIViewController , MKMapViewDelegate , CLLocationManagerDelegate ,UIGestureRecognizerDelegate, DataProtocol
{
    //MARK:- PROTOCOL METHODS
   

    func UpdateLocationData(AddLocationDetail: [String : Any]) {
    
//        lagitude = Double(AddLocationDetail["latitude"] as? String ?? "") ?? 0
//        logitude = Double(AddLocationDetail["longitude"] as? String ?? "") ?? 0
//
//        print("AddLocationDetail: \(AddLocationDetail)")
//        print("latitude:\(lagitude) longitude: \(logitude)")
//
//        StoreLocationDetail = AddLocationDetail
//        let annotation3 = MKPointAnnotation()
//        annotation3.coordinate = CLLocationCoordinate2D(latitude: lagitude, longitude: logitude)
//                annotation3.title = "3"
//                annotation3.subtitle = AddLocationDetail["LocationID"] as? String ?? ""
//                mapVW.delegate = self
//                mapVW.addAnnotation(annotation3)
//                zoomToFitMapAnnotations(aMapView: mapVW)
    }
    
    var isstatic = true
    
    var locations = [EventLocation]()
    
    var lagitude = Double()
    var logitude = Double()
    
    @IBOutlet var runlogo: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var mapVW: MKMapView!
    var eventList = [[String : Any]]()
    var locationDicArray = [[String : Any]]()
    let regionRadius: CLLocationDistance = 10
    var locationManager = CLLocationManager()
    var myLocation:CLLocationCoordinate2D?
    var getLatitude = ""
    var getLogitude = ""
    var StoreLocationDetail = [String : Any]()
    var a:String!
    var startDate = String()
    var EndDate = String()
    var mapTimer: Timer?
    var location_idArray = [String]()
    let dateMaker = DateFormatter()
    var getCoordinate = CLLocationCoordinate2D()
    var flag = Bool()
     var isfirsttimehome = true
    
    
    
    //MARK:- Time Excute Code
    @objc func runTimedCode()  {
        print("Hey Hi Timer")
    }
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
               tapGestureRecognizer1.delegate = self
               runlogo.isUserInteractionEnabled = true
               runlogo.addGestureRecognizer(tapGestureRecognizer1)
        
        Analytics.trackPageView(withScreen: Analytics.Screen.exampleScreenName)
   
        
        
        
        
   
        
        
       // self.view.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        
       // flag = true
      
        
//        if CLLocationManager.locationServicesEnabled() == true {
//
//            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
//                locationManager.requestWhenInUseAuthorization()
//            }
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            locationManager.requestWhenInUseAuthorization()
//            locationManager.startUpdatingLocation()
//            locationManager.requestLocation()
//            mapVW.delegate = self
//           // mapVW.mapType = .hybrid
//        } else {
//            print("Please turn on location services or GPS")
//        }
    
        self.navigationController?.isNavigationBarHidden = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedHide(tapGestureRecognizer:)))
        imgUser.isUserInteractionEnabled = true
        imgUser.addGestureRecognizer(tapGestureRecognizer)
        imgUser.setRadius(radius: imgUser.frame.height/2)
        APP_USER_PROFILE_DETAIL()
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = CLLocationCoordinate2D(latitude: 41.8781 , longitude: -87.6298)
//        annotation.title = "1"
//        mapVW.addAnnotation(annotation)
//
//        let annotation2 = MKPointAnnotation()
//        annotation2.coordinate = CLLocationCoordinate2D(latitude: 41.8793 , longitude: -87.6228)
//        annotation2.title = "2"
//        mapVW.addAnnotation(annotation2)
//
//        let annotation3 = MKPointAnnotation()
//        annotation3.coordinate = CLLocationCoordinate2D(latitude: 41.8755 , longitude: -87.6255)
//        annotation3.title = "3"
//        mapVW.addAnnotation(annotation3)
//        zoomToFitMapAnnotations(aMapView: mapVW)
        //getEvents()
    }
    
    /*
     : Yellow
     startDate :06-05-2019 11:57
     EndDate :06-05-2019 18:59
     
     let dateMaker = NSDateFormatter()
     dateMaker.dateFormat = "yyyy/MM/dd HH:mm:ss"
     let start = dateMaker.dateFromString("2015/04/15 08:00:00")!
     let end = dateMaker.dateFromString("2015/04/15 16:30:00")!
     
     func isBetweenMyTwoDates(date: NSDate) -> Bool {
     if start.compare(date) == .OrderedAscending && end.compare(date) == .OrderedDescending {
     return true
     }
     return false
     }
     
     println(isBetweenMyTwoDates(dateMaker.dateFromString("2015/04/15 12:42:00")!)) // prints true
     println(isBetweenMyTwoDates(dateMaker.dateFromString("2015/04/15 17:00:00")!)) // prints false
    */
    
//    override func viewDidAppear(_ animated: Bool) {
//            UIApplication.shared.statusBarStyle = .lightContent
//       }
//       
//       override func viewDidDisappear(_ animated: Bool) {
//            UIApplication.shared.statusBarStyle = .default
//       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        appLiveLocation()
        
    
        
        let googleTracker = GAI.sharedInstance().tracker(withTrackingId: "UA-126391178-1")
        googleTracker!.set(kGAIScreenName, value: "MyPageName")
           let builder = GAIDictionaryBuilder.createScreenView()
        googleTracker!.send(builder!.build() as [NSObject : AnyObject])
        
       
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.requestLocation()
            mapVW.delegate = self
            
            // mapVW.mapType = .hybrid
        } else {
            print("Please turn on location services or GPS")
        }
    }
    
    //MARK:- Timing Check
    func isBetweenMyTwoDates(date: Date) -> Bool {
       let start = dateMaker.date(from: "2019/05/06 08:00:00")!
       let end = dateMaker.date(from: "2019/05/06 16:30:00")!
        if start.compare(date) == .orderedAscending && end.compare(date) == .orderedDescending {
            return true
        }
        return false
    }
    
    
    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
            
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
              nextViewController.type = "Adv"
             self.navigationController?.pushViewController(nextViewController, animated: true)
           
             
         }
    
    func CheckTime()->Bool {
        var timeExist:Bool
        let calendar = Calendar.current
        let startTimeComponent = DateComponents(calendar: calendar, hour:8)
        let endTimeComponent   = DateComponents(calendar: calendar, hour: 17, minute: 00)
        
        let now = Date()
        let startOfToday = calendar.startOfDay(for: now)
        let startTime    = calendar.date(byAdding: startTimeComponent, to: startOfToday)!
        let endTime      = calendar.date(byAdding: endTimeComponent, to: startOfToday)!
        
        if startTime <= now && now <= endTime {
            print("between 8 AM and 5:30 PM")
            timeExist = true
        } else {
            print("not between 8 AM and 5:30 PM")
            timeExist = false
        }
        
        return timeExist
    }
    
    //MARK:- Remove Annotation
    func removeSpecificAnnotation() {
        for annotation in self.mapVW.annotations {
            if let title = annotation.title, title == "0", title == "1",title == "2" {
                self.mapVW.removeAnnotation(annotation)
            }
        }
    }
    
    //MARK:- Web service
    
   
    
    func getEvents()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
            let parameters = ["location_id" : 0] as [String : Any]
            
            SVProgressHUD.show(withStatus: nil)
            
            Alamofire.request(
                URL(string: ServiceList.SERVICE_URL+ServiceList.GET_EVENT)!,
                method: .post,
                parameters: parameters)
                
                .validate()
                .responseJSON { (response) -> Void in
                    guard response.result.isSuccess
                        else {
                            print(response.result.error!)
                            SVProgressHUD.dismiss()
                            return
                    }
                    
                    var resData : [String : AnyObject] = [:]
                    guard let data = response.result.value as? [String:AnyObject],
                        let _ = data["status"]! as? String
                        else{
                            print("Malformed data received from fetchAllRooms service")
                            SVProgressHUD.dismiss()
                            
                            return
                    }
                    
                    resData = data
                    if resData["status"] as? String ?? "" == "1"
                    {
                        self.eventList = resData["Response"] as? [[String : Any]] ?? []
                       // print(self.eventList)
                        //self.setAllPins()
                    }
                    
                    SVProgressHUD.dismiss()
            }
        }
    }

//    func setAllPins()
//    {
//        for _ in 0..<eventList.count
//        {
//            print("Event List: \(eventList)")
//            let annotation = MKPointAnnotation()
//            annotation.coordinate = CLLocationCoordinate2D(latitude: 41.8781 , longitude: -87.6298)
//
//            annotation.title = "Chicago"
//
//            mapVW.delegate = self
//            mapVW.addAnnotation(annotation)
//
//        }
//
//      //  zoomToFitMapAnnotations(aMapView: mapVW)
//    }

    func appLiveLocation()
    {
        let parameters = ["Latitude" :  getCoordinate.latitude ,
                          "Longitude" : getCoordinate.longitude ,
                          "User_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""
            ] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_LIVE_LOCATION,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                  
                    print("App Live Reesponse Data: \(result)")
                
                     self.locations.removeAll()
                    
                    if result.getBool(key: "status")
                    {
                     
                    let GetData:NSDictionary = result["data"] as! NSDictionary
                      //  let GetData:NSDictionary = result["LocationID"] as! NSDictionary
                        
                        let Location = GetData.value(forKey: "location") as? [[String : Any]] ?? []
                         print("LocationData: \(Location)")
                
                        if let reviews = GetData["location"] as? [[String:Any]] {
                            reviews.forEach { review in
                                let LocationAddress = review["LocationAddress"] as? String ?? ""
                                let LocationID = review["LocationID"] as? String ?? ""
                                let latitude = review["latitude"] as? String ?? ""
                                let longitude = review["longitude"] as? String ?? ""
                                let latitudeD = Double(latitude)
                                let longitudeD = Double(longitude)
                                let IsEvent = review["IsEvent"] as? Bool ?? false
                                let eventCheck = (review["event"] as? NSDictionary)?.value(forKey: "EventStatus") as? String ?? ""
                                
                                self.locations.append(EventLocation(LocationAddress: LocationAddress, LocationID: LocationID, EventStatus: eventCheck, latitude: latitudeD! , longitude: longitudeD!, isevent: IsEvent))
                            }
                        }
                        self.mapVW.removeAnnotations(self.mapVW.annotations)

                        let annotations = self.locations.map { location -> MKAnnotation in
                            let annotation = MKPointAnnotation()
                           
                            if location.EventStatus == "0"
                            {
                                annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                                annotation.title = "0"
                                annotation.subtitle = location.LocationID
                            }
                                
                            else if location.EventStatus == "1"
                            {
                                annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                                annotation.title = "1"
                                annotation.subtitle = location.LocationID
                            }
                                
                            else if location.EventStatus == "2"{
                                annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude , longitude: location.longitude)
                                annotation.title = "2"
                                annotation.subtitle = location.LocationID
                             }
                            else if location.EventStatus == "3"
                            {
                               annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude , longitude: location.longitude)
                               annotation.title = "3"
                               annotation.subtitle = location.LocationID
                            }
                            else if location.EventStatus == "4"
                                                       {
                                                          annotation.coordinate = CLLocationCoordinate2D(latitude: location.latitude , longitude: location.longitude)
                                                          annotation.title = "4"
                                                          annotation.subtitle = location.LocationID
                                                       }
                               return annotation
                            }
                        
                           self.mapVW.addAnnotations(annotations)
                         //  self.zoomToFitMapAnnotations(aMapView: self.mapVW)
                       
                    }
        })
        
    }
    
    func APP_USER_PROFILE_DETAIL()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_USER_PROFILE,
                method: .post,
                param: ["UserID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "", "FromID": "0"] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        let DataDict = result["data"] as? [[String:Any]] ?? []
                     //   print("playerDetailResult: \(DataDict)")
                        
                        let imgURL = ServiceList.IMAGE_URL + "\(DataDict[0]["UserProfile"] as? String ?? "")"
                        self.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                            completed: nil)
                        
                        
                    //    let imgURL =  ServiceList.IMAGE_URL + result.getString(key: "UserProfile")
                                          UserDefaults.standard.set(imgURL, forKey: "userprofileimg")
                                          //self.imgUser.sd_setShowActivityIndicatorView(true)
                                     //     self.imgUser.sd_setIndicatorStyle(.gray)
                                     //     self.imgUser.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "user"))
                    }
        })
    }
    
    //MARK:- Button Action
   
    @IBAction func btnAddLocationAction(_ sender: UIButton) {
      
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
         nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchEventController") as! SearchEventController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func UpdateMapAction(_ sender: UIButton) {
        
        locationManager.startUpdatingLocation()
      //  self.zoomToFitMapAnnotations(aMapView: self.mapVW)
        self.mapVW.showsUserLocation = true
    }
   
    //MARK:- Gesture
    
    @objc func imageTappedHide(tapGestureRecognizer: UITapGestureRecognizer)
    {
      let  UserType = UserDefaults.standard.getUserDict()["usertype"] as? String ?? ""
              if UserType == "Organization" {
                
                print("Organization Login")
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                       self.navigationController?.pushViewController(nextViewController, animated: true)
                
              }
              else
              {
                
                print("Player Login")
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                       self.navigationController?.pushViewController(nextViewController, animated: true)
                
              }
       
    }
    
    //MARK:- Map Methods
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        var annotationView = mapVW.dequeueReusableAnnotationView(withIdentifier: "Pin")
        //annotationView.p
        if annotationView == nil{
            
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
            
        } else{
            
            annotationView?.annotation = annotation
        }
        print("GYM -->", annotation.title as Any)
        if annotation.title == "1"
        {
            annotationView?.image = UIImage(named: "yellowLocation")
        }
            
        else if annotation.title == "2"
        {
            annotationView?.image = UIImage(named: "map_green")
        }

        else if annotation.title == "0"
        {
            annotationView?.image = UIImage(named: "redLocation")
        }
        else if annotation.title == "3"
               {
                   annotationView?.image = UIImage(named: "map_green")
               }
       else if annotation.title == "4"
                     {
                         //annotationView?.image = UIImage(named: "Unknown-2")
                        annotationView?.image = UIImage(named: "blue-1")
                     }
        
        annotationView?.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
     //   annotationView!.canShowCallout = true

        return annotationView
    }

    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
      
         if view.annotation?.title == "0"
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
            nextViewController.LocationID = view.annotation?.subtitle ?? ""
            nextViewController.LocationDetail = StoreLocationDetail
            nextViewController.firstViewCont = self
            nextViewController.EventStatus = (view.annotation?.title)! ?? ""
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
         else if view.annotation?.title == "1"
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
            print(StoreLocationDetail)
             nextViewController.LocationID = view.annotation?.subtitle ?? ""
            nextViewController.Eventstatus = (view.annotation?.title)! ?? ""
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
            
        else if view.annotation?.title == "2"
        {
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GameNameTestController") as! GameNameTestController
//            nextViewController.LocationID = view.annotation?.subtitle ?? ""
//
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
//            nextViewController.LocationID = view.annotation?.subtitle ?? ""
//            nextViewController.LocationDetail = StoreLocationDetail
//             nextViewController.EventStatus = (view.annotation?.title)! ?? ""
//            nextViewController.firstViewCont = self
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                    print(StoreLocationDetail)
                     nextViewController.LocationID = view.annotation?.subtitle ?? ""
                    nextViewController.Eventstatus = (view.annotation?.title)! ?? ""
                    self.navigationController?.pushViewController(nextViewController, animated: true)
            
            
        }
            
 
        else if view.annotation?.title == "3"
               {
                
                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
                   nextViewController.LocationID = view.annotation?.subtitle ?? ""
                   nextViewController.LocationDetail = StoreLocationDetail
                    nextViewController.EventStatus = (view.annotation?.title)! ?? ""
                   nextViewController.firstViewCont = self
                   self.navigationController?.pushViewController(nextViewController, animated: true)
                
               }
        else if view.annotation?.title == "4"
                     {
                      
                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GymListViewController") as! GymListViewController
                        nextViewController.GYMID = (view.annotation?.subtitle)! ?? ""
                        
                        
                        
                         self.navigationController?.pushViewController(nextViewController, animated: true)
                      
                     }
              
    }
    
    func removeAllAnnotations() {
        for annotation in self.mapVW.annotations {
            
            self.mapVW.removeAnnotation(annotation)
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

//        if flag {
//            flag = false
//            if let location = locations.last {
//                //print("Found user's location: \(location)")
//                print("latitude: \(location.coordinate.latitude)")
//                print("longitude: \(location.coordinate.longitude)")
//                getCoordinate = location.coordinate
//                locationManager.stopUpdatingLocation()
//                appLiveLocation()
//            }
//        }
        if isfirsttimehome {
            
            isfirsttimehome = false
             if let userLocation = locationManager.location?.coordinate {
                       let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 5000, longitudinalMeters: 5000)
                       mapVW.setRegion(viewRegion, animated: false)
                
                   }
        }
       
       
        if  StoredData.shared.EventStatus == "Event" {
             removeAllAnnotations()
             self.locations.removeAll()
            StoredData.shared.EventStatus = ""
            if let location = locations.last {
                
                print("Found user's location: \(location)")
                    let userLocation :CLLocation = locations[0] as CLLocation
                print("latitude: \(userLocation.coordinate.latitude)")
                print("longitude: \(userLocation.coordinate.longitude)")
                getCoordinate = location.coordinate
                locationManager.stopUpdatingLocation()
                appLiveLocation()
                
            }
        }
            
        else {
            if flag {
                
                flag = false
                if let location = locations.last {
                    
                    print("Found user's location: \(location)")
                     let userLocation :CLLocation = locations[0] as CLLocation
                    print("latitude: \(userLocation.coordinate.latitude)")
                    print("longitude: \(userLocation.coordinate.longitude)")
                    getCoordinate = location.coordinate
                    locationManager.stopUpdatingLocation()
                    appLiveLocation()
                    
                }
            }
        }

    }
    
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        //Access the last object from locations to get perfect current location
//
//        print("Update Location")
//        if let location = locations.last {
//            let span = MKCoordinateSpan(latitudeDelta: 0.00775, longitudeDelta: 0.00775)
//
//            if(isstatic == true)
//            {
//                let myLocation = CLLocationCoordinate2D(latitude: 41.8781 , longitude: -87.6298)
//                let region = MKCoordinateRegion(center: myLocation, span: span)
//                getCoordinate = location.coordinate
//                //appLiveLocation()
//                mapVW.setRegion(region, animated: true)
//                isstatic = false
//            }
//
//            else {
//                let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
//                let region = MKCoordinateRegion(center: myLocation, span: span)
//                getCoordinate = location.coordinate
//
//                appLiveLocation()
//                mapVW.setRegion(region, animated: true)
//            }
//        }
//
//        self.mapVW.showsUserLocation = true
//        locationManager.stopUpdatingLocation()
//    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Unable to access your current location")
    }
    
//       /*
//        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        let geoCoder = CLGeocoder()
//         locationManager.stopUpdatingLocation()
//        let location = CLLocation(latitude: locValue.latitude,
//                                  longitude: locValue.longitude)
//
//
//        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
//
//            // Place details
//            var placeMark: CLPlacemark!
//            placeMark = placemarks?[0]
//
//            // Address dictionary
//           // print(placeMark.addressDictionary)
//
//            // Location name
//            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
//                print(locationName)
//            }
//
//            // Street address
//            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
//                print(street)
//            }
//
//            // City
//            if let city = placeMark.addressDictionary!["City"] as? NSString {
//                print(city)
//            }
//
//            // Zip code
//            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
//                print(zip)
//            }
//
//            // Country
//            if let country = placeMark.addressDictionary!["Country"] as? NSString {
//                print(country)
//            }
//
//        })
//
//          centerMap(locValue)
//        let circle = MKCircle(center: location.coordinate, radius: 10)
//        self.mapVW.addOverlay(circle)
//       */
//    }

    func centerMap(_ center:CLLocationCoordinate2D)
    {
        
        self.saveCurrentLocation(center)
        let spanX = 0.007
        let spanY = 0.007
        let newRegion = MKCoordinateRegion(center:center , span: MKCoordinateSpan(latitudeDelta: spanX, longitudeDelta: spanY))
        mapVW.setRegion(newRegion, animated: true)
        
    }
    
    func saveCurrentLocation(_ center:CLLocationCoordinate2D) {
        let message = "\(center.latitude) , \(center.longitude)"
        print("message: \(message)")
        mapVW.showsUserLocation = true
       
        //self.lable.text = message
        //myLocation = centerlet annotation = MKPointAnnotation()
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = CLLocationCoordinate2D(latitude: center.latitude , longitude: center.longitude)
//        annotation.title = "1"
        // mapVW.addAnnotation(annotation)
    }
    
    func zoomToFitMapAnnotations(aMapView: MKMapView) {
        guard aMapView.annotations.count > 0 else {
            return
        }

        var topLeftCoord: CLLocationCoordinate2D = CLLocationCoordinate2D()
        topLeftCoord.latitude = -90
        topLeftCoord.longitude = 180
        var bottomRightCoord: CLLocationCoordinate2D = CLLocationCoordinate2D()
        bottomRightCoord.latitude = 90
        bottomRightCoord.longitude = -180
        for annotation: MKAnnotation in aMapView.annotations {
            topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude)
            topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude)
            bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude)
            bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude)
        }

        var region: MKCoordinateRegion = MKCoordinateRegion()
        region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5
        region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5
        region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.4
        region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.4
        region = aMapView.regionThatFits(region)
        aMapView.setRegion(region, animated: true)
        
//        if let userLocation = locationManager.location?.coordinate {
//            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
//            mapVW.setRegion(viewRegion, animated: false)
//        }
    }
    @IBAction func chat_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatListView") as! ChatListView
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func btn_invite(_ sender: Any) {
       
    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
        nextViewController.type = "Adv"
    self.navigationController?.pushViewController(nextViewController, animated: true)
     
                 
                  
        
    }
}

/*
 @IBAction func geocode(_ sender: UIButton) {
 guard let country = countryTextField.text else { return }
 guard let street = streetTextField.text else { return }
 guard let city = cityTextField.text else { return }
 
 // Create Address String
 let address = "\(country), \(city), \(street)"
 
 // Geocode Address String
 geocoder.geocodeAddressString(address) { (placemarks, error) in
 // Process Response
 self.processResponse(withPlacemarks: placemarks, error: error)
 }
 
 // Update View
 geocodeButton.isHidden = true
 activityIndicatorView.startAnimating()
 }
 
 
 import CoreLocation
 lazy var geocoder = CLGeocoder()
 
 private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
 // Update View
 geocodeButton.isHidden = false
 activityIndicatorView.stopAnimating()
 
 if let error = error {
 print("Unable to Forward Geocode Address (\(error))")
 locationLabel.text = "Unable to Find Location for Address"
 
 } else {
 var location: CLLocation?
 
 if let placemarks = placemarks, placemarks.count > 0 {
 location = placemarks.first?.location
 }
 
 if let location = location {
 let coordinate = location.coordinate
 locationLabel.text = "\(coordinate.latitude), \(coordinate.longitude)"
 } else {
 locationLabel.text = "No Matching Location Found"
 }
 }
 }
 
 https://cocoacasts.com/forward-geocoding-with-clgeocoder
 https://stackoverflow.com/questions/49629332/convert-lat-long-to-location-address-in-ios-swift
 */



