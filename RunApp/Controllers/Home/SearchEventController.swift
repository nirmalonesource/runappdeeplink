//
//  SearchEventController.swift
//  RunApp
///
//  Created by My Mac on 08/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import NightNight
import FGRoute
import FirebaseAnalytics
import Firebase
//import Crashlytics

enum selectedScope:Int {
    case playerId = 0
    case placeId = 1
    case newsId = 2
    case orgId = 3
}

class Place: NSObject {
    var imgPin:String = ""
    var LocationID:String = ""
    var LocationName:String = ""
    var address:String = ""
    var Distance:String = ""
    var LOC:String = ""
    var status:String = ""
    
    init(imgPin:String ,LocationID :String, LocationName:String,address:String,Distance:String,LOC:String,status:String) {
        self.imgPin = imgPin
        self.LocationID = LocationID
        self.LocationName = LocationName
        self.address = address
        self.Distance = Distance
        self.LOC = LOC
        self.status = status
    }
}

class Event: NSObject {
    var imgPin:String = ""
    var EventId:String = ""
    var EventName:String = ""
    var DateTime:String = ""
    var Desc:String = ""
    var LocationID:String = ""
    var status:String = ""
    
    init(imgPin:String,EventId:String, EventName:String,DateTime:String,Desc:String,LocationID:String,status:String) {
        self.imgPin = imgPin
        self.EventId = EventId
        self.EventName = EventName
        self.DateTime = DateTime
        self.Desc = Desc
        self.LocationID = LocationID
        self.status = status
    }
}

class Player: NSObject {
    var imgUser:String = ""
    var PlayerName:String = ""
    var School:String = ""
    var Location:String = ""
     var CheckedIn:String = ""
    init(imgUser:String, PlayerName:String,School:String,Location:String,CheckedIn:String) {
        self.imgUser = imgUser
        self.PlayerName = PlayerName
        self.School = School
        self.Location = Location
        self.CheckedIn = CheckedIn
    }
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?,value:Any) -> (String,Any)? in
            guard label != nil else { return nil }
            return (label!,value)
        }).compactMap{ $0 })
        return dict
    }
}

class Org: NSObject {
    var imgOrg:String = ""
    var OrgName:String = ""
    var OrgSchool:String = ""
    var OrgLocation:String = ""
     var OrgCheckedIn:String = ""
    init(imgOrg:String, OrgName:String,OrgSchool:String,OrgLocation:String,OrgCheckedIn:String) {
        self.imgOrg = imgOrg
        self.OrgName = OrgName
        self.OrgSchool = OrgSchool
        self.OrgLocation = OrgLocation
        self.OrgCheckedIn = OrgCheckedIn
    }
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?,value:Any) -> (String,Any)? in
            guard label != nil else { return nil }
            return (label!,value)
        }).compactMap{ $0 })
        return dict
    }
}



class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var lblLocationName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblLOC: UILabel!
    override func awakeFromNib() {
        lblLocationName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}

class SearchPlayerListCell: UITableViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblPlayerName: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet var imglocation: UIImageView!
    
    @IBOutlet weak var lblLoc: UILabel!
    
    override func awakeFromNib() {
        lblPlayerName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}
class SearchOrgListCell: UITableViewCell {
    
    @IBOutlet weak var imgOrg: UIImageView!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var lblOrgSchool: UILabel!
    @IBOutlet weak var lblOrgLocation: UILabel!
    @IBOutlet var imgOrglocation: UIImageView!
    
    
    override func awakeFromNib() {
        lblOrgName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
    
    
}



class SearchEventTableViewCell: UITableViewCell {
    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        lblEventName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
    }
}

class EventCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    
    override func awakeFromNib() {
         lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLine.mixedTextColor =  MixedColor(normal: 0xffffff, night:0x000000 )
    }
}

class CollectionTrendingCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblLast: UILabel!
    override func awakeFromNib() {
        lblName.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
        lblLast.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )

    }
}

class SearchEventController: UIViewController , UITableViewDataSource ,  UITableViewDelegate , UISearchBarDelegate,UIGestureRecognizerDelegate {

    @IBOutlet var runlogo: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblViewPlayers: UITableView!
   
    
    @IBOutlet weak var imgrightarrow: UIImageView!
    @IBOutlet weak var imgleftarrow: UIImageView!
    @IBOutlet weak var tblViewOrganization: UITableView!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var tblview3: UITableView!
    @IBOutlet weak var vwPopupTop2: UIView!
    
    @IBOutlet weak var tblView2: UITableView!
  
    @IBOutlet weak var lblSearchTitle: UILabel!
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    var selectedItem = Int()
    var TreadingID = String()
    
    @IBOutlet weak var CollectionTrending: UICollectionView!
    @IBOutlet var lbltredingheadline: UILabel!
    
    var nameArray = [String]()
    var eventList = [[String : Any]]()
    var placeList = [[String : Any]]()
    var playerList = [[String : Any]]()
    var Orglist = [[String : Any]]()
    var newsFeedList = [[String : Any]]()
    var treadingList = [[String : Any]]()
    
   // let initialDataAry:[Model] = Model.generateModelArray()

    var playerListArray = [Player]()
    var PlaceListArray = [Place]()
    var EventListArray = [Event]()
    var OrgListArray = [Org]()
    var dataPlayer:[Player] = []
    var dataOrg:[Org] = []
    var dataPlace:[Place] = []
    var dataEvent:[Event] = []
   
    //MARK:- Parse ResponseData

    func parseData(_ data : Data) {
        do {
            let readableJSON = try JSONSerialization.jsonObject(with: data , options: .mutableContainers) as! [String :AnyObject]
            print("readableJSON : \(readableJSON)")
            
            self.playerList = readableJSON["data"] as? [[String:Any]] ?? []
            self.tblViewPlayers.reloadData()
            
            showToast(uiview: self, msg: readableJSON["message"]! as! String)
            SVProgressHUD.dismiss()
        }
        catch{
            
            print(error)
            
        }
    }
    
    
    func searchBarSetup() {
       // searchBar.showsScopeBar = true
        //searchBar.scopeButtonTitles = ["Name","Year","By"]
        //searchBar.selectedScopeButtonIndex = 0
        searchBar.delegate = self
    }
    
    // MARK: - search bar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty && selectedItem == 0 {
            dataPlayer = playerListArray
            self.tblViewPlayers.reloadData()
        }
        
        else if searchText.isEmpty && selectedItem == 1 {
            dataPlace = PlaceListArray
            self.tblView2.reloadData()
            
        }
        
         else if searchText.isEmpty && selectedItem == 2 {
            dataEvent = EventListArray
            self.tblview3.reloadData()
        }
        else if searchText.isEmpty && selectedItem == 3 {
            dataOrg = OrgListArray
            self.tblview3.reloadData()
        }
        else {
           
            filterTableView(ind: selectedItem, text: searchText)
        }
    }
    
    func filterTableView(ind:Int,text:String) {
        switch ind {
        case selectedScope.playerId.rawValue:
            //fix of not searching when backspacing
            dataPlayer = playerListArray.filter({ (mod) -> Bool in
                return mod.PlayerName.lowercased().contains(text.lowercased()) || mod.School.lowercased().contains(text.lowercased()) || mod.Location.lowercased().contains(text.lowercased())
            })
            
            self.tblViewPlayers.reloadData()
         case selectedScope.placeId.rawValue:
            //fix of not searching when backspacing
            dataPlace = PlaceListArray.filter({ (mod) -> Bool in
                return mod.LocationName.lowercased().contains(text.lowercased()) ||
                    mod.address.lowercased().contains(text.lowercased()) ||
                    mod.LOC.lowercased().contains(text.lowercased())
            })
            
            self.tblView2.reloadData()
          case selectedScope.newsId.rawValue:
            //fix of not searching when backspacing
            dataEvent = EventListArray.filter({ (mod) -> Bool in
                return mod.EventName.lowercased().contains(text.lowercased()) ||
                    mod.DateTime.lowercased().contains(text.lowercased()) ||
                    mod.Desc.lowercased().contains(text.lowercased())
            })
            
            self.tblview3.reloadData()
            
            case selectedScope.orgId.rawValue:
            //fix of not searching when backspacing
            dataOrg = OrgListArray.filter({ (mod) -> Bool in
                return mod.OrgName.lowercased().contains(text.lowercased()) || mod.OrgSchool.lowercased().contains(text.lowercased()) || mod.OrgLocation.lowercased().contains(text.lowercased())
            })
            tblViewOrganization.reloadData()
        default:
            print("no type")
        }
    }
    
    //MARK:- Set Date
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.date(from: strDate)
       // dateFormatter.dateFormat = "yyyy-MM-dd HH:mm a"
        
        
          dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let Date24 = dateFormatter.string(from: date!)
        print("24 hour formatted Date:",Date24)
        return Date24

    }
    
    //MARK:- Web service
    
    func getNewsFeed()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_APP_TICKER,
                method: .get,
                param: [:] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                      //  let dic = result["data"] as! NSDictionary
                    //    self.newsFeedList = dic.value(forKey:  "news") as! [[String : Any]]
                        self.newsFeedList = result["data"] as? [[String:Any]] ?? []
                        //self.CollectionView2nd.reloadData()
                    }
        })
    }
    
    func playerListApi()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]

        callApi(ServiceList.SERVICE_URL+ServiceList.APP_PLAYER_LIST,
                method: .get,
                param: [:] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.playerList = result["data"] as? [[String:Any]] ?? []
                         for i in 0..<self.playerList.count {
                            
                            let id = self.playerList[i]["id"] as? String ?? ""
                            
                            if id != UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
                                let PlayerName = self.playerList[i]["username"] as? String ?? ""
                                let School = self.playerList[i]["School"] as? String ?? ""
                                let imgUser = self.playerList[i]["UserProfile"] as? String ?? ""
                                let Hometown = self.playerList[i]["Hometown"] as? String ?? ""
                                let CheckedIn = self.playerList[i]["CheckedIn"] as? String ?? ""
                                self.playerListArray.append(Player(imgUser: imgUser, PlayerName: PlayerName, School: School, Location: Hometown, CheckedIn: CheckedIn))
                            }
                        }
                        self.dataPlayer = self.playerListArray
                        self.tblViewPlayers.reloadData()
                    }
        })
    }
   
    func OrgListApi()
       {
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]

           callApi(ServiceList.SERVICE_URL+ServiceList.APP_Org_LIST,
                   method: .get,
                   param: [:] ,
                   extraHeader: header ,
                   completionHandler: { (result) in
                       print("ORgList",result)
                       if result.getBool(key: "status")
                       {
                           self.Orglist = result["data"] as? [[String:Any]] ?? []
                            for i in 0..<self.Orglist.count {
                               
                               let id = self.Orglist[i]["id"] as? String ?? ""
                               
                               if id != UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
                                   let orgname = self.Orglist[i]["username"] as? String ?? ""
                                   let School = self.Orglist[i]["School"] as? String ?? ""
                                   let imgUser = self.Orglist[i]["UserProfile"] as? String ?? ""
                                   let Hometown = self.Orglist[i]["Hometown"] as? String ?? ""
                                   let CheckedIn = self.Orglist[i]["CheckedIn"] as? String ?? ""
                                   self.OrgListArray.append(Org(imgOrg:imgUser, OrgName:orgname,OrgSchool:School,OrgLocation:Hometown,OrgCheckedIn:CheckedIn))
                               }
                           }
                           self.dataOrg = self.OrgListArray
                           self.tblViewOrganization.reloadData()
                       }
           })
       }
    
    
    
    
    func getPlaces()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOCATIONS,
                method: .post,
                param: ["LocationID" : "0"] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.placeList = result["data"] as? [[String:Any]] ?? []
                       
                        for i in 0..<self.placeList.count {
                            let LocationName = self.placeList[i]["LocationName"] as? String ?? ""
                            let address = self.placeList[i]["LocationAddress"] as? String ?? ""
                            let imgPin = self.placeList[i]["product_image"] as? String ?? ""
                            let LocationID = self.placeList[i]["LocationID"] as? String ?? ""
                            let Distance = self.placeList[i]["product_image"] as? String ?? ""
                            let LOC = self.placeList[i]["LocMode"] as? String ?? ""
                            let status = self.placeList[i]["status"] as? String ?? ""
                            
                            self.PlaceListArray.append(Place(imgPin: imgPin, LocationID: LocationID, LocationName: LocationName, address: address, Distance: Distance, LOC: LOC, status: status))
                        }
                        self.dataPlace = self.PlaceListArray
                        print(self.dataPlace)
                        self.tblView2.reloadData()
                    }
        })
    }

    func getEventWithNewStructure()
    {
        let parameters = ["EventID" : 0] as [String : Any]
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_EVENT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.eventList = result["data"] as? [[String:Any]] ?? []
                        for i in 0..<self.eventList.count {
                            let imgPin = self.eventList[i]["product_image"] as? String ?? ""
                            let EventId = self.eventList[i]["EventID"] as? String ?? ""
                            let EvenetName = self.eventList[i]["EvenetName"] as? String ?? ""
                            let Desc = self.eventList[i]["CreatedBy"] as? String ?? ""
                            let EventDate = self.eventList[i]["EventDate"] as? String ?? "" != "0000-00-00 00:00:00" ? self.setDateWithFormat(strDate: self.eventList[i]["EventDate"] as? String ?? "") : ""
                            let LocationID = self.eventList[i]["LocationID"] as? String ?? ""
                            let status = self.eventList[i]["status"] as? String ?? ""
                            
                            self.EventListArray.append(Event(imgPin: imgPin, EventId: EventId, EventName: EvenetName, DateTime: EventDate, Desc: Desc, LocationID: LocationID, status: status))
                        }
                        self.dataEvent = self.EventListArray
                        self.tblview3.reloadData()
                    }
        })
        
    }
    
    /*
      return treadingList.count
     
     
 */
    
    func getTrendingHeadline()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.APP_TREADING_HEADLINE,
                method: .get,
                param: [:] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        print(result.getBool(key: "status"))
                     self.treadingList = result["data"] as? [[String:Any]] ?? []
                        self.CollectionTrending.reloadData()
                    }
        })
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.searchBarSetup()
       // hideKeyboardWhenTappedAround()
        
        
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
           tapGestureRecognizer2.delegate = self
           runlogo.isUserInteractionEnabled = true
           runlogo.addGestureRecognizer(tapGestureRecognizer2)
        
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedLeft(tapGestureRecognizer:)))
           imgleftarrow.isUserInteractionEnabled = true
           imgleftarrow.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(imageTappedRight(tapGestureRecognizer:)))
                  imgrightarrow.isUserInteractionEnabled = true
                  imgrightarrow.addGestureRecognizer(tapGestureRecognizer1)
        
        
        imgleftarrow.transform = CGAffineTransform(rotationAngle:CGFloat(M_PI));

        
       // imgBG.mixedImage = MixedImage(normal: UIImage(named: "imgAppBG1") ?? UIImage(), night: UIImage(named: "imgAppBG1-W") ?? UIImage())
        vwPopupTop.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
         lblSearchTitle.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
         lbltredingheadline.mixedTextColor =  MixedColor(normal: 0x000000, night:0xffffff )
//        searchBar.mixedTintColor = MixedColor(normal: 0x000000, night:0xffffff )
//        searchBar.mixedBarTintColor = MixedColor(normal: 0x000000, night:0xffffff )
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.lightGray
        
        searchBar.delegate = self
        searchBar.backgroundImage = UIImage()

        tblView2.isHidden = true
        tblview3.isHidden = true
        tblViewOrganization.isHidden = true
        
        tblViewPlayers.tableFooterView = UIView()
        tblView2.tableFooterView = UIView()
        tblview3.tableFooterView = UIView()
        tblViewPlayers.tableFooterView = UIView()
        tblViewOrganization.tableFooterView = UIView()
        
        tblViewOrganization.delegate = self
        
        nameArray = ["Players","Places","Events","Organization"]
        
        self.view.layoutIfNeeded()
//        imgBG.setGredient()
        
        vwPopupTop.setRadius(radius: 10)
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        vwPopupTop2.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        
        playerListApi()
        OrgListApi()
        getEventWithNewStructure()
        getPlaces()
        getNewsFeed()
        getTrendingHeadline()
        self.view.layoutIfNeeded()
        
    }
    
    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
            
             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
              nextViewController.type = "Adv"
             self.navigationController?.pushViewController(nextViewController, animated: true)
           
             
         }
    
    
    //MARK:- Table methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblViewPlayers {
            return dataPlayer.count
        }
            
        else if tableView == tblView2 {
            return dataPlace.count
        }
            
        else if tableView == tblview3 {
            return dataEvent.count
        }
        else
        {
        return dataOrg.count
              //return dataPlayer.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == tblViewPlayers {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlayerListCell", for: indexPath) as! SearchPlayerListCell
            let playerData = dataPlayer[indexPath.row]
            cell.lblPlayerName.text = playerData.PlayerName
            cell.lblSchool.text = playerData.School
            cell.lblLocation.text = playerData.Location
            cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
            cell.imgUser.sd_setImage(with: URL(string : (playerData.imgUser.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
            let imgURL = ServiceList.IMAGE_URL + playerData.imgUser
            
            cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)
            
            if playerData.CheckedIn == "1" {
                cell.imglocation.image = UIImage (named: "LocationGreen")
            }
            else
            {
                 cell.imglocation.image = UIImage (named: "redLocation")
            }
            
            return cell
        }
        
        else if tableView == tblview3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchEventTableViewCell", for: indexPath) as! SearchEventTableViewCell
            
//            cell.lblEventName.text = eventList[indexPath.row]["EvenetName"] as? String
//            cell.lblDesc.text = eventList[indexPath.row]["CreatedBy"] as? String
//           // cell.lblDateTime.text = eventList[indexPath.row]["EventDate"] as? String
//
//            cell.lblDateTime.text = eventList[indexPath.row]["EventDate"] as? String ?? "" != "0000-00-00 00:00:00" ? setDateWithFormat(strDate: eventList[indexPath.row]["EventDate"] as? String ?? "") : ""
            let eventData = dataEvent[indexPath.row]
            cell.lblEventName.text = eventData.EventName
            cell.lblDesc.text = eventData.Desc
            cell.lblDateTime.text = eventData.DateTime
            
//            if eventData.status == "0"
//            {
//                cell.imgPin.image = UIImage(named: "redLocation")
//            }
//            else if eventData.status == "1"
//            {
//                cell.imgPin.image = UIImage(named: "map_green")
//            }
//            else
//            {
//                cell.imgPin.image = UIImage(named: "yellowLocation")
//            }
            
                        if eventData.status == "0"
                        {
                            cell.imgPin.image = UIImage(named: "redLocation")
                        }
                        else if eventData.status == "1"
                        {
                            cell.imgPin.image = UIImage(named: "yellowLocation")
                        }
                        else
                        {
                            cell.imgPin.image = UIImage(named: "map_green")
                        }
            if eventData.status == "4"
                                   {
//                                       cell.imgPin.image = UIImage(named: "Unknown-2")
                cell.imgPin.image = UIImage(named: "blue-1")
                                   }
            
            return cell
        }
       
        else if tableView == tblView2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! TableViewCell
           
            let placeData = dataPlace[indexPath.row]
            cell.lblLocationName.text = placeData.LocationName
            cell.lblAddress.text = placeData.address
            cell.lblLOC.text = "LOC: \(placeData.LOC)"
            cell.lblDistance.text = "\(placeData.Distance) :Miles"
            
//            if placeData.status == "0"
//            {
//                cell.imgPin.image = UIImage(named: "redLocation")
//            }
//            else if placeData.status == "1"
//            {
//                cell.imgPin.image = UIImage(named: "map_green")
//            }
//            else
//            {
//                cell.imgPin.image = UIImage(named: "yellowLocation")
//            }
             if placeData.status == "0"
                        {
                            cell.imgPin.image = UIImage(named: "redLocation")
                        }
                        
                        else if placeData.status == "1"
                        {
                            cell.imgPin.image = UIImage(named: "yellowLocation")
                        }
            else
            {
                cell.imgPin.image = UIImage(named: "map_green")
            }

            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchOrgListCell", for: indexPath) as! SearchOrgListCell
                       let playerData = dataOrg[indexPath.row]
                       cell.lblOrgName.text = playerData.OrgName
                       cell.lblOrgSchool.text = playerData.OrgSchool
                       cell.lblOrgLocation.text = playerData.OrgLocation
                       cell.imgOrg.setRadius(radius: cell.imgOrg.frame.height/2)
                       cell.imgOrg.sd_setImage(with: URL(string : (playerData.imgOrg.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
                       let imgURL = ServiceList.IMAGE_URL + playerData.imgOrg

                       cell.imgOrg.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage: #imageLiteral(resourceName: "run"), options: .progressiveLoad, completed: nil)

            if playerData.OrgCheckedIn == "1" {
                           cell.imgOrglocation.image = UIImage (named: "LocationGreen")
                       }
                       else
                       {
                            cell.imgOrglocation.image = UIImage (named: "redLocation")
                       }
                       return cell
        }
         
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblViewPlayers {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
             //  nextViewController.playerListDetail = playerList[indexPath.row]
             let playerData = dataPlayer[indexPath.row]
           // nextViewController.playerListDetail =  playerData.asDictionary
            for i in 0..<self.playerList.count {
                
                let id = self.playerList[i]["id"] as? String ?? ""
                
                if id != UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
                   
                    let PlayerName = self.playerList[i]["username"] as? String ?? ""
                    if playerData.PlayerName == PlayerName
                    {
                        nextViewController.playerListDetail = playerList[i]
                        print(playerList[i])
                    }
                }
            }
            nextViewController.isfromsearch = true;
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
            
        else if tableView == tblView2 {
            
            let placeData = dataPlace[indexPath.row]
            print("Event status : \(placeData.status)")
           
            
//            if placeData.status == "0" {
//
//                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
//
//                nextViewController.LocationID = placeData.LocationID
//                nextViewController.Eventstatus = placeData.status
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//
//
//            }
//
//            else if placeData.status == "1" {
//                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GameNameTestController") as! GameNameTestController
//                nextViewController.LocationID = placeData.LocationID
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//            }
//            else
//            {
//             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
//
//                           nextViewController.LocationID = placeData.LocationID
//                           nextViewController.Eventstatus = placeData.status
//                           self.navigationController?.pushViewController(nextViewController, animated: true)
//            }
            
            
            
            
            if placeData.status == "0" {
                        
//                           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
//
//                           nextViewController.LocationID = placeData.LocationID
//                           nextViewController.Eventstatus = placeData.status
//                           self.navigationController?.pushViewController(nextViewController, animated: true)
                              
                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
                                   nextViewController.LocationID = placeData.LocationID
                            nextViewController.EventStatus = placeData.status
                              self.navigationController?.pushViewController(nextViewController, animated: true)
                          
                   
                       }
                           else if placeData.status == "1"
                                                {
                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                                                               
                            nextViewController.LocationID = placeData.LocationID
                            nextViewController.Eventstatus = placeData.status
                       self.navigationController?.pushViewController(nextViewController, animated: true)
                           
            }
                                     
                       else if  placeData.status == "2" {
                
//                           let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GameNameTestController") as! GameNameTestController
//                           nextViewController.LocationID = placeData.LocationID
//                           self.navigationController?.pushViewController(nextViewController, animated: true)
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
                             nextViewController.LocationID = placeData.LocationID
                            nextViewController.EventStatus = placeData.status
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                
                       }

                                       else if  placeData.status == "4" {
                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GymListViewController") as! GymListViewController
                            nextViewController.GYMID =  placeData.LocationID
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                
                                       }
                
        else
            {
            
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
                nextViewController.LocationID = placeData.LocationID
               nextViewController.EventStatus = placeData.status
               self.navigationController?.pushViewController(nextViewController, animated: true)
           
            }
            
        }
            
        else if tableView == tblview3 {
            print(dataEvent)
            let eventData = dataEvent[indexPath.row]
            print("Event status : \(eventData.status)")
            print("Event location id : \(eventData.LocationID)")
             print("Event id : \(eventData.EventId)")
            
            
            
            
//            if eventData.status == "0" {
//                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
//                nextViewController.Eventstatus = eventData.status
//                nextViewController.LocationID = eventData.LocationID
//                nextViewController.EventID = eventData.EventId
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//            }
//
//            else if eventData.status == "1" {
//                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GameNameTestController") as! GameNameTestController
//                 nextViewController.LocationID = eventData.LocationID
//                 nextViewController.EventID = eventData.EventId
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//            }
            
                        if eventData.status == "0" {
                            
                          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
                            nextViewController.LocationID = eventData.LocationID
                            nextViewController.EventStatus = eventData.status
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
            
                        else if eventData.status == "1" {
                            
                          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "UpcomingRunViewController") as! UpcomingRunViewController
                                nextViewController.Eventstatus = eventData.status
                                nextViewController.LocationID = eventData.LocationID
                                nextViewController.EventID = eventData.EventId
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
            
            else if eventData.status == "2" {
                            
//                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GameNameTestController") as! GameNameTestController
//                 nextViewController.LocationID = eventData.LocationID
//                 nextViewController.EventID = eventData.EventId
//                self.navigationController?.pushViewController(nextViewController, animated: true)
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatBoardViewController") as! ChatBoardViewController
            nextViewController.LocationID = eventData.LocationID
            nextViewController.EventStatus = eventData.status
            self.navigationController?.pushViewController(nextViewController, animated: true)
                            
                            
            }
        }
        else if tableView == tblViewOrganization {
                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                    //  nextViewController.playerListDetail = playerList[indexPath.row]
                    let playerData = dataOrg[indexPath.row]
                  // nextViewController.playerListDetail =  playerData.asDictionary
                   for i in 0..<self.Orglist.count {
                       
                       let id = self.Orglist[i]["id"] as? String ?? ""

                       if id != UserDefaults.standard.getUserDict()["id"] as? String ?? "" {

                           let OrgName = self.Orglist[i]["username"] as? String ?? ""
                           if playerData.OrgName == OrgName
                           {
                               nextViewController.playerOrglResult = Orglist[i]
                            
                           }
                       }
                   }
                   nextViewController.isfromsearch = true;
                 self.navigationController?.pushViewController(nextViewController, animated: true)
//               }
        }
    }
  
    //MARK:- Search Action
    
    
    
    
    @IBAction func btnLocationClicked(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLocationController") as! AddNewLocationController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func searchBtnClicked(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchEventController") as! SearchEventController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
   @objc func imageTappedLeft(tapGestureRecognizer: UITapGestureRecognizer)
   {
      
    let visibleItems: NSArray = self.myCollectionView.indexPathsForVisibleItems as NSArray
              let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
              let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
              if nextItem.row < nameArray.count && nextItem.row >= 0{
                  self.myCollectionView.scrollToItem(at: nextItem, at: .right, animated: true)

              }
       // Your action
   }
    
    @objc func imageTappedRight(tapGestureRecognizer: UITapGestureRecognizer)
    {
       
      let visibleItems: NSArray = self.myCollectionView.indexPathsForVisibleItems as NSArray
               let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
               let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
                      if nextItem.row < nameArray.count {
                   self.myCollectionView.scrollToItem(at: nextItem, at: .left, animated: true)

               }
        // Your action
    }
   
    
    
  
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(searchBar.text!)")
        self.view.endEditing(true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Search Delegate
    
//    func updateSearchResults(for searchController: UISearchController) {
//        if let searchText = searchController.searchBar.text, !searchText.isEmpty {
//            dataPlayer = playerListArray.filter({ (product: Player) -> Bool in
//                return product.PlayerName.lowercased().contains(searchText.lowercased()) || product.School.lowercased().contains(searchText.lowercased()) || product.Location.lowercased().contains(searchText.lowercased())
//            })
//        }
//
//        else {
//            tblViewPlayers.reloadData()
//        }
//
//        tblViewPlayers.reloadData()
//    }
//    var timr=Timer()
//    var w:CGFloat=0.0
//
//    override func viewDidAppear(_ animated: Bool) {
//        configAutoscrollTimer()
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//         deconfigAutoscrollTimer()
//    }
//    func configAutoscrollTimer()
//    {
//
//        timr=Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(SearchEventController.autoScrollView), userInfo: nil, repeats: true)
//    }
//    func deconfigAutoscrollTimer()
//    {
//        timr.invalidate()
//
//    }
//    func onTimer()
//    {
//        autoScrollView()
//    }
//
//    @objc func autoScrollView()
//    {
//
//        let initailPoint = CGPoint(x: w,y :0)
//
//        if __CGPointEqualToPoint(initailPoint, CollectionView2nd.contentOffset)
//        {
//            if w<CollectionView2nd.contentSize.width
//            {
//                w += 2.0
//            }
//            else
//            {
//                w = -self.view.frame.size.width
//            }
//
//            let offsetPoint = CGPoint(x: w,y :0)
//
//            CollectionView2nd.contentOffset=offsetPoint
//
//        }
//        else
//        {
//            w=CollectionView2nd.contentOffset.x
//        }
//    }
}

extension SearchEventController: UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == myCollectionView {
        
            return nameArray.count
        }
            
        else {
             return treadingList.count
        }
    }
    
    /*
     let cell = tableView.dequeueReusableCell(withIdentifier: "CellTrending", for: indexPath) as! TrendingTableCell
     cell.lblTitle.text = treadingList[indexPath.row]["Treading_Title"] as? String
     cell.lblRun.text = treadingList[indexPath.row]["Treading_Desc"] as? String
     cell.imgTrending.setRadius(radius: cell.imgTrending.frame.height/2)
     let imgURL = ServiceList.IMAGE_URL + "\(treadingList[indexPath.row]["Treading_Image"] as? String ?? "")"
     cell.imgTrending.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
     completed: nil)
     return cell
     }
   */
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
          if collectionView == myCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCollectionViewCell", for: indexPath) as! EventCollectionViewCell
            cell.lblName.text = nameArray[indexPath.row]
            if indexPath.row == selectedItem {
                cell.lblLine.backgroundColor = UIColor.red
            }
            else {
                cell.lblLine.backgroundColor = UIColor.white
            }
       
             return cell
         }
            
          else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionTrendingCell", for: indexPath) as! CollectionTrendingCell
            cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
            let imgURL = ServiceList.IMAGE_URL + "\(treadingList[indexPath.row]["Treading_Image"] as? String ?? "")"
            cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad, completed: nil)
            cell.lblName.text = treadingList[indexPath.row]["Treading_Title"] as? String
            cell.lblLast.text = treadingList[indexPath.row]["Treading_Desc"] as? String
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == myCollectionView {
            return CGSize(width: collectionView.frame.width/3 - 10, height: 50)
        }
            
        else {
             return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == myCollectionView {
             selectedItem = indexPath.row
            lblSearchTitle.text = nameArray[indexPath.row] + ".."
            collectionView.reloadData()
            
            if indexPath.row == 0 {
                
                tblViewOrganization.isHidden = true
                tblView2.isHidden = true
                tblview3.isHidden = true
                tblViewPlayers.isHidden = false
                tblViewPlayers.reloadData()
            }
                
            else if indexPath.row == 1 {
                tblViewOrganization.isHidden = true
                tblViewPlayers.isHidden = true
                tblview3.isHidden = true
                tblView2.isHidden = false
                tblView2.reloadData()
            }
                
            else  if  indexPath.row == 2 {
                tblViewOrganization.isHidden = true
                tblViewPlayers.isHidden = true
                tblView2.isHidden = true
                tblview3.isHidden = false
                tblview3.reloadData()
            }
            else
            {
                tblViewPlayers.isHidden = true
                tblView2.isHidden = true
                tblview3.isHidden = true
                tblViewOrganization.isHidden = false
                tblViewOrganization.reloadData()
            }
        }
            
        else {
            print("Collection Trending")
            TreadingID = "\(treadingList[indexPath.row]["Treading_ID"] as? String ?? "")"
            let trendingurl = "\(treadingList[indexPath.row]["Treading_URL"] as? String ?? "")"
            ClickCount()
            let tracker = GAI.sharedInstance().defaultTracker

            tracker?.send(GAIDictionaryBuilder.createEvent(withCategory: "Game 1", action: "Start Pressed", label: "Start Timer One", value: nil).build() as [NSObject : AnyObject])
            
           
            
        //    Analytics.logEvent(withName: "SignUp", parameters: ["user_id": "", "user_name": ""])
            
              if (trendingurl != nil) {
            
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "webView") as! webView
                nextViewController.urlString = trendingurl
                         self.navigationController?.pushViewController(nextViewController, animated: true)
                     }
            
//            
//            if let url = URL(string:trendingurl) {
//                          UIApplication.shared.open(url)
//                      }
//            
            
            
        }
        
//           print("Other colletion View")
//            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
//            self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    
    
    func ClickCount()
          {
           let parameters = [
            "PlayerID":UserDefaults.standard.getUserDict()["id"] as? String ?? "","SponsorID":TreadingID,"IPAddress":FGRoute.getIPAddress()!,"AdType" : "TR"
            ] as [String : Any]
           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                         "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                         ] as [String : Any]
           
           callApi(ServiceList.SERVICE_URL+ServiceList.Sponser_click_count,
                   method: .post,
                   param: parameters,
                   extraHeader: header,
                   completionHandler: { (result) in
                       print(result)
                       if result.getBool(key: "status")
                       {
                           
                       }
           })
    

       }
    
}
