//
//  CartListViewController.swift
//  RunApp
//
//  Created by My Mac on 05/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight


class CartListCell: UITableViewCell {
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
}

class CartListViewController: UIViewController , UITableViewDataSource, UITableViewDelegate
{

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var tblChatView: UITableView!
    var productList = [[String : Any]]()
     var totalPoint = ""
    
    //MARK:- Web service
    
    func getProduct()
    {
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.GET_PRODUCT_LIST,
                method: .get,
                param: [:] ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        self.productList = result["data"] as? [[String:Any]] ?? []
                        self.tblChatView.reloadData()
                    }
        })
    }
    
    func buyProduct(product_id : String)
    {
        let parameters = ["ProductID" : product_id ,
                          "UserId" : UserDefaults.standard.getUserDict()["id"] as? String ?? ""] as [String : Any]
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : Any]
        
        callApi(ServiceList.SERVICE_URL+ServiceList.BUY_PRODUCT,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)
                    if result.getBool(key: "status")
                    {
                        showToast(uiview: self, msg: result.getString(key: "message"))
                    }
                    
        })
        
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
//        imgBG.setGredient()
        vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        tblChatView.tableFooterView = UIView()
       
        
        getProduct()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TABLEVIEW METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartListCell", for: indexPath) as! CartListCell
        
        cell.imgProduct.setRadius(radius: cell.imgProduct.frame.height/2)
        cell.imgProduct.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        cell.btnBuy.setRadius(radius: cell.btnBuy.frame.height/2)

        cell.lblProductName.text = productList[indexPath.row]["ProductName"] as? String
       // cell.lblDesc.text = "\(productList[indexPath.row]["ProductPrice"] as? String ?? "") points"
        
       
        cell.lblDesc.text = "\(totalPoint) points"
        
        let imgURL = ServiceList.IMAGE_URL + "\(productList[indexPath.row]["ProductImage"] as? String ?? "")"
        cell.imgProduct.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "run") , options: .progressiveLoad,
                            completed: nil)
        
        cell.btnBuy.tag = indexPath.row
        cell.btnBuy.addTarget(self, action: #selector(self.btnCellBuyAction(_:)), for: .touchUpInside)
        
      cell.lblProductName.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
        cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    //MARK:- Button Cell Action
    
    @objc func btnCellBuyAction(_ sender : UIButton)
    {
        let totalPointProduct = productList[sender.tag]["ProductPrice"] as? String ?? ""
        let getTotalPoint = Double(totalPointProduct)
       // let getStoredPoint = Double(StoredData.shared.totalPoint)
         let getStoredPoint = Double(totalPoint)
        
        if (getTotalPoint?.isLess(than: getStoredPoint!))! {
            print("Balance is there .. U can transfer money to someone!")
            
            let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Do you want to buy this product?")
            
            let action1 = EMAlertAction(title: "Yes", style: .cancel)
            {
                self.buyProduct(product_id: self.productList[sender.tag]["ProductID"] as? String ?? "0")
            }
            
            let action2 = EMAlertAction(title: "Cancel", style: .normal) {
                // Perform Action
            }
            
            alert.addAction(action: action1)
            alert.addAction(action: action2)
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            showAlert(uiview: self, msg: "Insufficient Point", isTwoButton: false)
            return
        }
        
      
    }
    

}
