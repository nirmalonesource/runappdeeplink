//
//  PrivacyPolicyView.swift
//  RunApp
//
//  Created by My Mac on 22/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import NightNight


class PrivacyPolicyView: UIViewController {
    
    
    @IBOutlet weak var txtview: UITextView!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var scrlVW: UIScrollView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
         vwPopupTop.setRadius(radius: 10)
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        txtview.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtview.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
     //MARK: - BUTTON METHODS
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
