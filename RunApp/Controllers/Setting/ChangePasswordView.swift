//
//  ChangePasswordView.swift
//  RunApp
//
//  Created by My Mac on 06/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import NightNight

class ChangePasswordView: UIViewController , UITextFieldDelegate
{
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    @IBOutlet weak var scrlVW: UIScrollView!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPasssword: UITextField!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
        txtOldPasssword.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtOldPasssword.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
        txtNewPassword.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtNewPassword.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        txtConfirmPassword.layer.mixedBorderColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        txtConfirmPassword.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        
        
        
        print(UserDefaults.standard.getUserDict()["email"] as? String ?? "")
        
//        imgBG.setGredient()
        vwPopupTop.setRadius(radius: 10)
        hideKeyboardWhenTappedAround()
        
        btnSubmit.setRadius(radius: btnSubmit.frame.height/2)
        
        for view in scrlVW.subviews as [UIView] {
            if let txt = view as? UITextField {
                txt.setRadius(radius: 10)
                txt.setRadiusBorder(color: UIColor.lightGray)
                txt.setLeftPaddingPoints(15, strImage: "")
            }
        }
    }
    
    //MARK:- WEB SERVICE
    
    func CHANGE_PASSWORD_LOGIN()
    {
        SVProgressHUD.show(withStatus: nil)
        let parameters = ["old_password" : txtOldPasssword.text! ,
                          "new_password" : txtNewPassword.text! ,
                          "user_id" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "confirm_new_password" : txtConfirmPassword.text!] as [String : Any]
        let headers = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : String]
        
        Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.CHANGE_PASSWORD_LOGIN)!,
                          method: .post,
                          parameters: parameters,
                          headers: headers).responseData
            { (response:DataResponse) in
                print(response.data as Any)
                do{
                    let readableJSON = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! [String :AnyObject]
                    print("readableJSON : \(readableJSON)")
                    if readableJSON.getBool(key: "status")
                    {
                        self.txtOldPasssword.text = ""
                        self.txtNewPassword.text = ""
                        self.txtConfirmPassword.text = ""
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    showToast(uiview: self, msg: readableJSON.getString(key: "message"))
                    SVProgressHUD.dismiss()
                    if let error = readableJSON["error"] as? String {
                        print("Error: \(error)")
                        SVProgressHUD.dismiss()
                    }
                }
                catch {
                    print(error)
                }
        }
    }
    
    func CHANGE_PASSWORD_LOGIN3()
    {
        let parameters = ["old_password" : txtOldPasssword.text! ,
                          "new_password" : txtNewPassword.text! ,
                          "user_id" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "confirm_new_password" : txtConfirmPassword.text!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ]
//        let url = ServiceList.SERVICE_URL+ServiceList.CHANGE_PASSWORD_LOGIN
//        let headers = [
//            "Content-Type":"application/x-www-form-urlencoded",
//            "authorization" : "apiKey"
//        ]
//
//        let configuration = URLSessionConfiguration.default
//        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
//        //let params : [String : Any] = ["param1":param1,"param2":param2]
//
//        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
//
//            if let JSON = response.result.value {
//                print("JSON: \(JSON)")
//
//            } else{
//                print("Request failed with error: ",response.result.error ?? "Description not available :(")
//            }
//        }
        callApi(ServiceList.SERVICE_URL+ServiceList.CHANGE_PASSWORD_LOGIN,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)

                    if result.getBool(key: "status")
                    {
                        
                        showToast(uiview: self, msg: result.getString(key: "message"))

                        self.txtOldPasssword.text = ""
                        self.txtNewPassword.text = ""
                        self.txtConfirmPassword.text = ""
                        self.navigationController?.popViewController(animated: true)
                    }
                    showToast(uiview: self, msg: result.getString(key: "message"))

        })
    }
    
     //MARK:- WEB SERVICE
    
    func CHANGE_PASSWORD_LOGIN1()
    {
         view.endEditing(true)
//        if !isInternetAvailable(){
//            noInternetConnectionAlert(uiview: self)
//        }
//
//        else
//        {
        
         let parameters = ["old_password" : txtOldPasssword.text! ,
                          "new_password" : txtNewPassword.text! ,
                          "user_id" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "confirm_new_password" : txtConfirmPassword.text!] as [String : Any]
        
        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                      "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                      ] as [String : String]
                
               // SVProgressHUD.show(withStatus: nil)
//            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.FORGOT_PASSWORD)!, method: .post, parameters: parameters, headers: header).validate().responseData { (<#DataResponse<Data>#>) in
//                <#code#>
//            }
            
//                Alamofire.request(
//                    URL(string: ServiceList.SERVICE_URL+ServiceList.FORGOT_PASSWORD)!,
//                    method: .post,
//                    parameters: parameters,
//                    headers: header)
//                    .validate()
//                    .responseJSON { (response) -> Void in
//                        guard response.result.isSuccess
//                            else {
//                                print(response.result.error!)
//                                SVProgressHUD.dismiss()
//                                return
//                        }
//
//                        var resData : [String : AnyObject] = [:]
//                        guard let data = response.result.value as? [String:AnyObject],
//                            let _ = data["status"]! as? String
//                            else{
//                                print("Malformed data received from fetchAllRooms service")
//                                SVProgressHUD.dismiss()
//                                return
//                        }
//
//                        resData = data
//                        if resData["status"] as? String ?? "" == "1"
//                        {
//                            self.navigationController?.popViewController(animated: true)
//                        }
//
//                        showToast(uiview: self, msg: resData["message"]! as! String)
//                        SVProgressHUD.dismiss()
//                }
            //}
        
       
        callApi(ServiceList.SERVICE_URL+ServiceList.CHANGE_PASSWORD_LOGIN,
                method: .post,
                param: parameters ,
                extraHeader: header ,
                completionHandler: { (result) in
                    print(result)

                    if result.getBool(key: "status")
                    {
                        self.txtOldPasssword.text = ""
                        self.txtNewPassword.text = ""
                        self.txtConfirmPassword.text = ""
                       self.navigationController?.popViewController(animated: true)
                    }
                  showToast(uiview: self, msg: result.getString(key: "message"))

        })
    }
    
    //MARK: - BUTTON METHODS
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSubmitAction(_ sender: UIButton){
       validationChange()
    }
    
    //MARK:- Textfield Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- Validation
    
    func validationChange() {
        
        self.view.endEditing(true)
        
        if txtOldPasssword.text == "" && txtNewPassword.text == "" && txtConfirmPassword.text == "" {
            showAlert(uiview: self, msg: "Please enter all fields ", isTwoButton: false)
        }
        
        if txtOldPasssword.text == "" {
           showAlert(uiview: self, msg: "Please enter Old Password", isTwoButton: false)
        }
        
       else if txtNewPassword.text == "" {
            showAlert(uiview: self, msg: "Please enter New Password", isTwoButton: false)
        }
            
        else if txtConfirmPassword.text == "" {
            showAlert(uiview: self, msg: "Please enter Confirm Password", isTwoButton: false)
        }
            
        else if txtNewPassword.text != txtConfirmPassword.text {
            showAlert(uiview: self, msg: "New Password & ConfirmPassword Do Not Match", isTwoButton: false)
        }
            
        else {
            CHANGE_PASSWORD_LOGIN3()
        }
    }
    
}
