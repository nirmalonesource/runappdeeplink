//
//  MyGymAttendedListViewController.swift
//  RunApp
//
//  Created by My Mac on 27/01/21.
//  Copyright © 2021 My Mac. All rights reserved.
//

import UIKit

import NightNight

class MYGymListCell: UITableViewCell {

    @IBOutlet weak var lblLoC: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var lblGymName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblFees: UILabel!
    @IBOutlet weak var lblPublish: UILabel!
    @IBOutlet weak var btnSelectOutlet: UIButton!

   
}

class MyGymAttendedListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
   @IBOutlet var vwPopup: UIView!
       
       
   @IBOutlet var tblmygymList: UITableView!
    
      var MyGymList = [[String : Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        vwPopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
             tblmygymList.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
                    
              
            
             vwPopup.setRadius(radius: 10)
             tblmygymList.tableFooterView = UIView()
        getMyGymList()
       
    }
    @IBAction func btn_back(_ sender: Any) {
           
            self.navigationController?.popViewController(animated: true)
           
           
       }
       
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return MyGymList.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
        let cell = tableView.dequeueReusableCell(withIdentifier: "MYGymListCell", for: indexPath) as! MYGymListCell
              
        cell.lblGymName.text = MyGymList[indexPath.row]["GymName"] as? String ?? ""
        //cell.lblDate.text = MyGymList[indexPath.row]["Working_Hour"] as? String ?? ""
        
        let cretedOn = MyGymList[indexPath.row]["DateTime"] as? String ?? ""
                                     

                                     if cretedOn != "" {
                                  
                               //   let cretedOn = (dic.value(forKey: "EventFromDate") as? String ?? "")
                                      
                                        cell.lblDate.text = cretedOn.toDate11()
                                       
                                     }
        
        
        cell.lblGymName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblFees.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblDate.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblPublish.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        cell.lblLoC.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
               
        cell.lblLoC.text = MyGymList[indexPath.row]["LocationAddress"] as? String ?? ""
               
               
              
               
               cell.lblFees.text = "$" + "\(MyGymList[indexPath.row]["Amount"] as? String ?? "")"
              
               cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
               
               return cell
       }
       
    
    
    
    func getMyGymList()
          {
             
              
              let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                            "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                            ]
      
              callApi(ServiceList.SERVICE_URL+ServiceList.APP_GYMCHECKINLIST,
                      method: .get,
                      param: [:] ,
                      extraHeader: header ,
                      completionHandler: { (result) in
                          print(result)

                          if result.getBool(key: "status")
                          {
                              
                              showToast(uiview: self, msg: result.getString(key: "message"))
                               self.MyGymList = result["data"] as? [[String : Any]] ?? []
                              
                              self.tblmygymList.reloadData()
                              
                          }
                          showToast(uiview: self, msg: result.getString(key: "message"))

              })
          }
          
      


}
