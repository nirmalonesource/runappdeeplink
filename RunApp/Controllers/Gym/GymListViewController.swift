//
//  GymListViewController.swift
//  
//
//  Created by My Mac on 27/01/21.
//

import UIKit
import NightNight
import CoreLocation

class GymListCell: UITableViewCell {

    @IBOutlet weak var lblLoC: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var lblGymName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblFees: UILabel!
    @IBOutlet weak var lblPublish: UILabel!
    @IBOutlet weak var btnSelectOutlet: UIButton!

   
}


class GymListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate


{
    @IBOutlet var btndatetime: UIButton!
    @IBOutlet var btn_datetimesel: UITextField!
   
    let datePicker = UIDatePicker()
    
    
    @IBOutlet var lbllocationtitle: UILabel!
    @IBOutlet var llbllocationaddress: UILabel!
    var Amt = ""
    var GYMID = ""
    var ScheduleID = ""
    @IBOutlet var lblamounttobepaid: UILabel!
    @IBOutlet var vwPaypopup: UIView!
    
    @IBOutlet var btn_datetime: UIButton!
    @IBOutlet var imgBlur: UIImageView!
    
    var lagitude = Double()
    var logitude = Double()
    var locationManager = CLLocationManager()
    var flag = Bool()
    
    
     var position = 0
    var GymList = [[String : Any]]()
     var Schedules = [[String : Any]]()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
            {
               if flag {
                   flag = false
                   if let location = locations.last
                   {
                       //print("Found user's location: \(location)")
                       print("latitude: \(location.coordinate.latitude)")
                       print("longitude: \(location.coordinate.longitude)")
                       lagitude = location.coordinate.latitude
                       logitude = location.coordinate.longitude
                       locationManager.stopUpdatingLocation()
                      // getlocationinfo()
                   }
               }
                   
           }
           
           func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
                  print("Unable to access your current location")
              }
          
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Schedules.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "GymListCell", for: indexPath) as! GymListCell
           
            cell.lblGymName.text = Schedules[indexPath.row]["Time"] as? String ?? ""
            cell.lblDate.text = Schedules[indexPath.row]["date"] as? String ?? ""
            cell.lblGymName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
            cell.lblFees.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
            cell.lblDate.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
            cell.lblPublish.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
            cell.lblLoC.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
            
            cell.lblLoC.text = Schedules[indexPath.row]["LocationAddress"] as? String ?? ""
            
         cell.btn_Cancel.tag = indexPath.row
           
        cell.btn_Cancel.addTarget(self, action: #selector(self.btnPay(_:)), for: .touchUpInside)
            
           
             let paid = Schedules[indexPath.row]["Registered"] as? String ?? ""
        
                if paid == "0"
        {
            cell.btn_Cancel.tag = indexPath.row

             cell.btn_Cancel.addTarget(self, action: #selector(self.btnPay(_:)), for: .touchUpInside)
            cell.btn_Cancel.isUserInteractionEnabled = true
                              cell.btn_Cancel.setTitle("PAY NOW", for: .normal)
        }
            else
                {
                    cell.btn_Cancel.isUserInteractionEnabled = false
                    cell.btn_Cancel.setTitle("Paid", for: .normal)
        }
            
            cell.lblFees.text = "$" + "\(Schedules[indexPath.row]["Amount"] as? String ?? "")"
           
            cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
            
            return cell
    }
    
 
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideViewWithAnimation(vw: vwPaypopup, img: imgBlur)
    }

    
    
    @IBAction func btn_back(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
        
        
    }
    
    @IBOutlet var vwPopup: UIView!
    
    
    @IBOutlet var tblgymList: UITableView!
    
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        vwPopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        tblgymList.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
               
     //   vwPaypopup.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
       //      lblamounttobepaid.mixedTextColor =  MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
            
           btn_datetimesel.setRadius(radius: 12)
            btn_datetimesel.setRadiusBorder(color: UIColor.gray)
             btn_datetimesel.roundCorners(corners: .allCorners, radius: 2)
        
        
        
        showDatePicker()
       
        vwPopup.setRadius(radius: 10)
        tblgymList.tableFooterView = UIView()
        
        if CLLocationManager.locationServicesEnabled() == true {
                                   
                                   if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                                       locationManager.requestWhenInUseAuthorization()
                                   }
                                   
                                   locationManager.desiredAccuracy = kCLLocationAccuracyBest
                                   locationManager.delegate = self
                                   locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                                   locationManager.requestWhenInUseAuthorization()
                                   locationManager.startUpdatingLocation()
                   
                                   locationManager.requestLocation()
                   
                   let currentLoc = locationManager.location
                   lagitude =  (currentLoc?.coordinate.latitude ?? 0)
                   logitude = (currentLoc?.coordinate.longitude ?? 0)
                    getGymList()
                                   flag = true
                                   // mapVW.mapType = .hybrid
                               } else {
                                   print("Please turn on location services or GPS")
                               }
        
    }
    
    @objc func btnPay(_ sender : UIButton)
        {
            
            let row = 0
            let schedulerow = sender.tag
            self.Schedules = GymList[row]["Schedule"] as? [[String : Any]] ?? []
            print(Schedules)
            Amt = Schedules[schedulerow]["Amount"] as? String ?? ""
            GYMID = Schedules[schedulerow]["GymID"] as? String ?? ""
            ScheduleID = Schedules[schedulerow]["ScheduleID"] as? String ?? ""
            
             
            
            
          if Amt == "0"
          {
            postpayment()
            }
        else
          {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GymPaymentViewController") as! GymPaymentViewController
                                        
            nextViewController.rsvpDictList1 =  self.Schedules[row]
                            nextViewController.GymID = self.GYMID
                            nextViewController.Amount = self.Amt
                            nextViewController.Datetime1 = self.btn_datetimesel.text!
            nextViewController.ScheduleID = self.ScheduleID
                self.navigationController?.pushViewController(nextViewController, animated: true)
            
            }
           
          //  lblamounttobepaid.text =  "Amount to be paid : $\(self.Amt)"
            
        //showViewWithAnimation(vw: vwPaypopup, img: imgBlur)
         
        
         
       }
    func showDatePicker(){
           //Formate Date
           datePicker.datePickerMode = .dateAndTime

          //ToolBar
          let toolbar = UIToolbar();
          toolbar.sizeToFit()
          let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
           let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
         let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

         btn_datetimesel.inputAccessoryView = toolbar
         btn_datetimesel.inputView = datePicker
         
        
         
           
        }

    
    @IBAction func btn_PayNow(_ sender: UIButton) {
        
//        if btn_datetimesel.text != ""
//        {
////          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GymPaymentViewController") as! GymPaymentViewController
////        nextViewController.rsvpDictList1 =  GymList[sender.tag]
////        nextViewController.GymID = GYMID
////        nextViewController.Amount = Amt
////        nextViewController.Datetime1 = btn_datetimesel.text!
////            self.navigationController?.pushViewController(nextViewController, animated: true)
//
//            position = sender.tag
//            getPaymentDetails()
//
//        }
//        else{
//            showToast(uiview: self, msg:"Please Select Date & Time")
//        }
    }
    
    
    
    func getPaymentDetails()
              {
               
               let date1 = btn_datetimesel.text
               
               
               
               let parameters = ["GymID" : GYMID,
                                 "Date" : date1!.toDate6()
                                       
                   ] as [String:Any]
                  
                  let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                ]
          
                  callApi(ServiceList.SERVICE_URL+ServiceList.get_payementDetails,
                          method: .post,
                          param: parameters ,
                          extraHeader: header ,
                          completionHandler: { (result) in
                              print(result)

                              if result.getBool(key: "status")
                              {
               
                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GymPaymentViewController") as! GymPaymentViewController
                               nextViewController.rsvpDictList1 =  self.GymList[self.position]
                  nextViewController.GymID = self.GYMID
                  nextViewController.Amount = self.Amt
                  nextViewController.Datetime1 = self.btn_datetimesel.text!
                   self.navigationController?.pushViewController(nextViewController, animated: true)
                               
                                  
                              }
                              showToast(uiview: self, msg: result.getString(key: "message"))

                  })
              }
              
          

      func postpayment()
                {
                 
                 let date1 = btn_datetimesel.text
                 
                 
                 
                 let parameters = ["GymID" : GYMID,
                                   "ScheduleID" : ScheduleID,
                                   "DateTime" : date1!,
                    "Token" : "",
                    "Amount": "",
                    "StripeTrasactionID" :"",
                    "Note" : ""
                                   
                                         
                     ] as [String:Any]
                    
                    print(parameters)
                    
                    let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                  "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                                  ]
            
                    callApi(ServiceList.SERVICE_URL+ServiceList.APP_GYMPAYMENT,
                            method: .post,
                            param: parameters ,
                            extraHeader: header ,
                            completionHandler: { (result) in
                                print(result)

                                if result.getBool(key: "status")
                                {
                 
                    // let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "GymPaymentViewController") as! GymPaymentViewController
                             //   self.navigationController?.pushViewController(nextViewController, animated: true)
                                    self.getGymList()
                                    
                                }
                                showToast(uiview: self, msg: result.getString(key: "message"))

                    })
                }
                
    
    
         @objc func donedatePicker(){

          let formatter = DateFormatter()
          formatter.dateFormat = "yyyy-MM-dd"
          btn_datetimesel.text = formatter.string(from: datePicker.date)
         getGymList()
          self.view.endEditing(true)
            
        }

        @objc func cancelDatePicker(){
           self.view.endEditing(true)
         }
       
      func getGymList()
        {
          
            
            let parameters = ["lat" : lagitude ,
                              "long" : logitude,
                              "GymID" : GYMID,
                              "date" : btn_datetimesel.text ?? ""
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ]
    
            callApi(ServiceList.SERVICE_URL+ServiceList.GymList,
                    method: .post,
                    param: parameters ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)

                        if result.getBool(key: "status")
                        {
                            
                         //   showToast(uiview: self, msg: result.getString(key: "message"))
                            showToast(uiview: self, msg: "Kindly select date and pay for joining gym.")
                             self.GymList = result["data"] as? [[String : Any]] ?? []
                            self.llbllocationaddress.text = self.GymList[0]["LocationAddress"] as? String ?? ""
                           self.lbllocationtitle.text = self.GymList[0]["LocationName"] as? String ?? ""
                            
                            self.Schedules = self.GymList[0]["Schedule"] as? [[String : Any]] ?? []
                            self.tblgymList.reloadData()
                            
                        }
                        else
                        {
                            showToast(uiview: self, msg: result.getString(key: "message"))
                        }
                       

            })
        }
        
    
    
    @IBAction func btn_showdatepicker(_ sender: Any) {
        showDatePicker()
        
    }
    
}
