//
//  SubcommentslistViewController.swift
//  RunApp
//
//  Created by My Mac on 11/02/21.
//  Copyright © 2021 My Mac. All rights reserved.
//

import UIKit
import NightNight

class subcommentlist : UITableViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var leadingConstrants: NSLayoutConstraint!
    
    @IBOutlet var btnimg: UIButton!
    @IBOutlet var lblcount: UILabel!
    @IBOutlet var btnlike: UIButton!
    @IBOutlet weak var lblTime: UILabel!
}



class SubcommentslistViewController: UIViewController,UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var lblTitlechatboard: UILabel!
     
     @IBOutlet var runlogo: UIImageView!
     
     
     @IBOutlet weak var imgBG: UIImageView!
     @IBOutlet weak var vwPopupTop: UIView!
     
    @IBOutlet weak var tblSubcommentView: UITableView!
     
     @IBOutlet weak var vwPopupTopComment: UIView!
     @IBOutlet weak var imgUser: UIImageView!
     @IBOutlet weak var txtWriteComment: UITextField!
     
     @IBOutlet weak var imgCommentUser: UIImageView!
     @IBOutlet weak var lblCommentUser: UILabel!
     @IBOutlet weak var lblCommentTitle: UILabel!
     @IBOutlet weak var lblCommentCount: UILabel!
     @IBOutlet weak var lblCount: UILabel!
    
    
  
    var subCommentList = [[String:Any]]()
     var CommetID = ""
   var ParentCommentID = ""
  var PhotoID = ""
    var commentList1 = [String:Any]()
    var subcommentid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(commentList1)
       lblCommentUser.text = commentList1["FirstName"] as? String ?? "Unknown name"
   let  message  = commentList1["Comment"] as? String
        
    let emojData = message!.data(using: .utf8)
    lblCommentTitle.text = String.init(data: emojData!, encoding: .nonLossyASCII)
        
        
        let imgURL = ServiceList.IMAGE_URL + "\(commentList1["UserProfile"] as? String ?? "")"
         imgCommentUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,completed: nil)
       imgCommentUser.setRadius(radius:imgCommentUser.frame.height/2)
       imgCommentUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))
        
        getSubComment()
        
        
        
        
        
          vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
                lblTitlechatboard.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                lblCommentUser.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                lblCommentTitle.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                lblCount.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                txtWriteComment.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
                
                
                
                
                self.view.layoutIfNeeded()
                txtWriteComment.setLeftPaddingPoints(30, strImage: "")
                txtWriteComment.layer.masksToBounds = true
                txtWriteComment.layer.cornerRadius = 20.0

                txtWriteComment.setRadiusBorder(color: UIColor.red)
                
        //        imgBG.setGredient()
                
                vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
                tblSubcommentView.tableFooterView = UIView()
               // getComment()
                
                
                
                
                self.view.layoutIfNeeded()
            
                
                
                       let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
                               tapGestureRecognizer2.delegate = self
                               runlogo.isUserInteractionEnabled = true
                               runlogo.addGestureRecognizer(tapGestureRecognizer2)
                
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        getSubComment()
    }
    

    
    
     @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
           
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
             nextViewController.type = "Adv"
            self.navigationController?.pushViewController(nextViewController, animated: true)
          
            
        }
        //MARK: - TextField Methods
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        
        //MARK: - BUTTON METHODS
        
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func btnSendPressed(_ sender: UIButton) {
            self.view.endEditing(true)
            if txtWriteComment.text == ""
            {
                showAlert(uiview: self, msg: "Please enter comment", isTwoButton: false)
            }
            else
            {
                addSubComment()
                
            }
        }
        
        //MARK:- TABLEVIEW METHODS
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return subCommentList.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subcommentlist", for: indexPath) as! subcommentlist
            
            cell.lblName.text = subCommentList[indexPath.row]["FirstName"] as? String ?? "Unknown name"

            let getCommet = subCommentList[indexPath.row]["Comment"] as? String

    //        let dataa : NSData = getCommet!.data(using: String.Encoding.utf8)! as NSData
    //        cell.lblDescription.text = String(data: dataa as Data, encoding: String.Encoding.nonLossyASCII)!
            let emojData = getCommet!.data(using: .utf8)
            cell.lblDescription.text = String.init(data: emojData!, encoding: .nonLossyASCII)

            cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
            cell.imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))

            let imgURL = ServiceList.IMAGE_URL + "\(subCommentList[indexPath.row]["UserProfile"] as? String ?? "")"
            cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                     completed: nil)
            cell.lblTime.text = "\(subCommentList[indexPath.row]["CreatedOn"] as? String ?? "")".toDate().timeAgoSinceDate()
            
            cell.lblcount.text = subCommentList[indexPath.row]["TotalLikes"] as? String
//
            
            cell.btnimg.tag = indexPath.row
            cell.btnimg.addTarget(self, action: #selector(userprofile(button:)), for: .touchUpInside)
            
            let status = subCommentList[indexPath.row]["IsLikes"] as? String ?? ""
                                       
                                        // cell.lblCount.text = total
                                         
                                         if status == "0" {
                                             
                                             let image = UIImage(named: "UnfilledHurt1")
                                             
                                             cell.btnlike.setImage(image, for: .normal)
                                             
                                           //  PhotoID = commentList[indexPath.row]["PhotoID"] as? String ?? ""
                                             
                                             cell.btnlike.tag = indexPath.row
                                             cell.btnlike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                                             cell.btnlike.isEnabled = true
                                            
                                         }
                                             
                                         else {
                                             
                                             let image = UIImage(named: "filledHurt1")
                                             cell.btnlike.setImage(image, for: .normal)
                                             
                                          
                                            
                                          
                                            
                                            cell.btnlike.tag = indexPath.row
                                            cell.btnlike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                                            cell.btnlike.isEnabled = true
                                          
                                           

                                         }
                                         
            
            cell.mixedBackgroundColor = MixedColor(normal:FontColor.nightfont, night:FontColor.dayfont)
            cell.lblName.mixedTextColor = MixedColor(normal:FontColor.dayfont, night:FontColor.nightfont)
            
            
            cell.selectionStyle = .none
            return cell
            
        }


    
    func getSubComment()
           {
               //let parameters = ["PhotoID" : 23] as [String : Any]

               let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                             "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                             ] as [String : Any]

               callApi(ServiceList.SERVICE_URL+ServiceList.GET_ADDSUBPHOTOCOMMENT + "/" + CommetID,
                       method: .get,
                       param: [:] ,
                       extraHeader: header ,
                       completionHandler: { (result) in
                           print(result)



                           if result.getBool(key: "status")
                           {
       //                        let arrDict = result["data"] as? [[String:Any]] ?? []
       //                        let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
       //                        self.commentList = sortedArray
                              // let GetData:NSDictionary = result["data"] as! NSDictionary
                               let arrDict = result["data"] as? [[String:Any]] ?? []

                              // let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
                               self.subCommentList = arrDict
                               
                              
                            
                            
                               self.tblSubcommentView.reloadData()


                              
                           }
               })
           }

    
            func addSubComment()
            {
        //        let str = txtWriteComment.text!
        //
        //        let data : NSData = str.data(using: String.Encoding.nonLossyASCII)! as NSData
        //        let valueUnicode : String = String(data: data as Data, encoding: String.Encoding.utf8)!
                let data =  txtWriteComment.text!.data(using: .nonLossyASCII)
                let valueUnicode : String = String.init(data: data!, encoding: .utf8)!
    
                
                
                let parameters = ["PhotoID" : PhotoID ,
                                  "Comment" : valueUnicode ,
                                  "IsParentComment" : 1,
                                  "ParentCommentID" : CommetID ] as [String : Any]
    
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                              ] as [String : Any]
    
                callApi(ServiceList.SERVICE_URL+ServiceList.AP_ADDPHOTOCOMMENT,
                        method: .post,
                        param: parameters ,
                        extraHeader: header ,
                        completionHandler: { (result) in
                            print(result)
                            if result.getBool(key: "status")
                            {
                                self.getSubComment()
                                self.txtWriteComment.text = ""
                            }
                            showToast(uiview: self, msg: result.getString(key: "message"))
                })
    
            }
    
    
    
       @objc func btnLikeAction(button: UIButton) {
           let buttonRow = button.tag
        //   tappedButtonsTags.append(button.tag)
                
          // let total = getPhotoDict[button.tag]["total"] as? String ?? ""
           
            let PlayerID = subCommentList[button.tag]["id"] as? String ?? ""
           if PlayerID == UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
               showToast(uiview: self, msg: "You can not like own post")
           }
           else
           {
               let status = subCommentList[button.tag]["IsLikes"] as? String ?? ""
                   
                   if status == "0" {
                       subcommentid = subCommentList[button.tag]["CommentID"] as? String ?? ""
                       APP_LIKEDISLIKE(CommentID : subcommentid)
                   }
                       
                   else {
                        subcommentid = subCommentList[button.tag]["CommentID"] as? String ?? ""
                        APP_LIKEDISLIKE(CommentID : subcommentid)
                   }
               
                   print("Like Clicked: \(buttonRow)")
           }
          
       }
   
           
   @objc func userprofile(button: UIButton) {
       
   
       
       
       

    let usertype = subCommentList[button.tag]["usertype"] as? String
                                                     
                                                     if usertype == "Organization"
                                                     {
                                                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                                        nextViewController.playerOrglResult = subCommentList[button.tag]
                                                            nextViewController.isfromsearch = true;
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                         
                                                         
                                                     }
                                                     else
                                                     {
                                                         
                                                         let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                                        nextViewController.playerListDetail = subCommentList[button.tag]
                                                         nextViewController.isfromsearch = true;
                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                     }
                 
     
       
       }
   
          func APP_LIKEDISLIKE( CommentID: String)
          {
              let parameters = ["CommentID" : subcommentid] as [String : Any]
              
              let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                            "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                            ] as [String : Any]
              
              callApi(ServiceList.SERVICE_URL+ServiceList.API_LIKEDISLIKE,
                      method: .post,
                      param: parameters,
                      extraHeader: header,
                      completionHandler: { (result) in
                          print(result)
                          if result.getBool(key: "status")
                          {
                              
                         
                           self.getSubComment()
                              showToast(uiview: self, msg: result.getString(key: "message"))
                          }
              })
          }
}
