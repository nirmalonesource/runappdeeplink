//
//  CommentListViewController.swift
//  RunApp
//
//  Created by My Mac on 11/02/21.
//  Copyright © 2021 My Mac. All rights reserved.
//

import UIKit
import NightNight


class commentListCell: UITableViewCell {
    
    
    
    @IBOutlet var btnLike: UIButton!
    
    @IBOutlet var lbllikecoubt: UILabel!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var leadingConstrants: NSLayoutConstraint!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet var lbllikecount: UILabel!
    
    @IBOutlet var btnimg: UIButton!
    @IBOutlet var btn_reply: UIButton!
}



class CommentListViewController: UIViewController,UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate,UIGestureRecognizerDelegate {

    
   
    @IBOutlet var lbllatestcomment: UILabel!
    @IBOutlet var runlogo: UIImageView!
     
     
     @IBOutlet weak var imgBG: UIImageView!
     @IBOutlet weak var vwPopupTop: UIView!
     
     @IBOutlet weak var tblCommentListView: UITableView!
     
     @IBOutlet weak var vwPopupTopComment: UIView!
     @IBOutlet weak var imgUser: UIImageView!
     @IBOutlet weak var txtWriteComment: UITextField!
     var commentList = [[String:Any]]()
     
     var CommentID = ""
     var PhotoID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getComment()
        
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
        lbllatestcomment.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
        txtWriteComment.mixedTextColor  = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
            
     
        txtWriteComment.setLeftPaddingPoints(30, strImage: "")
            txtWriteComment.layer.masksToBounds = true
            txtWriteComment.layer.cornerRadius = 20.0
            
            txtWriteComment.setRadiusBorder(color: UIColor.red)
            
            vwPopupTop.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
            tblCommentListView.tableFooterView = UIView()
            
          //  getComment()
            self.view.layoutIfNeeded()
            
            // Do any additional setup after loading the view.
            
            let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handlelogo))
            tapGestureRecognizer2.delegate = self
            runlogo.isUserInteractionEnabled = true
     runlogo.addGestureRecognizer(tapGestureRecognizer2)
                  
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    func getComment()
        {
           

            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                          "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                          ] as [String : Any]

            callApi(ServiceList.SERVICE_URL+ServiceList.Get_PHOTOCOMMENTS + "/" + PhotoID,
                    method: .get,
                    param: [:] ,
                    extraHeader: header ,
                    completionHandler: { (result) in
                        print(result)



                        if result.getBool(key: "status")
                        {
    //                        let arrDict = result["data"] as? [[String:Any]] ?? []
    //                        let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
    //                        self.commentList = sortedArray
                           // let GetData:NSDictionary = result["data"] as! NSDictionary
                            let arrDict = result["data"] as? [[String:Any]] ?? []

                           // let sortedArray = self.sortArrayDictDescending(dict: arrDict, dateFormat: "yyyy-MM-dd HH:mm:ss")
                            self.commentList = arrDict
                            self.tblCommentListView.reloadData()


                            self.tblCommentListView.reloadData()

                        }
            })
        }


    @objc func handlelogo(_ gestureRecognizer: UISwipeGestureRecognizer) {
                
                 let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SpotLightViewController") as! SpotLightViewController
                  nextViewController.type = "Adv"
                 self.navigationController?.pushViewController(nextViewController, animated: true)
               
                 
             }
       
       
       //MARK:- TextField Methods
       
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return true
       }
       
       //MARK:- BUTTON METHODS
       
       @IBAction func btnBackAction(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
       
       @IBAction func btnSendPressed(_ sender: UIButton) {
           self.view.endEditing(true)
         
        
        
           if txtWriteComment.text == ""
           {
               showAlert(uiview: self, msg: "Please enter comment", isTwoButton: false)
           }
           else
           {
               addComment()
           }
           
       }
       
       //MARK:- TABLEVIEW METHODS
       func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentList.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "commentListCell", for: indexPath) as! commentListCell
           
           cell.lblName.text = commentList[indexPath.row]["FirstName"] as? String ?? "Unknown name"
        
        let message = commentList[indexPath.row]["Comment"] as? String
        let emojData = message!.data(using: .utf8)
        cell.lblDescription.text = String.init(data: emojData!, encoding: .nonLossyASCII)
        
        
           cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
           cell.imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.nav_red))

        cell.lbllikecount.text = commentList[indexPath.row]["TotalLikes"] as? String
        
        let status = commentList[indexPath.row]["IsLikes"] as? String ?? ""
        
        cell.btn_reply.tag = indexPath.row
        cell.btn_reply.addTarget(self, action: #selector(btnReplyAction), for: .touchUpInside)
        cell.btn_reply.isEnabled = true
        
        cell.btnimg.tag = indexPath.row
        cell.btnimg.addTarget(self, action: #selector(userprofile(button:)), for: .touchUpInside)
                             
                              // cell.lblCount.text = total
                               
                               if status == "0" {
                                   
                                   let image = UIImage(named: "UnfilledHurt1")
                                   
                                   cell.btnLike.setImage(image, for: .normal)
                                   
                                 //  PhotoID = commentList[indexPath.row]["PhotoID"] as? String ?? ""
                                   
                                   cell.btnLike.tag = indexPath.row
                                   cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                                   cell.btnLike.isEnabled = true
                               }
                                   
                               else {
                                   
                                   let image = UIImage(named: "filledHurt1")
                                   cell.btnLike.setImage(image, for: .normal)
                                   
                                
                                  
                                
                                  
                                  cell.btnLike.tag = indexPath.row
                                  cell.btnLike.addTarget(self, action: #selector(btnLikeAction), for: .touchUpInside)
                                  cell.btnLike.isEnabled = true
                                
                                 

                               }
                               
        
        
        
           let imgURL = ServiceList.IMAGE_URL + "\(commentList[indexPath.row]["UserProfile"] as? String ?? "")"
           cell.imgUser.sd_setImage(with: URL(string : (imgURL.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad,
                                    completed: nil)
           cell.lblTime.text = "\(commentList[indexPath.row]["CreatedOn"] as? String ?? "")".toDate().timeAgoSinceDate()

           cell.lblName.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
            cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night:FontColor.dayfont)
           
           cell.lbllikecount.mixedTextColor = MixedColor(normal: FontColor.dayfont, night:FontColor.nightfont)
        
           cell.selectionStyle = .none
           return cell
           
       }
       
    
        @objc func btnLikeAction(button: UIButton) {
            let buttonRow = button.tag
         //   tappedButtonsTags.append(button.tag)
                 
           // let total = getPhotoDict[button.tag]["total"] as? String ?? ""
            
             let PlayerID = commentList[button.tag]["id"] as? String ?? ""
            if PlayerID == UserDefaults.standard.getUserDict()["id"] as? String ?? "" {
                showToast(uiview: self, msg: "You can not like own post")
            }
            else
            {
                let status = commentList[button.tag]["IsLikes"] as? String ?? ""
                    
                    if status == "0" {
                        CommentID = commentList[button.tag]["CommentID"] as? String ?? ""
                        APP_LIKEDISLIKE(CommentID : CommentID)
                    }
                        
                    else {
                         CommentID = commentList[button.tag]["CommentID"] as? String ?? ""
                         APP_LIKEDISLIKE(CommentID : CommentID)
                    }
                
                    print("Like Clicked: \(buttonRow)")
            }
           
        }
    
    @objc func btnReplyAction(button: UIButton) {
        
            let buttonRow = button.tag
        
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubcommentslistViewController") as! SubcommentslistViewController
                nextViewController.CommetID = commentList[buttonRow]["CommentID"] as? String ?? ""
                nextViewController.PhotoID = commentList[buttonRow]["PhotoID"] as? String ?? ""
                nextViewController.commentList1 =  commentList[buttonRow]
                self.navigationController?.pushViewController(nextViewController, animated: true)
              
           }
    
    
    @objc func userprofile(button: UIButton) {
           
       
           
           
           

        let usertype = commentList[button.tag]["usertype"] as? String
                                                         
                                                         if usertype == "Organization"
                                                         {
                                                             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrganizationProfile") as! OrganizationProfile
                                                            nextViewController.playerOrglResult = commentList[button.tag]
                                                                nextViewController.isfromsearch = true;
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                             
                                                             
                                                         }
                                                         else
                                                         {
                                                             
                                                             let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
                                                            nextViewController.playerListDetail = commentList[button.tag]
                                                             nextViewController.isfromsearch = true;
                                                           self.navigationController?.pushViewController(nextViewController, animated: true)
                                                         }
                     
         
           
           }
    
    
            func addComment()
            {
        //        let str = txtWriteComment.text!
        //
        //        let data : NSData = str.data(using: String.Encoding.nonLossyASCII)! as NSData
        //        let valueUnicode : String = String(data: data as Data, encoding: String.Encoding.utf8)!
                let data =  txtWriteComment.text!.data(using: .nonLossyASCII)
                let valueUnicode : String = String.init(data: data!, encoding: .utf8)!
    
                
                
                let parameters = ["PhotoID" : PhotoID ,
                                  "Comment" : valueUnicode ,
                                  "IsParentComment" : 0,
                                  "ParentCommentID" : ""] as [String : Any]
    
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                              "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                              ] as [String : Any]
    
                callApi(ServiceList.SERVICE_URL+ServiceList.AP_ADDPHOTOCOMMENT,
                        method: .post,
                        param: parameters ,
                        extraHeader: header ,
                        completionHandler: { (result) in
                            print(result)
                            if result.getBool(key: "status")
                            {
                                self.getComment()
                                self.txtWriteComment.text = ""
                            }
                            showToast(uiview: self, msg: result.getString(key: "message"))
                })
    
            }
    
    
           func APP_LIKEDISLIKE( CommentID: String)
           {
               let parameters = ["CommentID" : CommentID] as [String : Any]
               
               let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                             "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
                             ] as [String : Any]
               
               callApi(ServiceList.SERVICE_URL+ServiceList.API_LIKEDISLIKE,
                       method: .post,
                       param: parameters,
                       extraHeader: header,
                       completionHandler: { (result) in
                           print(result)
                           if result.getBool(key: "status")
                           {
                               
                          
                            self.getComment()
                               showToast(uiview: self, msg: result.getString(key: "message"))
                           }
               })
           }
       

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubcommentslistViewController") as! SubcommentslistViewController
        nextViewController.CommetID = commentList[indexPath.row]["CommentID"] as? String ?? ""
        nextViewController.PhotoID = commentList[indexPath.row]["PhotoID"] as? String ?? ""
        nextViewController.commentList1 =  commentList[indexPath.row]
            self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    
}
