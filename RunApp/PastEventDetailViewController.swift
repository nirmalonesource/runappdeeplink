//
//  PastEventDetailViewController.swift
//  RunApp
//
//  Created by My Mac on 25/11/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import NightNight

class PastEventDetailViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate {

   
    @IBOutlet weak var lblMaintitle: UILabel!
    
    @IBOutlet weak var imgBG: UIImageView!
       @IBOutlet weak var vwPopupTop: UIView!
    
    var CheckInList = [[String : Any]]()
    var locationid : String?
    @IBOutlet weak var tblcheckinalllist: UITableView!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

      //  CheckinList()
       
        vwPopupTop.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        
        tblcheckinalllist.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont , night: FontColor.dayfont)
        
        lblMaintitle.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

//    func Checkinlist()
//       {
//           let parameters = ["Player_ID" : UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                             "Location_ID" : locationid!
//
//               ] as [String : Any]
//
//           let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                         "X-SIMPLE-LOGIN-TOKEN":UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
//                         ] as [String : Any]
//
//           self.callApi(ServiceList.SERVICE_URL+ServiceList.APP_get_checkinList,
//                        method: .post,
//                        param: parameters,
//                        extraHeader: header) { (result) in
//
//                            print(result)
//
//                           if result.getBool(key: "status")
//                           {
//                               self.CheckInList = result["data"] as? [[String : Any]] ?? []
//
//
//                           }
//
//                           self.tblcheckinalllist.reloadData()
//                          // showToast(uiview: self, msg: result.getString(key: "message"))
//           }
//       }
    
//    func CheckinList()
//    {
//
//    let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
//                        "X-SIMPLE-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? "" ,
//                        ] as [String : Any]
//
//          callApi(ServiceList.SERVICE_URL+ServiceList.GET_LOCATIONS,
//                  method: .post,
//                  param: ["LocationID" : locationid!] ,
//                  extraHeader: header ,
//                  completionHandler: { (result) in
//                      print(result)
//                      if result.getBool(key: "status")
//                      {
//                          let GetData:NSArray = result["data"] as! NSArray
//                       //   self.dictLocation = GetData.value(forKey: "location") as? [String:Any] ?? [:]
//                        //  self.parseLocation(LocationDict: GetData.object(at: 0) as? [String : Any] ?? [:])
//                          let playerjoin:NSArray = GetData.value(forKey: "playerjoin") as! NSArray
//                          self.CheckInList = playerjoin.object(at: 0) as? [[String:Any]] ?? []
//
//                      }
//
//                      DispatchQueue.main.async {
//                        self.tblcheckinalllist.reloadData()
//                      }
//            })
//
//    }
        //MARK:- TABLEVIEW METHODS
       func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
      
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return CheckInList.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCheckInListCell", for: indexPath) as! EventCheckInListCell
          
        cell.lblname.text = CheckInList[indexPath.row]["FirstName"] as? String ?? ""
        let imgurl = ServiceList.IMAGE_URL + "\(CheckInList[indexPath.row]["UserProfile"] as? String ?? "")"
        cell.imgprofile.setRadius(radius: cell.imgprofile.frame.height/2)
        cell.imgprofile.sd_setImage(with: URL(string : (imgurl.URLQueryAllowedString()!)), placeholderImage : #imageLiteral(resourceName: "user") , options: .progressiveLoad, completed: nil)
        
        
//           let points = notificationList[indexPath.row]["Points"] as? String ?? ""
          
         // cell.lblpointsearned.text = points  + " Points"
              
           cell.lblname.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
     
         //     cell.lblpointsearned.mixedTextColor = MixedColor(normal: FontColor.dayfont, night: FontColor.nightfont)
       
          
           cell.mixedBackgroundColor = MixedColor(normal: FontColor.nightfont, night: FontColor.dayfont)
           
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
           return cell
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileView
             
             if CheckInList[indexPath.row].count > 0 {
                 nextViewController.playerListDetail = CheckInList[indexPath.row]
          
    
                
        }
              self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
      
      @IBAction func btn_back(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
      }
      
    
    
}
